/**
 * 
 */
package org.statnlp.util;

import org.statnlp.util.GenericPipeline;
import org.statnlp.util.Runner;

/**
 * To test the implementation of {@link GenericPipeline}
 */
public class GenericPipelineTest {
	
	public static void main(String[] args){
		Runner.run(args);
	}

}
