package org.statnlp.statistics;

import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.statnlp.example.nerelation.NYTReader;
import org.statnlp.example.nerelation.RelInstance;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.Span;


public class NYTStatistics {

	
	public static int countMentions(RelInstance[] insts) {
		int num = 0;
		int numL6 = 0;
		int max = 0;
		for (RelInstance inst : insts) {
			num += inst.getOutput().getSpans().size();
			for (Span span : inst.getOutput().getSpans()) {
				if (span.end - span.start + 1 > 6)
					numL6++;
				max = Math.max(max, span.end - span.start + 1);
			}
		}
		System.err.println("length > 6: " + numL6);
		System.err.println("maxlength: " + max);
		return num;
	}
	
	public static double calculateRelationDist(RelInstance[] insts) {
		double dist = 0;
		double maxDist = 0;
		int num = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			List<Span> spans = inst.getOutput().getSpans();
			num += relations.size();
			for (int i = 0; i < relations.size(); i++) {
				Span span1 = spans.get(relations.get(i).getArg1Idx());
				Span span2 = spans.get(relations.get(i).getArg2Idx());
				double currDist = 0;
				if (span1.overlap(span2)) {
					currDist = 0;
				} else if (span1.compareTo(span2) < 0) {
					currDist = span2.start - span1.end - 1;
				} else {
					currDist = span1.start - span2.end - 1;
				}
				maxDist = Math.max(maxDist, currDist);
				dist += currDist;
			}
		}
		System.err.println("max dist: " + maxDist);
		return dist/num;
	}
	
	public static void main(String[] args) throws IOException, ParseException {
		RelInstance[] trainInsts = NYTReader.read("data/NYT/train.json", true, -1);
		RelInstance[] testInsts = NYTReader.read("data/NYT/test.json", false, -1);
		System.err.println("#train: " + trainInsts.length);
		System.err.println("#test: " + testInsts.length);
		System.err.println("#train mentions: " + countMentions(trainInsts));
		System.err.println("#test mentions: " + countMentions(testInsts));
		System.err.println("#train overlap mentions: " + Utils.countOverlapMentionPairs(trainInsts));
		System.err.println("#test overlap mentions: " + Utils.countOverlapMentionPairs(testInsts));
		System.err.println("#train sent with o.l. mentions: " + Utils.countOverlapMentionSentences(trainInsts));
		System.err.println("#test sent with o.l. mentions: " + Utils.countOverlapMentionSentences(testInsts));
		
		System.err.println("#train relations: " + Utils.countRelations(trainInsts));
		System.err.println("#test relations: " + Utils.countRelations(testInsts));
		System.err.println("#train overlap relations: " + Utils.countOverlapRelationPairs(trainInsts));
		System.err.println("#test overlap relations: " + Utils.countOverlapRelationPairs(testInsts));
		System.err.println("#train sent with o.l. relattions: " + Utils.countOverlapRelationSentences(trainInsts));
		System.err.println("#test sent with o.l. relattions: " + Utils.countOverlapRelationSentences(testInsts));
		System.err.println();
		System.err.println("#train relations dist: " + calculateRelationDist(trainInsts));
		System.err.println("#test relations dist: " + calculateRelationDist(testInsts));
		System.err.println();
		System.err.println();
		System.err.println();
	}
	
}
