package org.statnlp.statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.RelInstance;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;


public class Utils {

	public static int countOverlapMentionPairs(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			List<Span> spans = inst.getOutput().getSpans();
			for (int i = 0; i < spans.size(); i++) {
				Span curr = spans.get(i);
				for (int j = i + 1; j < spans.size(); j++) {
					Span other = spans.get(j);
					if (curr.overlap(other))
						num++;
				}
			}
		}
		return num;
	}
	
	public static int countOverlapMentionSentences(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			List<Span> spans = inst.getOutput().getSpans();
			boolean overlap = false;
			for (int i = 0; i < spans.size(); i++) {
				Span curr = spans.get(i);
				for (int j = i + 1; j < spans.size(); j++) {
					Span other = spans.get(j);
					if (curr.overlap(other))
						overlap = true;
				}
			}
			if (overlap)
				num++;
		}
		return num;
	}
	
	public static int countOverlapRelationPairs(RelInstance[] insts) {
		int crossNum = 0;
		int pairNum = 0;
		int totalPairNum = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			for (int i = 0; i < relations.size(); i++) {
				Relation curr = relations.get(i);
				boolean cross = false;
				for (int j = i + 1; j < relations.size(); j++) {
					Relation other = relations.get(j);
					if (curr.overlap(other)) {
						cross = true;
						pairNum++;
					}
					totalPairNum++;
				}
				if (cross) {
					crossNum++;
				}
			}
		}
		System.out.println("crossing number: " + crossNum);
		System.out.println("crossing pair number: " + pairNum);
		System.out.println("totalPairNum pair number: " + totalPairNum);
		return crossNum;
	}
	
	/**
	 * Count whether the second arguments will have multiple different types
	 * @param insts
	 * @return
	 */
	public static int countRightHandSideDiffTypeSameRel(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			/**LeftSpan, RelationType, rightSpanLeftIndex, rightSpan**/
			Map<Span, Map<RelationType, Map<Integer, List<Span>>>> map = new HashMap<>();
			for (int i = 0; i < relations.size(); i++) {
				Relation curr = relations.get(i);
				int leftSpanIdx = Math.min(curr.getArg1Idx(), curr.getArg2Idx());
				int rightSpanIdx = Math.max(curr.getArg1Idx(), curr.getArg2Idx());
				Span leftSpan = inst.getOutput().getSpans().get(leftSpanIdx);
				Span rightSpan = inst.getOutput().getSpans().get(rightSpanIdx);
				Map<RelationType, Map<Integer, List<Span>>> subMap ;
				Map<Integer, List<Span>> subsubMap;
				RelationType relType = curr.getRel();
				int end = 1;
				for (int s = 0; s < end; s++) {
					if (map.containsKey(leftSpan)) {
						subMap = map.get(leftSpan);
						if (subMap.containsKey(relType)) {
							subsubMap = subMap.get(relType);
							if (subsubMap.containsKey(rightSpan.start)) {
								subsubMap.get(rightSpan.start).add(rightSpan);
							} else{
								List<Span> list = new ArrayList<>();
								list.add(rightSpan);
								subsubMap.put(rightSpan.start, list);
							}
						} else {
							subsubMap = new HashMap<>();
							List<Span> list = new ArrayList<>();
							list.add(rightSpan);
							subsubMap.put(rightSpan.start, list);
							subMap.put(relType, subsubMap);
						}
					} else {
						subMap = new HashMap<>();
						subsubMap = new HashMap<>();
						List<Span> list = new ArrayList<>();
						list.add(rightSpan);
						subsubMap.put(rightSpan.start, list);
						subMap.put(relType, subsubMap);
						map.put(leftSpan, subMap);
					}
					if (leftSpan.start == rightSpan.start) {
						end = 2;
						leftSpan = inst.getOutput().getSpans().get(rightSpanIdx);
						rightSpan = inst.getOutput().getSpans().get(leftSpanIdx);
					}
				}
			}
			int numDiffType = 0;
			for (Span leftSpan : map.keySet()) {
				Map<RelationType, Map<Integer, List<Span>>> subMap = map.get(leftSpan);
				for(RelationType rel : subMap.keySet()) {
					Map<Integer, List<Span>> subsubMap = subMap.get(rel);
					for (int rightSpanLeftIndex : subsubMap.keySet()){
						List<Span> list = subsubMap.get(rightSpanLeftIndex);
						Collections.sort(list);
						Set<Entity> set = new HashSet<>();
						for (Span rightSpan : list) {
							set.add(rightSpan.entity);
						}
						numDiffType += set.size() - 1;
					}
				}
			}
			num += numDiffType;
		}
		System.out.println("right hand side different type same relation: " + num);
		return num;
	}
	
	/**
	 * Count whether there are multiple first argument with same type but different length 
	 * @param insts
	 * @return
	 */
	public static int countLeftHandSideSameTypeSameRel(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			/**LeftIndex, RelationType, leftType, rightSpan**/
			Map<Integer, Map<RelationType, Map<Entity, List<Span>>>> map = new HashMap<>();
			for (int i = 0; i < relations.size(); i++) {
				Relation curr = relations.get(i);
				int leftSpanIdx = Math.min(curr.getArg1Idx(), curr.getArg2Idx());
				Span leftSpan = inst.getOutput().getSpans().get(leftSpanIdx);
				Map<RelationType, Map<Entity, List<Span>>> subMap ;
				Map<Entity, List<Span>> subsubMap;
				RelationType relType = curr.getRel();
				if (map.containsKey(leftSpan.start)) {
					subMap = map.get(leftSpan.start);
					if (subMap.containsKey(relType)) {
						subsubMap = subMap.get(relType);
						if (subsubMap.containsKey(leftSpan.entity)) {
							subsubMap.get(leftSpan.entity).add(leftSpan);
						} else{
							List<Span> list = new ArrayList<>();
							list.add(leftSpan);
							subsubMap.put(leftSpan.entity, list);
						}
					} else {
						subsubMap = new HashMap<>();
						List<Span> list = new ArrayList<>();
						list.add(leftSpan);
						subsubMap.put(leftSpan.entity, list);
						subMap.put(relType, subsubMap);
					}
				} else {
					subMap = new HashMap<>();
					subsubMap = new HashMap<>();
					List<Span> list = new ArrayList<>();
					list.add(leftSpan);
					subsubMap.put(leftSpan.entity, list);
					subMap.put(relType, subsubMap);
					map.put(leftSpan.start, subMap);
				}
			}
			int numSameTypeDiffLength = 0;
			for (int leftSpanStart : map.keySet()) {
				Map<RelationType, Map<Entity, List<Span>>> subMap = map.get(leftSpanStart);
				for(RelationType rel : subMap.keySet()) {
					Map<Entity, List<Span>> subsubMap = subMap.get(rel);
					for (Entity ent : subsubMap.keySet()){
						List<Span> list = subsubMap.get(ent);
						Set<Span> set = new HashSet<>(list);
						if (set.size() - 1 > 0) {
							for (Span span : set) {
								System.out.println(span.toString());
							}
							System.out.println(leftSpanStart);
						}
						numSameTypeDiffLength += set.size() - 1;
					}
				}
			}

			if (numSameTypeDiffLength > 0)
				System.out.println(inst.getInput().toString());
			num += numSameTypeDiffLength;
		}
		System.out.println("left hand side same type same relation different length: " + num);
		return num;
	}
	
	public static int countExactOverlapRelationPairs(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			for (int i = 0; i < relations.size(); i++) {
				Relation curr = relations.get(i);
				for (int j = i + 1; j < relations.size(); j++) {
					Relation other = relations.get(j);
					if (curr.exactOverlap(other)) {
//						System.out.println(inst.getInput().toString());
//						System.out.println("curr relation: " + curr.toString());
//						System.out.println("other relation: " + other.toString());
						num++;
					}
				}
			}
		}
		return num;
	}
	
	public static int countOverlapRelationSentences(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			boolean overlap = false;
			for (int i = 0; i < relations.size(); i++) {
				Relation curr = relations.get(i);
				for (int j = i + 1; j < relations.size(); j++) {
					Relation other = relations.get(j);
					if (curr.overlap(other))
						overlap = true;
				}
			}
			if (overlap)
				num++;
		}
		return num;
	}
	
	public static int countRelations(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			num += inst.getOutput().getRelations().size();
		}
		return num;
	}
	
	/**
	 * Print all the relations and number of relations
	 * @param insts
	 * @return
	 */
	public static void printAllRelations(RelInstance[] insts) {
		Set<RelationType> set = new HashSet<>();
		for (RelInstance inst : insts) {
			for (Relation rel : inst.getOutput().getRelations()) {
				set.add(rel.getRel());
			}
		}
		System.err.println("[Info] Number of Relation type: " + set.size());
		System.err.println("[Info] Relations: "+set.toString());
	}
	
	/**
	 * Count the number of mentions that is the first argument under the same relation
	 * starting from the same position, but with different entities.
	 * @param insts
	 */
	public static int countSamePOSSameRelDiffEntofFirstArg(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			/**startingPoint, RelationType, FirstArgumentSpanEntity**/
			Map<Integer, Map<RelationType, Set<Span>>> map = new HashMap<>();
			List<Span> spans = inst.getOutput().getSpans();
			for (Relation rel : relations) {
				Span leftSpan = spans.get(rel.getArg1Idx() < rel.getArg2Idx() ? rel.getArg1Idx() :  rel.getArg2Idx());
				Map<RelationType, Set<Span>> subMap;
				Set<Span> set;
				if (map.containsKey(leftSpan.start)) {
					subMap = map.get(leftSpan.start);
					if (subMap.containsKey(rel.getRel())) {
						set = subMap.get(rel.getRel());
						set.add(leftSpan);
					} else {
						set = new HashSet<Span>();
						set.add(leftSpan);
						subMap.put(rel.getRel(), set);
					}
				} else {
					set = new HashSet<Span>();
					set.add(leftSpan);
					subMap = new HashMap<>();
					subMap.put(rel.getRel(), set);
					map.put(leftSpan.start, subMap);
				}
				if (set.size() > 1) {
//					System.out.println(inst.getInput().toString());
//					System.out.println(leftSpan.toString() + "\t" + rightSpan.toString() + "\t" + rel.getRel().form);
				}
			}
			for (int left : map.keySet()) {
				Map<RelationType, Set<Span>> subMap = map.get(left);
				for (RelationType rel : subMap.keySet()) {
					Set<Entity> smallSet = new HashSet<Entity> ();
					for (Span span : subMap.get(rel)) {
						smallSet.add(span.entity);
					}
					if (smallSet.size() > 1)
						num += subMap.get(rel).size();
				}
			}
		}
		return num;
	}
	
	/**
	 * Count the number of relations that the starting points of 
	 * first and second argument are the same
	 * @param insts
	 * @return number
	 */
	public static int countNumRelStartingPointSame(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			List<Span> spans = inst.getOutput().getSpans();
			for(Relation rel : relations) {
				Span arg1Span = spans.get(rel.getArg1Idx());
				Span arg2Span = spans.get(rel.getArg2Idx());
				if (arg1Span.start == arg2Span.start) {
					num++;
				}
			}
		}
		return num;
	}
	
	/**
	 * Count the number of relations that the starting points of 
	 * first and second argument are the same and the first argument is longer
	 * @param insts
	 * @return number
	 */
	public static int countNumRelStartingPointSameArg1Longer(RelInstance[] insts) {
		int num = 0;
		Set<String> relSet = new HashSet<String>();
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			List<Span> spans = inst.getOutput().getSpans();
			for(Relation rel : relations) {
				Span arg1Span = spans.get(rel.getArg1Idx());
				Span arg2Span = spans.get(rel.getArg2Idx());
				if (arg1Span.start == arg2Span.start && arg1Span.end > arg2Span.end) {
					relSet.add(rel.getRel().form.split("::")[0]);
					num++;
				}
			}
		}
		System.out.println(relSet);
		return num;
	}
	
	/**
	 * Count the number of relations that two arguments are overlapped
	 * @param insts
	 * @return number
	 */
	public static int countNumRelArgsOverlap(RelInstance[] insts) {
		int num = 0;
		int startOverlap = 0;
		int endOverlap = 0;
		int arg1ContainsArg2 = 0;
		int cross = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			List<Span> spans = inst.getOutput().getSpans();
			for(Relation rel : relations) {
				Span arg1Span = spans.get(rel.getArg1Idx());
				Span arg2Span = spans.get(rel.getArg2Idx());
				if (arg1Span.overlap(arg2Span))
					num++;
				if (arg1Span.start == arg2Span.start) {
					startOverlap++;
				}
				if (arg1Span.end == arg2Span.end) {
					endOverlap++;
				}
				if (arg1Span.start <= arg2Span.start && arg1Span.end >= arg2Span.end) {
					arg1ContainsArg2++;
				}
				if (arg1Span.start >= arg2Span.start && arg1Span.end <= arg2Span.end) {
					arg1ContainsArg2++;
				}
				if (arg1Span.end >= arg2Span.start && arg2Span.start>= arg1Span.start && arg2Span.end > arg1Span.end){
					cross++;
				}
				if (arg2Span.end >= arg1Span.start && arg1Span.start >= arg2Span.start && arg1Span.end > arg2Span.end) {
					cross++;
				}
			}
		}
		System.out.println("arg1 and arg2 start is same: "+startOverlap);
		System.out.println("arg1 and arg2 end is same: "+endOverlap);
		System.out.println("one is contained by another: "+arg1ContainsArg2);
		System.out.println("cross: "+cross);
		return num;
	}

	/**
	 * Count the number of multiple second arguments  under 
	 * same first argument, same relation.
	 * @param insts
	 * @return
	 */
	public static int countNumSecondArgOfSameRelSameFirstArg(RelInstance[] insts) {
		int num = 0;
		int secondOverlapNum = 0;
		int maxNumSecondArg = 0;
		int adjacentSeconArg = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			/**Relation, FirstArgument, List<SeconArgument>**/
			Map<RelationType, Map<Span, Set<Span>>> map = new HashMap<>();
			List<Span> spans = inst.getOutput().getSpans();
			for (Relation rel : relations) {
				Span leftSpan = spans.get(rel.getArg1Idx() < rel.getArg2Idx() ? rel.getArg1Idx() :  rel.getArg2Idx());
				Span rightSpan = spans.get(rel.getArg1Idx() < rel.getArg2Idx() ? rel.getArg2Idx() :  rel.getArg1Idx());
				Map<Span, Set<Span>> subMap;
				Set<Span> set;
				if (map.containsKey(rel.getRel())) {
					subMap = map.get(rel.getRel());
					if (subMap.containsKey(leftSpan)) {
						set = subMap.get(leftSpan);
						set.add(rightSpan);
					} else {
						set = new HashSet<Span>();
						set.add(rightSpan);
						subMap.put(leftSpan, set);
					}
				} else {
					set = new HashSet<Span>();
					set.add(rightSpan);
					subMap = new HashMap<>();
					subMap.put(leftSpan, set);
					map.put(rel.getRel(), subMap);
				}
				if (set.size() > 1) {
//					
//					System.out.println(inst.getInput().toString());
//					System.out.println(leftSpan.toString());
//					System.out.println(spans.indexOf(leftSpan) + ", " + spans.indexOf(rightSpan));
//					System.out.println(set.toString());
				}
			}
			for (RelationType rel : map.keySet()) {
				Map<Span, Set<Span>> subMap = map.get(rel);
				for (Span span : subMap.keySet()) {
					num += subMap.get(span).size() - 1;
					boolean overlap = false;
					List<Span> secondArgs = new ArrayList<>(subMap.get(span));
					maxNumSecondArg = Math.max(maxNumSecondArg, secondArgs.size());
					if (secondArgs.size() > 3)
						System.out.println(inst.getInput().toString());
					Collections.sort(secondArgs);
					for (int i = 0; i < secondArgs.size(); i++) {
						for (int j = i+1; j < secondArgs.size(); j++) {
							if (secondArgs.get(i).overlap(secondArgs.get(j)))
								overlap = true;
						}
					}
					for (int i = 0; i < secondArgs.size()-1; i++) {
						if (secondArgs.get(i).end + 1 == secondArgs.get(i).start)
							adjacentSeconArg++;
					}
					if (overlap) {
						secondOverlapNum++;
					}
				}
			}
		}
		System.out.println("max number of second arguments:"+maxNumSecondArg);
		System.out.println("number of second arguments is overlap:"+secondOverlapNum);
		System.out.println("number of second arguments are adjacent:"+adjacentSeconArg);
		return num;
	}

	/**
	 * Count the number of entities that have no relation but overlap with each other under the same entity type
	 * @return
	 */
	public static int countNoRelEntitiesOverlap(RelInstance[] insts) {
		int num = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			List<Span> spans = inst.getOutput().getSpans();
			boolean overlap = false;
			int[] rms = new int[spans.size()];
			for(Relation rel : relations) {
				rms[rel.getArg1Idx()] = -1;
				rms[rel.getArg2Idx()] = -1;
			}
			Span[][] noRelSpans = new Span[inst.getInput().length()][Entity.ENTITIES.size()];
			for (int i = 0; i <rms.length; i++) {
				if (rms[i] == -1) continue;
				if (noRelSpans[spans.get(i).start][spans.get(i).entity.id] != null) {
					overlap = true;
					break;
				} else {
					noRelSpans[spans.get(i).start][spans.get(i).entity.id] = spans.get(i);
				}
			}
			if (overlap) {
				System.out.println(inst.getInput().toString());
				num++;
			}
		}
		return num;
	}
	
	public static int countDiffRelation(RelInstance[] insts) {
		int num = 0;
		
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			List<Span> spans = inst.getOutput().getSpans();
			Map<RelationType, List<Relation>> relMap = new HashMap<>();
			for (Relation relation : relations) {
				if (relMap.containsKey(relation.getRel())) {
					relMap.get(relation.getRel()).add(relation);
				} else {
					List<Relation> list = new ArrayList<>();
					list.add(relation);
					relMap.put(relation.getRel(), list);
				}
			}
		}
		return num;
	}
}
