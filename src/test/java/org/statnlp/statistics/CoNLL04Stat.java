package org.statnlp.statistics;

import java.io.IOException;
import java.util.List;

import org.statnlp.example.nerelation.CoNLL04Reader;
import org.statnlp.example.nerelation.RelInstance;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.Span;

public class CoNLL04Stat {

	public static int countMentions(RelInstance[] insts) {
		int num = 0;
		int numL6 = 0;
		int max = 0;
		for (RelInstance inst : insts) {
			List<Span> spans = inst.getOutput().getSpans();
			for (int i = 0 ; i < spans.size(); i++) {
				Span span = spans.get(i); 
				if (!span.entity.form.equals("O"))
					num++;
				int len = inst.getInput().get(i).getTag().split("/").length;
				if (len > 6)
					numL6++;
				max = Math.max(max, len);
			}
		}
		System.err.println("length > 6: " + numL6);
		System.err.println("maxlength: " + max);
		return num;
	}
	
	public static double calculateRelationDist(RelInstance[] insts) {
		double dist = 0;
		double maxDist = 0;
		int num = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			num += relations.size();
			for (int i = 0; i < relations.size(); i++) {
				dist += Math.abs(relations.get(i).getArg2Idx() - relations.get(i).getArg1Idx()) - 1;
				maxDist = Math.max(maxDist, relations.get(i).getArg2Idx() - relations.get(i).getArg1Idx() - 1);
			}
		}
		System.err.println("max dist: " + maxDist);
		return dist/num;
	}
	
	public static void main(String[] args) throws IOException {
		
		RelInstance[] insts = CoNLL04Reader.read("data/conll04/conll04.corp", -1);
		System.err.println("overlapping relation sents: " +Utils.countOverlapRelationSentences(insts));
		System.err.println("#entities: " +countMentions(insts));
		System.err.println("#relations: " + Utils.countRelations(insts));
		System.err.println("#overlap relations: " + Utils.countOverlapRelationPairs(insts));
		System.err.println("#avg relation dist: " + calculateRelationDist(insts));
	}
}
