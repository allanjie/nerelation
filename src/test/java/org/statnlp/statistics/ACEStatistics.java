package org.statnlp.statistics;

import java.io.IOException;
import java.util.List;

import org.statnlp.example.nerelation.ACEReader;
import org.statnlp.example.nerelation.RelInstance;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.Span;

public class ACEStatistics {

	public static String[] datasets = new String[]{"ACE2004Rel", "ACE2005Rel"};
	
	
	public static int countMentions(RelInstance[] insts) {
		int num = 0;
		int numL6 = 0;
		int max = 0;
		for (RelInstance inst : insts) {
			num += inst.getOutput().getSpans().size();
			for (Span span : inst.getOutput().getSpans()) {
				if (span.end - span.start + 1 > 6)
					numL6++;
				max = Math.max(max, span.end - span.start + 1);
			}
		}
		System.err.println("length > 6: " + numL6);
		System.err.println("maxlength: " + max);
		return num;
	}
	
	public static double calculateRelationDist(RelInstance[] insts) {
		double dist = 0;
		double maxDist = 0;
		int num = 0;
		int bound = 5;
		int boundNum = 0;
		for (RelInstance inst : insts) {
			List<Relation> relations = inst.getOutput().getRelations();
			List<Span> spans = inst.getOutput().getSpans();
			num += relations.size();
			for (int i = 0; i < relations.size(); i++) {
				Span span1 = spans.get(relations.get(i).getArg1Idx());
				Span span2 = spans.get(relations.get(i).getArg2Idx());
				double currDist = 0;
				double spanDist = 0;
				if (span1.overlap(span2)) {
					currDist = 0;
				} else if (span1.compareTo(span2) < 0) {
					currDist = span2.start - span1.end - 1;
					spanDist = spans.indexOf(span2) - spans.indexOf(span1);
				} else {
					currDist = span1.start - span2.end - 1;
					spanDist = spans.indexOf(span1) - spans.indexOf(span2);
				}
				if (spanDist > bound) {
					boundNum++;
				}
				maxDist = Math.max(maxDist, currDist);
				dist += currDist;
			}
		}
		System.err.println("max dist: " + maxDist);
		System.err.println("dist larger than "+bound+": "+boundNum);
		return dist/num;
	}
	
	public static void main(String[] args) throws IOException {
		String dataset1 = "expace04";
		ACEReader.useHeadStart = false;
		ACEReader.useHeadEnd = false;
		RelInstance[] allInsts = ACEReader.read("data/"+dataset1+"/all.data", -1);
		System.err.println("#all: " + allInsts.length);
		System.err.println("#all mentions: " + countMentions(allInsts));
		System.err.println("#all relations: " + Utils.countRelations(allInsts));
		System.err.println("#cross overlap relations: " + Utils.countOverlapRelationPairs(allInsts));
		System.err.println("#exact overlap relations: " + Utils.countExactOverlapRelationPairs(allInsts));
		System.err.println("#right hand side different type same relation: " + Utils.countRightHandSideDiffTypeSameRel(allInsts));
		System.err.println("#left hand side same type same rel diff length: " + Utils.countLeftHandSideSameTypeSameRel(allInsts));
		System.err.println("#relations two args overlap: " + Utils.countNumRelArgsOverlap(allInsts));
		System.err.println("#starting points of first and second argument are the same: " + Utils.countNumRelStartingPointSame(allInsts));
		System.err.println("#starting points of first and second argument are the same, the first one is longer: " + Utils.countNumRelStartingPointSameArg1Longer(allInsts));
		System.err.println("#start same, rel same, but diff ent type for first argument:  " + Utils.countSamePOSSameRelDiffEntofFirstArg(allInsts)); 
		System.err.println("#same rel, same first arg, numOfDiff second arg:  " + Utils.countNumSecondArgOfSameRelSameFirstArg(allInsts)); 
		System.err.println("#no relation entity same type and overlap:  " + Utils.countNoRelEntitiesOverlap(allInsts)); 
		/***
		for (String dataset : datasets) {
			System.err.println("Dataset: " + dataset);
			RelInstance[] trainInsts = ACEReader.read("data/"+dataset+"/train.data", -1);
			RelInstance[] devInsts = ACEReader.read("data/"+dataset+"/dev.data", -1);
			RelInstance[] testInsts = ACEReader.read("data/"+dataset+"/test.data", -1);
			System.err.println("#train: " + trainInsts.length);
			System.err.println("#dev: " + devInsts.length);
			System.err.println("#test: " + testInsts.length);
			System.err.println("#train mentions: " + countMentions(trainInsts));
			System.err.println("#dev mentions: " + countMentions(devInsts));
			System.err.println("#test mentions: " + countMentions(testInsts));
			System.err.println("#train overlap mentions: " + Utils.countOverlapMentionPairs(trainInsts));
			System.err.println("#dev overlap mentions: " + Utils.countOverlapMentionPairs(devInsts));
			System.err.println("#test overlap mentions: " + Utils.countOverlapMentionPairs(testInsts));
			System.err.println("#train sent with o.l. mentions: " + Utils.countOverlapMentionSentences(trainInsts));
			System.err.println("#dev sent with o.l. mentions: " + Utils.countOverlapMentionSentences(devInsts));
			System.err.println("#test sent with o.l. mentions: " + Utils.countOverlapMentionSentences(testInsts));
			
			System.err.println("#train relations: " + Utils.countRelations(trainInsts));
			System.err.println("#dev relations: " + Utils.countRelations(devInsts));
			System.err.println("#test relations: " + Utils.countRelations(testInsts));
			System.err.println("#train relations: ");
			Utils.printAllRelations(trainInsts);
			System.err.println("#dev relations: ");
			Utils.printAllRelations(devInsts);
			System.err.println("#test relations: ");
			Utils.printAllRelations(testInsts);
			System.err.println("#train overlap relations: " + Utils.countOverlapRelationPairs(trainInsts));
			System.err.println("#dev overlap relations: " + Utils.countOverlapRelationPairs(devInsts));
			System.err.println("#test overlap relations: " + Utils.countOverlapRelationPairs(testInsts));
			System.err.println("#train sent with o.l. relattions: " + Utils.countOverlapRelationSentences(trainInsts));
			System.err.println("#dev sent with o.l. relattions: " + Utils.countOverlapRelationSentences(devInsts));
			System.err.println("#test sent with o.l. relattions: " + Utils.countOverlapRelationSentences(testInsts));
			System.err.println();
			System.err.println("#train relations dist: " + calculateRelationDist(trainInsts));
			System.err.println("#dev relations dist: " + calculateRelationDist(devInsts));
			System.err.println("#test relations dist: " + calculateRelationDist(testInsts));
			System.err.println();
			System.err.println();
			System.err.println();
		}
		**/
	}
}
