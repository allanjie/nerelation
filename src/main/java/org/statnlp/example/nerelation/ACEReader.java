package org.statnlp.example.nerelation;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;

public class ACEReader {

	public static boolean useHeadStart = false;
	public static boolean useHeadEnd = false;
	
	/**
	 * This reader only read the mention span, not all the spans include O
	 * We consider the end of a mention is the end point of the head annotation
	 * @param file
	 * @param number
	 * @return
	 * @throws IOException
	 */
	public static RelInstance[] read(String file, int number) throws IOException {
		ArrayList<RelInstance> insts = new ArrayList<RelInstance>();
		String line = null;
		BufferedReader br = RAWF.reader(file);
		int instId = 1;
		int numRelwithSameSpans = 0;
		while ((line = br.readLine())!= null ) {
			String[] words = line.split(" ");
			line = br.readLine(); // read the pos tag.
			String[] tags = line.split(" ");
			WordToken[] wts = new WordToken[words.length];
			for (int t = 0; t < wts.length; t++) {
				wts[t] = new WordToken(words[t], tags[t]);
			}
			Sentence sent = new Sentence(wts);
			String ners = br.readLine();
			List<Span> spans = new ArrayList<>();
			if (!ners.equals("")) {
				String[] spanInfos = ners.split("\\|");
				for (String spanInfo : spanInfos) {
					String[] spanArr = spanInfo.split(" ");
					String type = spanArr[1];
					String[] indices = spanArr[0].split(",");
					/**** Note: the end of span is the end point of the head end. ***/
					int mentionStart = useHeadStart? Integer.valueOf(indices[2]): Integer.valueOf(indices[0]);
					int mentionEnd = useHeadEnd ?  Integer.valueOf(indices[3]) - 1 : Integer.valueOf(indices[1]) - 1;
					Span span = new Span(mentionStart, mentionEnd, 
							Integer.valueOf(indices[2]), Integer.valueOf(indices[3]) - 1, 
							Entity.get(type));
					if (!spans.contains(span)) {
						spans.add(span);
						setHeadIdx(sent, span);
					}
				}
			}
			Collections.sort(spans);
			String allRelations = br.readLine();
			List<Relation> relations = new ArrayList<>();
			if (allRelations.equals("")) {
				
			} else {
				String[] vals = allRelations.split("\\|");
				for (String oneRelation : vals) {
					String[] indices = oneRelation.split(" ");
					String relType = indices[0];
					if (relType.startsWith("Discourse")) continue;
					String[] firstIndices = indices[1].split(",");
					String[] secondIndices = indices[3].split(",");
					int mentionStart = useHeadStart? Integer.valueOf(firstIndices[2]) : Integer.valueOf(firstIndices[0]);
					int mentionEnd = useHeadEnd? Integer.valueOf(firstIndices[3]) - 1: Integer.valueOf(firstIndices[1]) - 1;
					Span span1 = new Span(mentionStart, mentionEnd, 
							Integer.valueOf(firstIndices[2]), Integer.valueOf(firstIndices[3]) - 1, 
							Entity.get(indices[2]));
					mentionStart = useHeadStart? Integer.valueOf(secondIndices[2]) : Integer.valueOf(secondIndices[0]);
					mentionEnd = useHeadEnd? Integer.valueOf(secondIndices[3]) - 1: Integer.valueOf(secondIndices[1]) - 1;
					Span span2 = new Span(mentionStart, mentionEnd, 
							Integer.valueOf(secondIndices[2]), Integer.valueOf(secondIndices[3]) - 1, 
							Entity.get(indices[4]));
					int span1Idx = spans.indexOf(span1);
					int span2Idx = spans.indexOf(span2);
					if (span1Idx < 0 || span2Idx < 0)
						throw new RuntimeException("smaller than 0?");
					if (span1Idx == span2Idx) {
						numRelwithSameSpans++;
						//same span. if useHeadStart false and useHEadEnd false, possibly same span.
						continue;
					}
					Relation relation = new Relation(span1Idx, span2Idx, RelationType.get(relType));
					if (!relations.contains(relation))
						relations.add(relation);
				}
			}
			//Note that we didn't include the NR relation label
			RelInstance inst = new RelInstance(instId, 1.0, sent, new RelStruct(spans, relations));
			insts.add(inst);
			instId++;
			if (number != -1 && insts.size() > number) {
				break;
			}
			spans = new ArrayList<>();
			relations = new ArrayList<>();
			line = br.readLine(); //empty line
		}
		br.close();
		System.out.println("[ignored] number of relations that 1st and 2nd arguments are same:"+numRelwithSameSpans);
		return insts.toArray(new RelInstance[insts.size()]);
	}
	
	/**
	 * Set the head Index according to the paper Zhou et al., 2005. 
	 * @param sent
	 * @param span
	 * @return
	 */
	private static void setHeadIdx (Sentence sent, Span span) {
		//a mention is from the start to the head end according to Zhou 2005
		for (int i = span.start; i <= span.end; i++) {
			if (i > span.start && sent.get(i).getTag().equals("IN")) {
				span.headIdx = i -1;
				return;
			}
		}
		span.headIdx = span.end;
	}
	
	
//	public static void main(String[] args) throws IOException{
//		read("data/ACE2004Rel/train.data", -1);
//	}
	
}
