package org.statnlp.example.nerelation;

import java.util.HashSet;
import java.util.Set;

public class ACE2004Config {
	
	//non-use relation.
	public static final String FINED_DISC = "Discourse::";
	
	/**
	 * Reverse suffix for the reverse relation.
	 */
	public static final String REV_SUFF = "-rev";
	
	/**
	 * Using the discourse relation;
	 */
	public static boolean useDISC = false;
	
	/**
	 * Set the pennTreeRoot to empty.
	 * such that chunk link can read.
	 */
	public static boolean pennTreeRootEmpty = true;
	
	/**
	 * The location of chunklinking script
	 */
	public static String chunkLinkScript = "scripts/chunklink.pl";
	
	/**
	 * Using the Coarsed-grained type for ACE.
	 */
	public static boolean COARSED = true;
	
	/**
	 * Make the persoc relation symmetric as in the data.
	 * Means create one label only.
	 * Better set it to false;
	 */
	public static boolean PERSOC_SYM = false;
	
	/**
	 * Use the head end as the mention end.
	 */
	public static boolean HEAD_END_AS_MENT_END = true;
	
	public static boolean USE_RULE_CONSTRAINT = true;
	
	/**
	 * TODO: Currently we read the rule from data first.
	 * Later we can restrict it follows the rule in guideline.
	 * Currently always make it true first cause if we use predefined rules. 
	 * Many sentences might violates the predefined rules in Chan and Roth 2010.
	 */
	public static final boolean READ_RULE_FROM_DATA = true;
	
	/**define some coarse relation type for constraints for ACE2004**/
	public static final String EMP_ORG = "Employee/Membership/Subsidiary";
	public static final String EMP_ORG_REV = "Employee/Membership/Subsidiary-rev";
	public static final String PHY = "Physical";
	public static final String PHY_REV = "Physical-rev";
	public static final String PER_ORG_AFF = "PER/ORG-Affiliation";
	public static final String PER_ORG_AFF_REV = "PER/ORG-Affiliation-rev";
	public static final String PER_SOC = "Personal/Social"; //symmetric as in ACE2004 guideline
	public static final String PER_SOC_REV = "Personal/Social-rev"; //symmetric as in ACE2004 guideline
	public static final String GPE_AFF = "GPE-Affiliation";
	public static final String GPE_AFF_REV = "GPE-Affiliation-rev";
	public static final String ART = "Agent-Artifact";
	public static final String ART_REV = "Agent-Artifact-rev";
	public static final String DISC = "Discourse";
	public static final String DISC_REV = "Discourse-rev";
	public static final String NR = "NR";
	
	/** Entity Type in ACE2004 ***/
	public static final String PER = "PER";
	public static final String ORG = "ORG";
	public static final String LOC = "LOC";
	public static final String GPE = "GPE";
	public static final String FAC = "FAC";
	public static final String VEH = "VEH";
	public static final String WEA = "WEA";
	
	/**
	 * Adding rules according to the ACE 2004 annotation guidelines
	 * @return
	 */
	public static Set<String> predefineRules (boolean addDISC, boolean persoc_sym) {
		Set<String> rules = new HashSet<>();
		//EMP_ORG
		String[] a1s = new String[]{GPE, ORG, PER};
		String[] a2s = new String[]{FAC, GPE, VEH, WEA};
		for (String a1 : a1s) {
			for (String a2 : a2s) {
				rules.add(ART+","+a1+","+a2);
				rules.add(ART_REV+","+a2+","+a1);
			}
		}
		a1s = new String[]{GPE, ORG, PER};
		a2s = new String[]{GPE, ORG, PER};
		for (String a1 : a1s) {
			for (String a2 : a2s) {
				rules.add(EMP_ORG+","+a1+","+a2);
				rules.add(EMP_ORG_REV+","+a2+","+a1);
			}
		}
		a1s = new String[]{GPE, ORG, PER};
		a2s = new String[]{GPE, LOC};
		for (String a1 : a1s) {
			for (String a2 : a2s) {
				rules.add(GPE_AFF+","+a1+","+a2);
				rules.add(GPE_AFF_REV+","+a2+","+a1);
				rules.add(PER_ORG_AFF+","+a1+","+a2);
				rules.add(PER_ORG_AFF_REV+","+a2+","+a1);
			}
		}
		rules.add(PER_SOC+","+PER+","+PER);
		if (persoc_sym)
			rules.add(PER_SOC_REV+","+PER+","+PER);
		a1s = new String[]{PER, ORG, GPE, LOC, FAC, VEH, WEA};
		for (String a1 : a1s) {
			for (String a2 : a1s) {
				rules.add(PHY+","+a1+","+a2);
				rules.add(PHY_REV+","+a2+","+a1);
				if (addDISC) {
					rules.add(DISC+","+a1+","+a2);
					rules.add(DISC+","+a2+","+a1);
				}
				rules.add(NR+","+a1+","+a2);
			}
		}
		return rules;
	}
	
}
