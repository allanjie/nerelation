package org.statnlp.example.nerelation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MentionType implements Comparable<MentionType>, Serializable{
	
	private static final long serialVersionUID = -3314363044582374266L;
	public static final Map<String, MentionType> MENTIONS = new HashMap<String, MentionType>();
	public static final Map<Integer, MentionType> MENTIONS_INDEX = new HashMap<Integer, MentionType>();
	private static boolean locked = false;
	
	public static MentionType get(String form){
		if(!MENTIONS.containsKey(form)){
			if (!locked) {
				MentionType label = new MentionType(form, MENTIONS.size());
				MENTIONS.put(form, label);
				MENTIONS_INDEX.put(label.id, label);
			} else {
				throw new RuntimeException("the map is locked");
			}
		}
		return MENTIONS.get(form);
	}
	
	public static MentionType get(int id){
		return MENTIONS_INDEX.get(id);
	}
	
	public static void lock () {
		locked = true;
	}
	
	public String form;
	public int id;
	
	private MentionType(String form, int id) {
		this.form = form;
		this.id = id;
	}

	@Override
	public int hashCode() {
		return form.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MentionType))
			return false;
		MentionType other = (MentionType) obj;
		if (form == null) {
			if (other.form != null)
				return false;
		} else if (!form.equals(other.form))
			return false;
		return true;
	}
	
	public String toString(){
		return String.format("%s(%d)", form, id);
	}

	@Override
	public int compareTo(MentionType o) {
		return Integer.compare(id, o.id);
	}
	
	public static int compare(MentionType o1, MentionType o2){
		if(o1 == null){
			if(o2 == null) return 0;
			else return -1;
		} else {
			if(o2 == null) return 1;
			else return o1.compareTo(o2);
		}
	}
}
