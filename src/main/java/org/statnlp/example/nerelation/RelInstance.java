package org.statnlp.example.nerelation;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseInstance;

/**
 * RelInstance: the instance for joint prediction containing 
 * the entity spans and also the relations.
 * @author allan (allanmcgrady@gmail.com)
 *
 */
public class RelInstance extends BaseInstance<RelInstance, Sentence, RelStruct> {

	public RelInstance(int instanceId, double weight) {
		super(instanceId, weight);
	}
	
	public RelInstance(int instanceId, double weight, Sentence input, RelStruct output) {
		super(instanceId, weight);
		this.input = input;
		this.output = output;
	}

	private static final long serialVersionUID = -8632299607648533554L;

	@Override
	public int size() {
		return this.input.length();
	}

}
