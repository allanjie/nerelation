package org.statnlp.example.nerelation.logistic;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.example.nerelation.ACE2004Config;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.MentionType;
import org.statnlp.example.nerelation.RESentence;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkModel;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class LGMain {

	
	public static double trainRatio = 1; //used for splitting conll04 corpus. from 0 to 1
	public static double l2val = 0.01;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static int totalNumInst = 2;
	public static String ace2004 = "data/ACE2004Folds"; //including newswire and bnews data
	public static String ace2004Chunk = "data/ACE2004FoldsChunk"; //including newswire and bnews data
	public static int foldNum = 5; //total fold number, fixed to 5 for now
	public static int numInstsInFold = -1;
	public static boolean useDISC = true;
	public static Set<String> rules;
	public static boolean useHeadEnd = true;
	
	/**
	 * Entities: {Loc=Loc(4), Peop=Peop(2), Org=Org(3), Other=Other(0), O=O(1)}
	 * Relations:{OrgBased_In=OrgBased_In(1), NR=NR(7), Located_In=Located_In(2), 
	 * Contains=Contains(3), Work_For=Work_For(4), Killed_By=Killed_By(6), Live_In=Live_In(0), Kill=Kill(5)}
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		
		setArgs(args);
		
		/***
		 * Parameter settings and model configuration
		 */
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.PARALLEL_FEATURE_EXTRACTION = true;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = false;
		ACE2004Config.useDISC = useDISC;
		ACE2004Config.PERSOC_SYM = false;
		ACE2004Config.HEAD_END_AS_MENT_END = useHeadEnd;
		ACE2004Config.USE_RULE_CONSTRAINT = true;
		
		/**
		 * 5-fold validation.
		 */
		RESentence[][] lgsents = LGReader.readFoldsChunk(ace2004Chunk, foldNum);
		if (ACE2004Config.USE_RULE_CONSTRAINT) {
			if (!ACE2004Config.READ_RULE_FROM_DATA) {
				rules = ACE2004Config.predefineRules(ACE2004Config.useDISC, ACE2004Config.PERSOC_SYM);
			} else {
				rules = new HashSet<>();
			}
		} else {
			rules = null;
		}
		double[] allMetrics = new double[3];
		for (int f = 1; f <= foldNum; f++) {
			Set<String> currRule = f == 1? rules : null;
			LGInstance[][] data = LGReader.readFolds(ace2004, f, lgsents, currRule, numInstsInFold);
			System.out.println("#Relations: " + RelationType.RELS.size());
			System.out.println("Relations: " + RelationType.RELS.toString());
			System.out.println("Entity: " + Entity.ENTITIES.toString());
			//System.out.println("Rules: " + rules.toString());
			Entity.lock();
			RelationType.lock();
			MentionType.lock();
			GlobalNetworkParam gnp = new GlobalNetworkParam(OptimizerFactory.getLBFGSFactory());
			LGFeatureManager tfm = new LGFeatureManager(gnp);
			LGNetworkCompiler tnc = new LGNetworkCompiler(rules);
			NetworkModel model = DiscriminativeNetworkModel.create(tfm, tnc);
			LGInstance[] trainInsts  = data[0];
			model.train(trainInsts, numIteration);
			/**
			 * Testing Phase
			 */
			LGInstance[] testInsts = data[1];
			Instance[] results = model.decode(testInsts);
			double[] metrics = LGEval.evaluate(results);
			for (int i = 0; i< allMetrics.length; i++) {
				allMetrics[i] += metrics[i];
			}
		}
		for (int i = 0; i < allMetrics.length; i++) {
			allMetrics[i] /= foldNum;
		}
		System.out.printf("[Result] Final: \t\tPrec.:%.2f%%\tRec.:%.2f%%\tF1.:%.2f%%\n", 
				allMetrics[0], allMetrics[1], allMetrics[2]);
	}
	
	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Logistic Regression Model for Relation Extraction");
		parser.addArgument("-t", "--thread").setDefault(8).help("number of threads");
		parser.addArgument("--numInstsInFold").setDefault(numInstsInFold).help("number of instances in each fold");
		parser.addArgument("--l2").setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--disc").action(Arguments.storeTrue()).setDefault(false).help("include disc or not");
		parser.addArgument("--headEnd").action(Arguments.storeTrue()).setDefault(false).help("use head as mention end.");
		parser.addArgument("--iter").setDefault(numIteration).help("The number of iteration.");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = Integer.valueOf(ns.getString("thread"));
        numInstsInFold = Integer.valueOf(ns.getString("numInstsInFold"));
        l2val = Double.valueOf(ns.getString("l2"));
        useDISC = ns.getBoolean("disc");
        useHeadEnd = ns.getBoolean("headEnd");
        numIteration = Integer.valueOf(ns.getString("iter"));
        System.err.println(ns.getAttrs().toString());
	}
}
