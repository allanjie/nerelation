package org.statnlp.example.nerelation.logistic;

import java.util.List;

import org.statnlp.example.nerelation.RESentence;
import org.statnlp.example.nerelation.Span;

public class LGInput {

	protected RESentence sent;
	
	/**
	 * Only consider the entity span.
	 */
	protected List<Span> spans;
	
	/**
	 * The index is the index in the span.
	 */
	protected int leftSpanIdx;
	protected int rightSpanIdx;
	
	/**
	 * Input Class to logistic regression model.
	 * @param sent
	 * @param spans
	 * @param leftSpanIdx: may not be the arg1Idx, depends on the label
	 * @param rightSpanIdx: may not be the arg2idx, depends on the label
	 */
	public LGInput(RESentence sent, List<Span> spans, int leftSpanIdx, int rightSpanIdx) {
		this.sent = sent;
		this.spans = spans;
		this.leftSpanIdx = leftSpanIdx;
		this.rightSpanIdx = rightSpanIdx;
	}

}
