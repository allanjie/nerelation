package org.statnlp.example.nerelation.logistic;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class LGNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -3523483926592577270L;

	public enum NodeTypes {LEAF,NODE,ROOT};
	public int _size;
	private boolean DEBUG = false;
	
	/**
	 * A global mappign for the rules
	 */
	private Set<String> rules;
	
	public LGNetworkCompiler(Set<String> rules) {
		this.rules = rules;
		NetworkIDMapper.setCapacity(new int[]{3, RelationType.RELS.size(), 3});
	}

	private long toNode_leaf() {
		return NetworkIDMapper.toHybridNodeID(new int[]{0, 0, NodeTypes.LEAF.ordinal()});
	}
	
	private long toNode(int label) {
		return NetworkIDMapper.toHybridNodeID(new int[]{1, label, NodeTypes.NODE.ordinal()});
	}
	
	private long toNode_root() {
		return NetworkIDMapper.toHybridNodeID(new int[]{2, 0, NodeTypes.ROOT.ordinal()});
	}
	
	@Override
	public Network compileLabeled(int networkId, Instance myInst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> networkBuilder = NetworkBuilder.builder();
		LGInstance inst = (LGInstance)myInst;
		long leaf = toNode_leaf();
		long[] children = new long[]{leaf};
		networkBuilder.addNode(leaf);
		long node = toNode(inst.getOutput().id);
		networkBuilder.addNode(node);
		networkBuilder.addEdge(node, children);
		children = new long[]{node};
		long root = toNode_root();
		networkBuilder.addNode(root);
		networkBuilder.addEdge(root, children);
		BaseNetwork network = networkBuilder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = (BaseNetwork)this.compileUnlabeled(networkId, inst, param);
			if(!unlabeled.contains(network))
				System.err.println("not contains");
		}
		return network;
	}
	
	@Override
	public Instance decompile(Network network) {
		BaseNetwork lgNet = (BaseNetwork)network;
		LGInstance inst = (LGInstance)network.getInstance();
		long node = this.toNode_root();
		int nodeIdx = Arrays.binarySearch(lgNet.getAllNodes(), node);
		int labeledNodeIdx = lgNet.getMaxPath(nodeIdx)[0];
		int[] arr = lgNet.getNodeArray(labeledNodeIdx);
		int labelId = arr[1];
		inst.setPrediction(RelationType.get(labelId));
		return inst;
	}



	@Override
	public Network compileUnlabeled(int networkId, Instance myInst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> networkBuilder = NetworkBuilder.builder();
		LGInstance inst = (LGInstance)myInst;
		LGInput input = inst.getInput();
		List<Span> spans = input.spans;
		long leaf = toNode_leaf();
		long[] leaves = new long[]{leaf};
		networkBuilder.addNode(leaf);
		long root = toNode_root();
		networkBuilder.addNode(root);
		String leftForm = spans.get(input.leftSpanIdx).entity.form;
		String rightForm = spans.get(input.rightSpanIdx).entity.form;
		//make sure the RelationType map contains the NR relation also
		for (int l = 0; l < RelationType.RELS.size(); l++) {
			long node = this.toNode(l);
			if (rules != null) {
				String rule = RelationType.get(l).form + "," + leftForm + "," + rightForm;
				if (!rules.contains(rule)) continue;
			}
			networkBuilder.addNode(node);
			networkBuilder.addEdge(node, leaves);
			networkBuilder.addEdge(root, new long[]{node});
		}
		return networkBuilder.build(networkId, myInst, param, this);
	}


}
