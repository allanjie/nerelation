package org.statnlp.example.nerelation.logistic;

import org.statnlp.example.base.BaseInstance;
import org.statnlp.example.nerelation.RelationType;

public class LGInstance extends BaseInstance<LGInstance, LGInput, RelationType> {

	private static final long serialVersionUID = 6971863806062822950L;
	
	public LGInstance(int instanceId, double weight) {
		super(instanceId, weight);
	}

	public LGInstance(int instanceId, double weight, LGInput input, RelationType output) {
		this(instanceId, weight);
		this.input = input;
		this.output = output;
	}


	@Override
	public int size() {
		return 2;
//		throw new NetworkException ("The instance does not have size.");
	}
	
	public LGInput duplicateInput(){
		return input;
	}
	
	public RelationType duplicateOutput() {
		return this.output;
	}

}
