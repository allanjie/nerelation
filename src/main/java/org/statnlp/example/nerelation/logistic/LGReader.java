package org.statnlp.example.nerelation.logistic;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.nerelation.ACE2004Config;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.MentionType;
import org.statnlp.example.nerelation.RESentence;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;
import org.statnlp.example.nerelation.struct.DependencyTree;

import cern.colt.Arrays;

public class LGReader {

	/**
	 * This reader only read the mention span, not all the spans include O
	 * @param file
	 * @param number
	 * @return
	 * @throws IOException
	 */
	public static LGInstance[] read(String file, boolean isLabeled, int number) throws IOException {
		List<LGInstance> insts = readList(file, null, isLabeled, number, null);
		return insts.toArray(new LGInstance[insts.size()]);
	}
	
	public static LGInstance[] read(String file, boolean isLabeled, int number, Set<String> rules) throws IOException {
		List<LGInstance> insts = readList(file, null, isLabeled, number, rules);
		return insts.toArray(new LGInstance[insts.size()]);
	}
	
	/**
	 * This reader only read the mention span, not all the spans include O
	 * @param file
	 * @param number
	 * @return
	 * @throws IOException
	 */
	private static List<LGInstance> readList(String file, RESentence[] sents, boolean isLabeled, int number, Set<String> rules) throws IOException {
		ArrayList<LGInstance> insts = new ArrayList<LGInstance>();
		String line = null;
		BufferedReader br = RAWF.reader(file);
		int instId = 1;
		int preSentId = 0;
		int numSent = 0;
		int numRel = 0;
		int numEntityPair = 0;
		int numMentions = 0;
		while ((line = br.readLine())!= null ) {
			String[] words = line.split(" ");
			line = br.readLine(); // read the pos tag.
			String[] tags = line.split(" ");
			WordToken[] wts = new WordToken[words.length];
			for (int t = 0; t < wts.length; t++) {
				wts[t] = new WordToken(words[t], tags[t]);
			}
			String pennStr = br.readLine();
			if (ACE2004Config.pennTreeRootEmpty)
				pennStr = pennStr.replaceFirst("ROOT", "");
			RESentence sent = new RESentence(wts, pennStr);
			line = br.readLine();
			String[] headIdxs = line.split(" ");
			line = br.readLine();
			String[] depLabels = line.split(" ");
			for (int t = 0; t < wts.length; t++) {
				wts[t].setHead(Integer.valueOf(headIdxs[t])); //might be -1, means root
				wts[t].setDepLabel(depLabels[t]);
			}
			sent.depTree = new DependencyTree(sent);
			if (sents != null) {
				RESentence preSent = sents[preSentId];
				if (words.length != preSent.length()) {
					System.err.println(Arrays.toString(words));
					System.err.println(words.length + "," + preSent.length() + " "+ preSent.toString());
					throw new RuntimeException("lengths are not equal");
				}
				for (int t = 0; t < wts.length; t++) {
					wts[t].setChunk(preSent.get(t).getChunk());
					wts[t].setChunkHead(preSent.get(t).getChunkHead());
					wts[t].setIobChain(preSent.get(t).getIobChain());
				}
			}
			String ners = br.readLine();
			List<Span> spans = new ArrayList<>();
			if (!ners.equals("")) {
				String[] spanInfos = ners.split("\\|");
				for (String spanInfo : spanInfos) {
					String[] spanArr = spanInfo.split(" ");
					/**contains the "entity type, mention type, and entity subtype".
					 * Note ACE2004 PER entity does not have the subtype information.
					 * ***/
					String[] types = spanArr[1].split(",");
					String[] indices = spanArr[0].split(",");
					String entSubType = types.length == 2 ? types[0] : types[2];
					/**** Note: the end of span is the end point of the head end. ***/
					int end = ACE2004Config.HEAD_END_AS_MENT_END ? 
							Integer.valueOf(indices[3]) - 1 : Integer.valueOf(indices[1]) - 1;
					Span span = new Span(Integer.valueOf(indices[0]), end, 
							Entity.get(types[0]), MentionType.get(types[1]), entSubType);
					setHeadIdx(sent, span);
					if (!spans.contains(span)) {
						spans.add(span);
					}
				}
				Collections.sort(spans);
			}
			preSentId++;
			numSent++;
			numMentions += spans.size();
			String allRelations = br.readLine();
			int[][] filled = new int[spans.size()][spans.size()];
			if (spans.size() == 0) {
				numSent--;
			}
			if (allRelations.equals("")) {
				
			} else {
				String[] vals = allRelations.split("\\|");
				for (String oneRelation : vals) {
					numRel++;
					String[] indices = oneRelation.split(" ");
					String relType = indices[0];
					String[] arg1MentionTypes = indices[2].split(",");
					String[] arg2MentionTypes = indices[4].split(",");
					String[] firstIndices = indices[1].split(",");
					String[] secondIndices = indices[3].split(",");
					int end1 = ACE2004Config.HEAD_END_AS_MENT_END ?
							Integer.valueOf(firstIndices[3]) - 1 : Integer.valueOf(firstIndices[1]) - 1;
					int end2 = ACE2004Config.HEAD_END_AS_MENT_END ?
							Integer.valueOf(secondIndices[3]) - 1 : Integer.valueOf(secondIndices[1]) - 1;
					Span span1 = new Span(Integer.valueOf(firstIndices[0]), end1, 
							Entity.get(arg1MentionTypes[0]), MentionType.get(arg1MentionTypes[1]));
					Span span2 = new Span(Integer.valueOf(secondIndices[0]), end2, 
							Entity.get(arg2MentionTypes[0]), MentionType.get(arg2MentionTypes[1]));
					int span1Idx = spans.indexOf(span1);
					int span2Idx = spans.indexOf(span2);
					int leftIdx = span1Idx < span2Idx ? span1Idx : span2Idx;
					int rightIdx = span1Idx < span2Idx ? span2Idx : span1Idx;
					filled[leftIdx][rightIdx] = 1;
					LGInput input = new LGInput(sent, spans, leftIdx, rightIdx);
					String coarsedType = null;
					if (ACE2004Config.COARSED) {
						coarsedType = relType.split("::")[0];
						relType = coarsedType;
						if (relType.equals(ACE2004Config.DISC) && !ACE2004Config.useDISC) {
							relType = ACE2004Config.NR;
						}
					} else {
						if (relType.equals(ACE2004Config.FINED_DISC) && !ACE2004Config.useDISC) {
							relType = ACE2004Config.NR;
						}
					}
					//define the direction.
					/**means the reversed direction for the relation**/
					String direction = span1Idx < span2Idx ? "" : ACE2004Config.REV_SUFF;
					if (ACE2004Config.PERSOC_SYM && relType.startsWith(ACE2004Config.PER_SOC)) {
						direction = "";
					}
					relType = relType.equals(ACE2004Config.NR) ? relType : relType + direction;
					RelationType relationType = RelationType.get(relType);
					/***  
					if (relType.equals(LGConfig.ART) && spans.get(leftIdx).entity.form.equals(LGConfig.PER) && spans.get(rightIdx).entity.form.equals(LGConfig.FAC)) {
						System.out.println(sent.toString());
					}
					***/
					if (rules != null  && ACE2004Config.READ_RULE_FROM_DATA) {
						rules.add(relationType.form+","+spans.get(leftIdx).entity.form+","+spans.get(rightIdx).entity.form);
					}
					LGInstance inst = new LGInstance(instId, 1.0, input, relationType);
					if (isLabeled)
						inst.setLabeled();
					else inst.setUnlabeled();
					insts.add(inst);
					instId++;
					if (span1Idx < 0 || span2Idx < 0)
						throw new RuntimeException("smaller than 0?");
				}
			}
			for (int i = 0; i < spans.size(); i++) {
				for (int j = i + 1; j < spans.size(); j++) {
					numEntityPair++;
					if (filled[i][j] == 0) {
						LGInput input = new LGInput(sent, spans, i, j);
						RelationType nr  = RelationType.get(ACE2004Config.NR);
						LGInstance inst = new LGInstance(instId, 1.0, input, nr);
						if (rules != null && ACE2004Config.READ_RULE_FROM_DATA) {
							rules.add(nr.form+","+spans.get(i).entity.form+","+spans.get(j).entity.form);
						}
						if (isLabeled) {
							inst.setLabeled();
						} else {
							inst.setUnlabeled();
						}
						insts.add(inst);
						instId++;
					}
				}
			}
			if (number != -1 && insts.size() > number) {
				break;
			}

			spans = new ArrayList<>();
			line = br.readLine(); //empty line
		}
		br.close();
		RelationType.get(ACE2004Config.NR);
		System.out.println("number of sents with entities: " + numSent);
		System.out.println("number of relations:" + numRel);
		System.out.println("number of mention pairs:" + numEntityPair);
		System.out.println("number of mentions:" + numMentions);
		return insts;
	}
	
	/**
	 * Read the fold file in a folder, the folder should only contains file
	 * This method read all folds and make the testFold^{th} as the test Fold. 
	 * without the fold sentences chunk
	 * @param folder
	 * @param testFold
	 * @return
	 * @throws IOException
	 */
	public static LGInstance[][] readFolds(String folder, int testFold) throws IOException {
		return readFolds(folder, testFold, null);
	}
	
	public static LGInstance[][] readFolds(String folder, int testFold, RESentence[][] foldSents) throws IOException {
		return readFolds(folder, testFold, foldSents, null, -1);
	}
	
	public static LGInstance[][] readFolds(String folder, int testFold, RESentence[][] foldSents, Set<String> rules) throws IOException {
		return readFolds(folder, testFold, foldSents, rules, -1);
	}
	
	/**
	 * Read the fold file in a folder, the folder should only contains file
	 * This method read all folds and make the testFold^{th} as the test Fold.
	 * @param folder
	 * @param testFold: indicate which fold should be the testFold, starting from 1.
	 * @param foldSents: the preread sentence with chunk information
	 * @param numInstInFold: number of instances to read in each fold;
	 * @return
	 */
	public static LGInstance[][] readFolds(String folder, int testFold, RESentence[][] foldSents, Set<String> rules, int numInstInFold) throws IOException {
		File aceDir = new File(folder);
		List<LGInstance> trainInsts = new ArrayList<>();
		List<LGInstance> testInsts = new ArrayList<>();
		LGInstance[][] data = new LGInstance[2][];
		for(File subdir: aceDir.listFiles()){
			if(subdir.isDirectory()){
				continue;
			}
			boolean labeled = true;
			String[] names = subdir.getName().split("\\.");
			int idx = Integer.valueOf(names[1]);
			if (idx == testFold)
				labeled = false;
			RESentence[] sents = foldSents!=null? foldSents[idx - 1] : null;
			System.out.println("Reading file "+subdir.getAbsolutePath());
			List<LGInstance> insts = readList(subdir.getAbsolutePath(), sents, labeled, numInstInFold, rules);
			if (labeled) {
				trainInsts.addAll(insts);
			} else {
				testInsts = insts;
				data[1] = testInsts.toArray(new LGInstance[testInsts.size()]);
			}
		}
		data[0] =  trainInsts.toArray(new LGInstance[trainInsts.size()]);
		return data;
	}
	
	/**
	 * To read the chunk file.
	 * @param folder
	 * @return
	 * @throws IOException
	 */
	public static RESentence[][] readFoldsChunk(String folder, int totalFoldNum) throws IOException{
		File aceDir = new File(folder);
		RESentence[][] foldSents = new RESentence[totalFoldNum][];
		for(File subdir: aceDir.listFiles()){
			if(subdir.isDirectory()){
				continue;
			}
			String[] names = subdir.getName().split("\\.");
			int idx = Integer.valueOf(names[1]);
			System.out.println("Reading file "+subdir.getAbsolutePath());
			List<RESentence> sents = readChunks(subdir.getAbsolutePath());
			foldSents[idx - 1] =  sents.toArray(new RESentence[sents.size()]);
		}
		return foldSents;
	}
	
	/**
	 * Read the chunks in the chunk file
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private static List<RESentence> readChunks (String file) throws IOException {
		List<RESentence> sents = new ArrayList<>();
		String line = null;
		BufferedReader br = RAWF.reader(file);
		List<WordToken> wtList = new ArrayList<>();
		while ((line = br.readLine()) != null) {
			line = line.trim();
			if (line.startsWith("#")) continue;
			if (line.equals("")) {
				RESentence sent = new RESentence(wtList.toArray(new WordToken[wtList.size()]));
				sents.add(sent);
				wtList = new ArrayList<>();
			} else {
				String[] vals = line.split("\\s+");
				String word = vals[5];
				String chunk = vals[3];
				String[] iobChain = vals[9].split("/");
				int wId = Integer.valueOf(vals[2]);
				int chunkHead = !vals[8].matches("[0-9]+")? wId : Integer.valueOf(vals[8]);
				WordToken wt = new WordToken(word);
				wt.setChunk(chunk);
				wt.setChunkHead(chunkHead);
				wt.setIobChain(iobChain);
				wtList.add(wt);
			}
		}
		br.close();
		return sents;
	}
	
	/**
	 * Set the head Index according to the paper Zhou et al., 2005. 
	 * @param sent
	 * @param span
	 * @return
	 */
	private static void setHeadIdx (Sentence sent, Span span) {
		//a mention is from the start to the head end according to Zhou 2005
		for (int i = span.start; i <= span.end; i++) {
			if (i > span.start && sent.get(i).getTag().equals("IN")) {
				span.headIdx = i -1;
				return;
			}
		}
		span.headIdx = span.end;
	}
	
//	public static void main(String[] args) throws IOException{
//		LGConfig.pennTreeRootEmpty  = true;
//		LGInstance[] insts = read("data/ACE2004Folds/fold.1.data", true, -1);
//		System.err.println("Number of Instances: " + insts.length);
//		System.err.println("Number of Instances: " + insts.length);
//		readChunks("data/ACE2004FoldsChunk/fold1.chunk");
//	}


}
