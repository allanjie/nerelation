package org.statnlp.example.nerelation.logistic;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.nerelation.ACE2004Config;
import org.statnlp.example.nerelation.RelationType;

public class LGEval {

	
	/**
	 * Evaluate the relation
	 * @param results
	 * @return the precision, recall and fscore
	 */
	public static double[] evaluate(Instance[] results) {
		//calculating the precision fist.
		int[] p = new int[RelationType.RELS.size()];
		int[] totalPredict = new int[RelationType.RELS.size()];
		int[] totalInData = new int[RelationType.RELS.size()];
		double[] metrics = new double[3];
		for (Instance inst : results) {
			LGInstance res = (LGInstance)inst;
			RelationType prediction = res.getPrediction();
			RelationType gold = res.getOutput();
			String predForm = prediction.form;
			String goldForm = gold.form;
			int corrPredId = prediction.id;
			int corrGoldId = gold.id;
			if (predForm.endsWith(ACE2004Config.REV_SUFF)) {
				corrPredId = RelationType.get(predForm.replace(ACE2004Config.REV_SUFF, "")).id;
			}
			if (goldForm.endsWith(ACE2004Config.REV_SUFF)) {
				corrGoldId = RelationType.get(goldForm.replace(ACE2004Config.REV_SUFF, "")).id;
			}
			if (prediction.equals(gold)) {
				p[corrPredId]++;
			}
			totalPredict[corrPredId]++;
			totalInData[corrGoldId]++;
		}
		
		int allP = 0;
		int allPredict = 0;
		int allInData = 0;
		for (int r = 0; r < RelationType.RELS.size(); r++) {
			if (RelationType.get(r).form.equals(ACE2004Config.NR) || RelationType.get(r).form.endsWith(ACE2004Config.REV_SUFF)) continue;
			double precision = p[r] * 1.0/totalPredict[r] * 100;
			double recall = p[r] * 1.0 / totalInData[r] * 100;
			double fscore = 2.0 * p[r] / (totalPredict[r] + totalInData[r]) * 100;
			String spacing = "\t";
			System.out.printf("[Result] %s: %sPrec.:%.2f%%\tRec.:%.2f%%\tF1.:%.2f%%\n", 
					RelationType.get(r).form, spacing, precision, recall, fscore);
			allP += p[r];
			allPredict += totalPredict[r];
			allInData += totalInData[r];
		}
		double precision = allP * 1.0/ allPredict * 100;
		double recall = allP * 1.0 / allInData * 100;
		double fscore = 2.0 * allP / (allPredict + allInData) * 100;
		metrics[0] = precision;
		metrics[1] = recall;
		metrics[2] = fscore;
		System.out.printf("[Result] All: \t\tPrec.:%.2f%%\tRec.:%.2f%%\tF1.:%.2f%%\n", 
				precision, recall, fscore);
		return metrics;
	}
	
//	public static void main(String[] args) throws IOException {
//		LGInstance[] insts = LGReader.read("data/ACE2004Rel/train.data", true, -1);
//		System.out.println("Entities: " + Entity.ENTITIES.toString());
//		System.out.println("Relations:" + RelationType.RELS.toString());
//		Random rand = new Random(1234);
//		for (LGInstance inst : insts) {
//			int relId = rand.nextInt(RelationType.RELS.size());
//			inst.setPrediction(RelationType.get(relId));
//		}
//		evaluate(insts);
//	}
	
}
