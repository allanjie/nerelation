package org.statnlp.example.nerelation;

import java.util.List;

public class RelStruct {

	List<Span> spans;
	List<Relation> relations;
	
	public RelStruct(List<Span> spans, List<Relation> relations) {
		this.spans = spans;
		this.relations = relations;
	}

	public List<Span> getSpans() {
		return spans;
	}

	public void setSpans(List<Span> spans) {
		this.spans = spans;
	}

	public List<Relation> getRelations() {
		return relations;
	}

	public void setRelations(List<Relation> relations) {
		this.relations = relations;
	}
	
	
	
}
