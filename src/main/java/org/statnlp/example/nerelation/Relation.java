package org.statnlp.example.nerelation;

public class Relation {

	int arg1Idx;
	int arg2Idx;
	RelationType rel;
	
	public Relation(int arg1Idx, int arg2Idx, RelationType rel) {
		super();
		this.arg1Idx = arg1Idx;
		this.arg2Idx = arg2Idx;
		this.rel = rel;
	}
	
	/**
	 * Note that please use this within the same sentence.
	 * @param other
	 * @return
	 */
	public boolean overlap(Relation other) {
		if (arg1Idx == other.arg1Idx || arg1Idx == other.arg2Idx ||
				arg2Idx == other.arg2Idx || arg2Idx == other.arg1Idx) return true;
		else return false;
	}
	
	public boolean exactOverlap(Relation other) {
		int r1Left = Math.min(arg1Idx, arg2Idx);
		int r1Right = Math.max(arg1Idx, arg2Idx);
		int r2Left = Math.min(other.arg1Idx, other.arg2Idx);
		int r2Right = Math.max(other.arg1Idx, other.arg2Idx);
		if (r1Left == r2Left && r1Right == r2Right) return true;
		else return false;
	}

	
	
	public int getArg1Idx() {
		return arg1Idx;
	}

	public void setArg1Idx(int arg1Idx) {
		this.arg1Idx = arg1Idx;
	}

	public int getArg2Idx() {
		return arg2Idx;
	}

	public void setArg2Idx(int arg2Idx) {
		this.arg2Idx = arg2Idx;
	}

	public RelationType getRel() {
		return rel;
	}

	public void setRel(RelationType rel) {
		this.rel = rel;
	}

	@Override
	public String toString() {
		return "Relation [arg1Idx=" + arg1Idx + ", arg2Idx=" + arg2Idx + ", rel=" + rel + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + arg1Idx;
		result = prime * result + arg2Idx;
		result = prime * result + ((rel == null) ? 0 : rel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Relation other = (Relation) obj;
		if (arg1Idx != other.arg1Idx)
			return false;
		if (arg2Idx != other.arg2Idx)
			return false;
		if (rel == null) {
			if (other.rel != null)
				return false;
		} else if (!rel.equals(other.rel))
			return false;
		return true;
	}
	
	
	
}
