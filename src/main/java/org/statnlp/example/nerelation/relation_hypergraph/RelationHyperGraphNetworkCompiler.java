package org.statnlp.example.nerelation.relation_hypergraph;

import java.util.List;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.nerelation.RelStruct;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.Span;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkException;
import org.statnlp.hypergraph.NetworkIDMapper;

/**
 * NetworkCompiler for the Model I.
 * In this model, the instance should be already converted to reverse direction as well.
 * Namely, the first argument should be always starting earlier than or same position as the second argument 
 * @author allanjie
 *
 */
public class RelationHyperGraphNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -1615827922429731961L;
	public static final boolean DEBUG = false;
	
	public int maxSize = 200;
	public BaseNetwork unlabeledNetwork;

	public enum NodeType{
		X_NODE,
		I_0_c_NODE,
		I_1_NODE,
		Z_NODE,
		I_0_NODE,
		T_NODE, //only for first argument
		R_NODE,
		E_NODE,
		A_NODE,
	}
	
	public RelationHyperGraphNetworkCompiler(int maxSize) {
		NetworkIDMapper.setCapacity(new int[]{maxSize, 10, 100, 100, 2});
		this.maxSize = maxSize;
	}

	@Override
	public Network compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		RelationHyperGraphInstance relInst = (RelationHyperGraphInstance)inst;
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder();
		int size = relInst.size();
		long xNode = toNode_X();
		builder.addNode(xNode);
		
		RelStruct output = relInst.getOutput();
		List<Relation> rels = output.getRelations();
		List<Span> spans = output.getSpans();
		for (Relation rel : rels) {
			int arg1SpanIdx = rel.getArg1Idx();
			int arg2SpanIdx = rel.getArg2Idx();
			Span arg1Span = spans.get(arg1SpanIdx);
			Span arg2Span = spans.get(arg2SpanIdx);
			int relId = rel.getRel().id;
			int arg1EntId = arg1Span.entity.id;
			//int arg2EntId = arg2Span.entity.id;
			long tNode = this.toNode_T(arg1Span.start, size, relId, arg1EntId);
			long prevINode = -1;
			for (int pos = arg1Span.start; pos <= arg1Span.end; pos++) {
				long curINode = pos > arg2Span.start ? this.toNode_I_0_c(pos, size, relId, arg1EntId) : this.toNode_I_0(pos, size, relId, arg1EntId);
				if(!builder.contains(curINode)){
					builder.addNode(curINode);
				}
				if (prevINode == -1) {
					if (!builder.contains(tNode)) {
						builder.addNode(tNode);
					}
					try {
						// NOTE: we can't handle multiple first arguments, double check before running.
						builder.addEdge(tNode, new long[]{curINode});
					} catch (NetworkException e) {
						// edge from T to I0 is added already ( means two mentions with same type start at same position)
						// could be same mentions, could be different mentions with different length, very rare.
					}
				} else {
					try {
						builder.addEdge(prevINode, new long[]{curINode});
					} catch (NetworkException e) {
						//The same mentions 
					}
				}
				prevINode = curINode;
			}
			if (arg1Span.end > arg2Span.start) {
				long lastIcNode = this.toNode_I_0_c(arg1Span.end, size, relId, arg1EntId);
				try {
					builder.addEdge(lastIcNode, new long[]{xNode});
				} catch (NetworkException e) {
					//The same mentions appear in the different (same) relation with different second arguments
				}
				//for (int pos = arg2Span.start; pos <=)
			} else {
				//for (int pos = arg1Span)
			}
				
//			if (arg1Span.start == arg2Span.start) {
//				
//			} else if (arg1Span.start < arg2Span.start) {
//				if (arg1Span.end <= arg2Span.start) {
//					for (int pos = arg1Span.start; pos <= arg1Span.end; pos++) {
//						long i0Node = this.toNode_I_0(pos, size, relId, arg1EntId);
//						builder.addNode(i0Node);
//						builder.addEdge(prevNode, new long[]{i0Node});
//						prevNode = i0Node;
//					}
//				} else {
//					
//				}
//			} else {
//				throw new NetworkException("Invalid structure, the arg1 should start earlier or same as arg2 before constructing network.");
//			}
		}
		
		return builder.build(networkId, inst, param, this);
	}

	@Override
	public Network compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private long toNode_X() {
		return this.toNode(0, 1, NodeType.X_NODE, 0, 0, 0);
	}
	
	private long toNode_I_0_c(int pos, int size, int relId, int arg1EntId) {
		return this.toNode(pos, size, NodeType.I_0_c_NODE, relId, arg1EntId, 0);
	}
	
	private long toNode_I_1(int pos, int size, int relId, int arg2EntId) {
		return this.toNode(pos, size, NodeType.I_1_NODE, relId, arg2EntId, 1);
	}
	
	private long toNode_Z(int pos, int size, int relId) {
		return this.toNode(pos, size, NodeType.Z_NODE, relId, 0, 0);
	}
	
	private long toNode_I_0(int pos, int size, int relId, int arg1EntId) {
		return this.toNode(pos, size, NodeType.I_0_NODE, relId, arg1EntId, 0);
	}
	
	private long toNode_T(int pos, int size, int relId, int arg1EntId) {
		return this.toNode(pos, size, NodeType.T_NODE, relId, arg1EntId, 0);
	}
	
	private long toNode_R(int pos, int size, int relId) {
		return this.toNode(pos, size, NodeType.R_NODE, relId, 0, 0);
	}
	
	private long toNode_E(int pos, int size) {
		return this.toNode(pos, size, NodeType.E_NODE, 0, 0, 0);
	}
	
	private long toNode_A(int pos, int size) {
		return this.toNode(pos, size, NodeType.A_NODE, 0, 0, 0);
	}

	private long toNode(int pos, int size, NodeType nodeType, int relationLabelId, int entLabelId, int arg){
		int[] arr = new int[]{size - pos - 1, nodeType.ordinal(), relationLabelId, entLabelId, arg};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	@Override
	public Instance decompile(Network network) {
		// TODO Auto-generated method stub
		return null;
	}


}
