package org.statnlp.example.nerelation.relation_hypergraph;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseInstance;
import org.statnlp.example.nerelation.RelStruct;

public class RelationHyperGraphInstance extends BaseInstance<RelationHyperGraphInstance, Sentence, RelStruct> {

	public RelationHyperGraphInstance(int instanceId, double weight) {
		super(instanceId, weight);
	}
	
	public RelationHyperGraphInstance(int instanceId, double weight, Sentence sent, RelStruct output) {
		super(instanceId, weight);
		this.input = sent;
		this.output = output;
	}

	private static final long serialVersionUID = 3188147552200201640L;

	@Override
	public int size() {
		return this.input.length();
	}

}
