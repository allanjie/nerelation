package org.statnlp.example.nerelation;

import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.nerelation.struct.DependencyTree;

public class RESentence extends Sentence {

	public String pennString;
	public DependencyTree depTree;
	
	private static final long serialVersionUID = -3938379517615089551L;

	public RESentence(WordToken[] tokens) {
		super(tokens);
	}
	
	public RESentence(WordToken[] tokens, String penn) {
		super(tokens);
		this.pennString = penn;
	}
	
	public void setPennString(String penn) {
		this.pennString = penn;
	}

	public String getPennString() {
		return this.pennString;
	}
	
}
