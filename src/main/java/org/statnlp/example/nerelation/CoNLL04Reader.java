package org.statnlp.example.nerelation;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;

public class CoNLL04Reader {

	public static RelInstance[] read(String file) throws IOException {
		return read(file, -1);
	}
	
	/**
	 * Read the conll04.corp
	 * Note that each position might contains multiple words
	 * separated by "/"
	 * @param file
	 * @param number
	 * @return
	 * @throws IOException
	 */
	public static RelInstance[] read(String file, int number) throws IOException {
		ArrayList<RelInstance> insts = new ArrayList<RelInstance>();
		String line = null;
		BufferedReader br = RAWF.reader(file);
		ArrayList<WordToken> wts = new ArrayList<WordToken>();
		List<Span> spans = new ArrayList<>();
		int instId = 1;
		int wIdx = 0;
		while ((line = br.readLine())!= null ) {
			wIdx = 0;
			wts = new ArrayList<>();
			while (!line.equals("")) {
				String[] vals = line.split("\\t");
				String ent = vals[1];
				Entity entity = Entity.get(ent);
				String pos_tags = vals[4];
				String words = vals[5]; //word and tags might be many.
				wts.add(new WordToken(words, pos_tags));
				Span span = new Span (wIdx, wIdx + 1, entity);
				spans.add(span);
				line = br.readLine();
			}
			//the line is empty line, then should be relations.
			line = br.readLine();//this should be relation
			if (line.equals("")) {
				wts = new ArrayList<WordToken>();
				spans = new ArrayList<>();
				continue;
			} else {
				List<Relation> relations = new ArrayList<>();
				Sentence input = new Sentence(wts.toArray(new WordToken[wts.size()]));
				while (!line.equals("")) {
					String[] vals = line.split("\\t");
					int arg1Idx = Integer.valueOf(vals[0]);
					int arg2Idx = Integer.valueOf(vals[1]);
					String rel = vals[2];
					RelationType relType = RelationType.get(rel);
					Relation relation = new Relation(arg1Idx, arg2Idx, relType);
					relations.add(relation);
					line = br.readLine();
				}
				//after that should be another sentence.
				RelInstance inst = new RelInstance(instId, 1.0, input, new RelStruct(spans, relations));
				instId++;
				insts.add(inst);
				wts = new ArrayList<>();
				spans = new ArrayList<>();
			}
			
		}
		br.close();
		System.err.println("#inst: " + insts.size()); //currently it's still not 1437
		return insts.toArray(new RelInstance[insts.size()]);
	}
	
	
	
	
	
	public static void main(String[] args) throws IOException{
		read("data/conll04/conll04.corp", -1);
	}
	
}
