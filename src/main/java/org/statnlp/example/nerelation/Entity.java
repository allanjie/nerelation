package org.statnlp.example.nerelation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Entity implements Comparable<Entity>, Serializable{
	
	private static final long serialVersionUID = -3314363044582374266L;
	public static final Map<String, Entity> ENTITIES = new HashMap<String, Entity>();
	public static final Map<Integer, Entity> ENTITIES_INDEX = new HashMap<Integer, Entity>();
	
	private static boolean locked = false;
	
	public static Entity get(String form){
		if(!ENTITIES.containsKey(form)){
			if (!locked) {
				Entity label = new Entity(form, ENTITIES.size());
				ENTITIES.put(form, label);
				ENTITIES_INDEX.put(label.id, label);
			} else {
				throw new RuntimeException("the map is locked");
			}
		}
		return ENTITIES.get(form);
	}
	
	public static Entity get(int id){
		return ENTITIES_INDEX.get(id);
	}
	
	public String form;
	public int id;
	
	private Entity(String form, int id) {
		this.form = form;
		this.id = id;
	}
	
	public static void lock () {
		locked = true;
	}

	@Override
	public int hashCode() {
		return form.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Entity))
			return false;
		Entity other = (Entity) obj;
		if (form == null) {
			if (other.form != null)
				return false;
		} else if (!form.equals(other.form))
			return false;
		return true;
	}
	
	public String toString(){
		return String.format("%s(%d)", form, id);
	}

	@Override
	public int compareTo(Entity o) {
		return Integer.compare(id, o.id);
	}
	
	public static int compare(Entity o1, Entity o2){
		if(o1 == null){
			if(o2 == null) return 0;
			else return -1;
		} else {
			if(o2 == null) return 1;
			else return o1.compareTo(o2);
		}
	}
}
