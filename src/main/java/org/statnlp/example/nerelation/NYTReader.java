package org.statnlp.example.nerelation;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;

public class NYTReader {

	
	public static RelInstance[] read(String file, boolean isLabel, int number) throws IOException, ParseException {
		ArrayList<RelInstance> insts = new ArrayList<RelInstance>();
		String line = null;
		BufferedReader br = RAWF.reader(file);
		List<Span> spans = new ArrayList<>();
		List<Relation> relations = new ArrayList<>();
		int instId = 1;
		JSONParser parser = new JSONParser();
		int numRelations = 0;
		int numMentions = 0;
		while ((line = br.readLine())!= null ) {
			 Object obj = parser.parse(line);
			 JSONObject jsonObj = (JSONObject)obj;
			 String sentStr = (String)jsonObj.get("sentText");
			 sentStr = sentStr.trim().replace("\"", "");
//			 System.out.println(sentStr);
			 String[] words = sentStr.split(" ");
			 WordToken[] wts = new WordToken[words.length];
			 for (int i = 0; i < words.length; i++) {
				 wts[i] = new WordToken(words[i]);
			 }
			 Sentence sent = new Sentence(wts);
			 JSONArray entityJsonArr = (JSONArray)jsonObj.get("entityMentions");
//			 System.out.println(entityJsonArr);
			 Map<String, String> twords2type = new HashMap<>();
			 numMentions += entityJsonArr.size();
			 int tIdx = 0;
			 JSONObject eobj = (JSONObject)entityJsonArr.get(tIdx);
			 String text = (String)eobj.get("text");
			 String[] twords = text.split(" ");
			 String elabel = (String)eobj.get("label");
			 twords2type.put(text, elabel);
			 int left = -1;
			 int right = -1;
			 //System.out.println(Arrays.toString(words));
			 for (int w = 0; w < words.length; w++) {
				 if (words[w].equals(twords[0])) {
					 boolean allSame = true;
					 for (int s = 1; s < twords.length; s++) {
						 if (!words[w + s].equals(twords[s])) {
							 allSame = false;
							 break;
						 }
					 }
					 if (allSame) {
						 left = w;
						 right = w + twords.length - 1;
						 Span span = new Span(left, right, Entity.get(elabel));
						 if (spans.contains(span)) throw new RuntimeException("contained already ?");
						 for (Span inSpan : spans) {
							 if (inSpan.overlap(span))
								 throw new RuntimeException("overlapp?");
						 }
						 spans.add(span);
//						 System.out.println(span.toString());
						 if (tIdx !=  entityJsonArr.size() - 1) {
							 tIdx++;
							 eobj = (JSONObject)entityJsonArr.get(tIdx);
							 text = (String)eobj.get("text");
							 twords = text.split(" ");
							 elabel = (String)eobj.get("label");
							 twords2type.put(text, elabel);
							 if (twords2type.containsKey(text) && !twords2type.get(text).equals(elabel)){
								 throw new RuntimeException("Same text different type");
							 }
							 w = right;
						 }
						 allSame = false;
					 }
				 }
			 }
			 if (tIdx != entityJsonArr.size() - 1) {
				 System.out.println(tIdx);
				 throw new RuntimeException("not finish finding the entities yet");
			 }
			 JSONArray relationJsonArr = (JSONArray)jsonObj.get("relationMentions");
			 numRelations += relationJsonArr.size();
			 for (int i = 0; i < relationJsonArr.size(); i++) {
				 JSONObject robj = (JSONObject)relationJsonArr.get(i);
				 String rlabel = (String)robj.get("label");
				 if (rlabel.equals("None") || rlabel.equals("none")) {
//					 numRelations++;
					 continue;
				 }
				 RelationType.get(rlabel);
				 String arg1text = (String)robj.get("em1Text");
				 String arg2text = (String)robj.get("em2Text");
				 if (arg1text.equals(arg2text)) throw new RuntimeException("same text have relations?");
				 int arg1SpanIdx = -1;
				 int arg2SpanIdx = -1;
				 for (int spanIdx = 0; spanIdx < spans.size(); spanIdx++) {
					 Span span = spans.get(spanIdx);
					 String currText = "";
					 for (int idx = span.start; idx <= span.end; idx++) {
						 currText += idx == span.start ? sent.get(idx).getForm() : " " + sent.get(idx).getForm();
					 }
					 if (currText.equals(arg1text)){
						 arg1SpanIdx = spanIdx;
						 if (arg1SpanIdx != -1 && arg2SpanIdx != -1) {
							 Relation relation = new Relation(arg1SpanIdx, arg2SpanIdx, RelationType.get(rlabel));
							 if (!relations.contains(relation))
								 relations.add(relation);
						 }
					 } else if (currText.equals(arg2text)) {
						 arg2SpanIdx = spanIdx;
						 if (arg1SpanIdx != -1 && arg2SpanIdx != -1) {
							 Relation relation = new Relation(arg1SpanIdx, arg2SpanIdx, RelationType.get(rlabel));
							 if (!relations.contains(relation))
								 relations.add(relation);
						 }
					 }
				 }
//				 Relation relation = new Relation(arg1SpanIdx, arg2SpanIdx, RelationType.get(rlabel));
//				 relations.add(relation);
			 }
//			 numRelations += relations.size();
			 RelInstance inst = new RelInstance(instId, 1.0, sent, new RelStruct(spans, relations));
			 insts.add(inst);
			 spans = new ArrayList<>();
			 relations = new ArrayList<>();
			 instId++;
		}
		System.out.println("number of mentions: " + numMentions);
		System.out.println("number of relations: " + numRelations);
		return insts.toArray(new RelInstance[insts.size()]);
	}
	
	public static void main(String[] args) throws IOException, ParseException{
		read("data/NYT/train.json", false, -1);
		read("data/NYT/test.json", false, -1);
		System.out.println("#distinct relations:" + RelationType.RELS.size());
		System.out.println("Distinct relations:" + RelationType.RELS.toString());
	}
}
