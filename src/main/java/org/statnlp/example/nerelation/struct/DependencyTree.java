package org.statnlp.example.nerelation.struct;

import org.statnlp.commons.types.Sentence;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.stack.TIntStack;

public class DependencyTree {

	private TIntObjectMap<TIntList> tree;
	
	/**
	 * The sentence must be with head index and it's 0-indexed. 
	 * In this case, the root should be indexed by -1.
	 * @param sent
	 */
	public DependencyTree (Sentence sent) {
		this.build(sent);
	}
	
	public void build(Sentence sent) {
		tree = new TIntObjectHashMap<>();
		for (int i = 0; i < sent.length(); i++) {
			this.add(sent.get(i).getHeadIndex(), i);
		}
	}
	
	private void add (int parent, int child) {
		if (tree.containsKey(parent)) {
			TIntList list = tree.get(parent);
			list.add(child);
		} else {
			TIntList list = new TIntArrayList();
			list.add(child);
			tree.put(parent, list);
		}
	}
	
	
	public boolean dfsFind (int val, TIntStack stack) {
		return dfsFind(-1, val, stack);
	}
	
	private boolean dfsFind(int curr, int val, TIntStack stack) {
		stack.push(curr);
		if (curr == val) {
			return true;
		} else {
			TIntList children = tree.get(curr);
			if (children != null) {
				for (int i = 0; i < children.size(); i++) {
					boolean found = dfsFind(children.get(i), val, stack);
					if (found) {
						return true;
					} 
				}
			}
			stack.pop();
			return false;
		}
	}
	
	/****  ///debuggging code.
	//Testing the dependency tree.
	public static void main(String[] args) {
		List<WordToken> wts = new ArrayList<WordToken>();
		wts.add(new WordToken("Mike", "NNP",1, "", "compound")); wts.add(new WordToken("O", "NNP",3, "", "nmod:poss"));
		wts.add(new WordToken("'", "NNP",1, "", "case")); wts.add(new WordToken("Sullivan", "NNP",-1, "", "root"));
		wts.add(new WordToken(",", "NNP",3, "", "punct")); wts.add(new WordToken("VOA", "NNP",6, "", "compound"));
		wts.add(new WordToken("News", "NNP",3, "", "conj")); wts.add(new WordToken(",", "NNP",3, "", "punct"));
		wts.add(new WordToken("Los", "NNP",9, "", "compound")); wts.add(new WordToken("Angeles", "NNP",3, "", "appos"));
		wts.add(new WordToken(".", "NNP",3, "", "punct"));
		LGSentence sent = new LGSentence(wts.toArray(new WordToken[wts.size()]));
		DependencyTree tree = new DependencyTree(sent);
		sent.depTree = tree;
		TIntStack stack = new TIntArrayStack();
		for (int val = 0; val < sent.length(); val++) {
			stack = new TIntArrayStack();
			boolean found = tree.dfsFind(val, stack);
			System.out.println("val "+val+" is found? " + found+" trace:"+stack);
		}
		for (int i = 0; i< sent.length(); i++)
			for (int j = 0; j < sent.length(); j++) {
				System.out.println(i + "," + j + ":" + tree.findPath(sent, i, j));
			}
		
		
	}
	
	private String findPath(LGSentence sent, int hm1Idx, int hm2Idx) {
		StringBuilder path = new StringBuilder();
		TIntStack stack1 = new TIntArrayStack();
		TIntStack stack2 = new TIntArrayStack();
		boolean found1 =  sent.depTree.dfsFind(hm1Idx, stack1);
		boolean found2 = sent.depTree.dfsFind(hm2Idx, stack2);
		if (!(found1 && found2)) throw new RuntimeException("Not found in the dependency tree?");
		int[] trace1 = stack1.toArray();
		int[] trace2 = stack2.toArray();
		int contIdx1 = containsElement(trace1, hm2Idx);
		int contIdx2 = containsElement(trace2, hm1Idx);
		if (contIdx1 >= 0 || contIdx2 >= 0) {
			if (contIdx1 >= 0 && contIdx2 >= 0 && hm1Idx != hm2Idx) throw new RuntimeException("impossible");
			if (contIdx1 >= 0) {
				for (int k = 0; k < contIdx1 ; k++) { // because the trace is backtracked.
					String depLabel = sent.get(trace1[k]).getDepLabel();
					if (k==0) path.append(depLabel);
					else path.append(" " + depLabel);
				}
			} else {
				for (int k = contIdx2 - 1; k >=0 ; k--) { // because the trace is backtracked.
					String depLabel = sent.get(trace2[k]).getDepLabel();
					if (k==contIdx2 - 1) path.append(depLabel);
					else path.append(" " + depLabel);
				}
			}
		} else {
			int stamp1 = trace1.length - 1;
			int stamp2 = trace2.length - 1;
			while (stamp1 >= 0 && stamp2 >= 0) {
				if (trace1[stamp1] != trace2[stamp2]) {
					stamp1++;
					stamp2++;
					break;
				} else {
					stamp1--;
					stamp2--;
				}
			}
			//trace1[stamp1] should be equal to trace2[stamp2]
			for (int k = 0; k < stamp1; k++) {
				String depLabel = sent.get(trace1[k]).getDepLabel();
				if (k==0) path.append(depLabel);
				else path.append(" " + depLabel);
			}
			for (int k = stamp2 - 1; k >=0; k--) {
				String depLabel = sent.get(trace2[k]).getDepLabel();
				path.append(" " + depLabel);
			}
		}
		//Note: the path is label path
		return path.toString();
	}
	
	private int containsElement(int[] arr, int val) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == val)
				return i;
		}
		return -1;
	}
	***/
}
