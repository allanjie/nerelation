package org.statnlp.example.nerelation.tree;

import org.statnlp.example.base.BaseInstance;
import org.statnlp.example.nerelation.RelationType;

/**
 * TreeInstance: input is the sentence with input sentence and also span output
 * is the relation tree over the entity span. The output tree is a binary tree
 * which can be represented by an array.
 * Span can be with the entities only. 
 * 
 * @author allan
 *
 */
public class TreeInstance extends BaseInstance<TreeInstance, SentSpan, RelationType[][]> {

	private static final long serialVersionUID = -1793735840363475154L;

	public TreeInstance(int instanceId, double weight) {
		super(instanceId, weight);
	}

	public TreeInstance(int instanceId, double weight, SentSpan input, RelationType[][] output) {
		this(instanceId, weight);
		this.input = input;
		this.output = output;
	}

	/**
	 * Return the length of input sentence
	 * note that we need the number of span.
	 */
	@Override
	public int size() {
		return this.input.sent.length();
	}
	
	public SentSpan duplicateInput(){
		return input;
	}
	
	public RelationType[][] duplicateOutput() {
		return this.output.clone();
	}

}
