package org.statnlp.example.nerelation.tree;

import java.util.List;

import org.statnlp.example.nerelation.RESentence;
import org.statnlp.example.nerelation.Span;

/**
 * SentSpan: the input data structure with sentence and entity spans as input.
 * 
 * @author Allan (allanmcgrady@gmail.com)
 *
 */
public class SentSpan {

	protected RESentence sent;
	
	/**
	 * The span should contains the entities only.
	 */
	protected List<Span> spans;

	public SentSpan(RESentence sent, List<Span> spans) {
		this.sent = sent;
		this.spans = spans;
	}

	@Override
	public String toString() {
		return "SentSpan [sent=" + sent + "]\n[spans=" + spans + "]";
	}

}
