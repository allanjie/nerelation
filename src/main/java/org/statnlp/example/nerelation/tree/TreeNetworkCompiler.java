package org.statnlp.example.nerelation.tree;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.nerelation.ACE2004Config;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkIDMapper;

public class TreeNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -8962135867023273966L;
	
	protected enum NodeType {node, root};
	protected boolean DEBUG = true;
	protected Set<String> rules;
	protected boolean useRelRules;
	protected int maxRelDist = 11;
	
	/**
	 * Passing the maximum size to the network compiler.
	 * For each instance, we create the instance specific unlabeled network.
	 */
	public TreeNetworkCompiler(Set<String> rules, boolean useRelRules, int maxRelDist) {
		NetworkIDMapper.setCapacity(new int[]{150, 150, RelationType.RELS.size() + 1, 2});
		this.rules = rules;
		this.useRelRules = useRelRules;
		this.maxRelDist = maxRelDist;
	}

	/**
	 * This root node does not contains the relation type information.
	 * The root for the forest. since we are using max-marginal decoding, the root might not even needed.
	 * @param spanLen
	 * @return
	 */
	private long toRoot(int spanLen) {
		int[] arr = new int[]{spanLen , spanLen , RelationType.RELS.size(), NodeType.root.ordinal()};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private long toNode(int left, int right, int relId){
		int[] arr = new int[]{right, right - left, relId, NodeType.node.ordinal()};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	
	@Override
	public Network compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> networkBuilder = NetworkBuilder.builder();
		TreeInstance treeInst = (TreeInstance)inst;
		RelationType[][] relations = treeInst.getOutput();
		List<Span> spans = treeInst.getInput().spans;
		long root = this.toRoot(spans.size());
		networkBuilder.addNode(root);
		// because the sentence length is 1 more than the leaves in the tree.
		for (int right = 0; right < spans.size(); right++) {
			//adding all the leaves with len = 0;
			if (right !=  spans.size() - 1) {
				//System.out.println(right + "," + (right + 1) + "," + relations[right][right+1].form);
				long leaf = this.toNode(right, right + 1, relations[right][right + 1].id);
				networkBuilder.addNode(leaf);
				if (spans.size() == 2) { 
					networkBuilder.addEdge(root, new long[]{leaf});
				}
			}
			Span rightSpan = spans.get(right);
			//len means "right - left".
			for (int len = 2; len <= right; len++) {
				int left = right - len;
				Span leftSpan = spans.get(left);
				int paRelId = relations[left][right].id;
				long parent = this.toNode(left, right, paRelId);
				RelationType rparent = RelationType.get(paRelId);
				for (int m = left + 1; m < right; m++) {
					for (int c1RelId = 0; c1RelId < RelationType.RELS.size(); c1RelId++) {
						long leftChild = this.toNode(left, m, c1RelId);
						RelationType rleft = RelationType.get(c1RelId);
						for (int c2RelId = 0; c2RelId < RelationType.RELS.size(); c2RelId++) {
							long rightChild = this.toNode(m, right, c2RelId);
							RelationType rright = RelationType.get(c2RelId);
							//rules contain two rule.
							String entRule = rparent.form + "," + leftSpan.entity.form + "," + rightSpan.entity.form;
							String relRule = rparent.form + "," + rleft.form + "," + rright.form;
							if (networkBuilder.contains(leftChild) && networkBuilder.contains(rightChild) && 
									rules.contains(entRule)) {
								if (!useRelRules || (useRelRules && rules.contains(relRule))) {
									networkBuilder.addNode(parent);
									networkBuilder.addEdge(parent, new long[]{leftChild, rightChild});
								}
								
							}
						}
					}
				}
				if (left == 0 && right == spans.size() -1) {
					//System.err.println(treeInst.getInput().sent.toString());
					//System.err.printf("left: %d, right: %d, relId: %d, %s\n", left, right, paRelId, RelationType.get(paRelId).form);
					networkBuilder.addEdge(root, new long[] {parent});
				}
			}
		}
		BaseNetwork network = networkBuilder.build(networkId, inst, param, this);
		//System.out.println("number of nodes in labeled:" + network.getAllNodes().length);
		if (DEBUG) {
			BaseNetwork debugUnlabeled = (BaseNetwork)this.compileUnlabeled(networkId, treeInst, param);
			if (!debugUnlabeled.contains(network)) {
				throw new RuntimeException("not contains");
			}
		}
		return network;
	}

	@Override
	public Network compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> networkBuilder = NetworkBuilder.builder();
		TreeInstance treeInst = (TreeInstance)inst;
		List<Span> spans = treeInst.getInput().spans;
		int spanLen = ((TreeInstance)inst).getInput().spans.size();
		//span len - 1 because of the index is 1 bit delayed
		long root = this.toRoot(spanLen);
		networkBuilder.addNode(root);
		for (int right = 0; right < spanLen; right++) {
			//adding all the leaves with len = 0;
			if (right !=  spans.size() - 1) {
				for (int relId = 0; relId < RelationType.RELS.size(); relId++) {
					long leaf = this.toNode(right, right + 1, relId);
					networkBuilder.addNode(leaf);
					if (spanLen == 2) { //means only two entities. the root should connect these two.
						networkBuilder.addEdge(root, new long[]{leaf});
					}
				}
			}
			Span rightSpan = spans.get(right);
			for (int len = 2; len <= right; len++) {
				int left = right - len;
				Span leftSpan = spans.get(left);
				for (int relId = 0; relId < RelationType.RELS.size(); relId++) {
					//define some rules using the entity to restrict the parents
					if (len > this.maxRelDist && relId != RelationType.get(ACE2004Config.NR).id) continue;
					RelationType rparent = RelationType.get(relId);
					String entRule = rparent.form + "," + leftSpan.entity.form + "," + rightSpan.entity.form;
					if (rules.contains(entRule)) {
						long parent = this.toNode(left, right, relId);
						for (int m = left + 1; m < right; m++) {
							for (int c1RelId = 0; c1RelId < RelationType.RELS.size(); c1RelId++) {
								long leftChild = this.toNode(left, m, c1RelId);
								RelationType rleft = RelationType.get(c1RelId);
								for (int c2RelId = 0; c2RelId < RelationType.RELS.size(); c2RelId++) {
									RelationType rright = RelationType.get(c2RelId);
									long rightChild = this.toNode(m, right, c2RelId);
									String relRule = rparent.form + "," + rleft.form + "," + rright.form;;
									if (networkBuilder.contains(leftChild) && networkBuilder.contains(rightChild)) {
										if (!useRelRules || (useRelRules && rules.contains(relRule))) {
											networkBuilder.addNode(parent);
											networkBuilder.addEdge(parent, new long[]{leftChild, rightChild});
										}
									}
								}
							}
						}
						if (len == spanLen - 1 && networkBuilder.contains(parent)) {
							networkBuilder.addEdge(root, new long[]{parent});
						}
					}
				}
			}
		}
		return networkBuilder.build(networkId, inst, param, this);
	}

	
	@Override
	public Instance decompile(Network network) {
		BaseNetwork net = (BaseNetwork)network;
		TreeInstance inst = (TreeInstance)net.getInstance();
		if (NetworkConfig.MAX_MARGINAL_DECODING) {
			SentSpan input = inst.getInput();
			List<Span> spans = input.spans;
			int spanLen = inst.getInput().spans.size();
			RelationType[][] rels = new RelationType[spanLen][spanLen];
			for (int left = 0; left < spans.size(); left++) {
				for (int right = left + 1; right < spans.size(); right++) {
					double maxMarginal = Double.NEGATIVE_INFINITY;
					int bestLabel = -1;
					for (int l = 0; l < RelationType.RELS.size(); l++) {
						long node = this.toNode(left, right, l);
						int nodeIdx = Arrays.binarySearch(net.getAllNodes(), node);
						if (nodeIdx >= 0 && net.getMarginal(nodeIdx) > maxMarginal) {
							maxMarginal = net.getMarginal(nodeIdx);
							bestLabel = l;
						}
					}
					if (bestLabel == -1) {
						System.out.println(inst.getInput().sent.toString());
						throw new RuntimeException("best label is not found on "+left+","+right);
					}
					rels[left][right] = RelationType.get(bestLabel);
				}
			}
			inst.setPrediction(rels);
		} else {
			//Viterbi Decoding
			long root = this.toRoot(inst.getInput().spans.size());
			int rootIdx = Arrays.binarySearch(net.getAllNodes(), root);
			int spanLen = inst.getInput().spans.size();
			RelationType[][] rels = new RelationType[spanLen][spanLen];
			this.findBest(net, rels, rootIdx);
			inst.setPrediction(rels);
		}
		return inst;
	}
	
	private void findBest(BaseNetwork network, RelationType[][] rels, int parent_k) {
		int[] children_k = network.getMaxPath(parent_k);
		for (int child_k : children_k) {
			long node = network.getNode(child_k);
			int[] nodeArr = NetworkIDMapper.toHybridNodeArray(node);
			int right = nodeArr[0];
			int left = nodeArr[0] - nodeArr[1];
			int relId = nodeArr[2];
			rels[left][right] = RelationType.get(relId);
			this.findBest(network, rels, child_k);
		}
	}
	
	public double costAt(Network network, int parent_k, int[] child_k){
		int size = network.getInstance().size();
		Network labeledNet = network.getLabeledNetwork();
		long node = network.getNode(parent_k);
		int node_k = labeledNet.getNodeIndex(node);
		if(node_k < 0){
			if(NetworkConfig.NORMALIZE_COST){
				return 1.0 / size;
			} else 
				return 1.0;
		} else {
			return 0.0;
		}
	}


}
