package org.statnlp.example.nerelation.tree;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.nerelation.ACE2004Config;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.MentionType;
import org.statnlp.example.nerelation.RESentence;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;
import org.statnlp.example.nerelation.struct.DependencyTree;

import cern.colt.Arrays;

public class TreeReader {


	/**
	 * This reader only read the mention span, not all the spans include O
	 * @param file
	 * @param number
	 * @return
	 * @throws IOException
	 */
	private static List<TreeInstance> readList(String file, RESentence[] sents, boolean isLabeled, int number, Set<String> rules) throws IOException {
		ArrayList<TreeInstance> insts = new ArrayList<TreeInstance>();
		String line = null;
		BufferedReader br = RAWF.reader(file);
		int instId = 1;
		int preSentId = 0;
		int numRel = 0;
		int numEntityPair = 0;
		int maxRelationDist = -1;
		int maxNumEntInSent = -1;
		int notConnectedNum = 0;
		int numMentions = 0;
		while ((line = br.readLine())!= null ) {
			String[] words = line.split(" ");
			line = br.readLine(); // read the pos tag.
			String[] tags = line.split(" ");
			WordToken[] wts = new WordToken[words.length];
			for (int t = 0; t < wts.length; t++) {
				wts[t] = new WordToken(words[t], tags[t]);
			}
			String pennStr = br.readLine();
			if (ACE2004Config.pennTreeRootEmpty)
				pennStr = pennStr.replaceFirst("ROOT", "");
			RESentence sent = new RESentence(wts, pennStr);
			line = br.readLine();
			String[] headIdxs = line.split(" ");
			line = br.readLine();
			String[] depLabels = line.split(" ");
			for (int t = 0; t < wts.length; t++) {
				wts[t].setHead(Integer.valueOf(headIdxs[t])); //might be -1, means root
				wts[t].setDepLabel(depLabels[t]);
			}
			sent.depTree = new DependencyTree(sent);
			if (sents != null) {
				RESentence preSent = sents[preSentId];
				if (words.length != preSent.length()) {
					System.err.println(Arrays.toString(words));
					System.err.println(words.length + "," + preSent.length() + " "+ preSent.toString());
					throw new RuntimeException("lengths are not equal");
				}
				for (int t = 0; t < wts.length; t++) {
					wts[t].setChunk(preSent.get(t).getChunk());
					wts[t].setChunkHead(preSent.get(t).getChunkHead());
					wts[t].setIobChain(preSent.get(t).getIobChain());
				}
			}
			String ners = br.readLine();
			List<Span> spans = new ArrayList<>();
			if (!ners.equals("")) {
				String[] spanInfos = ners.split("\\|");
				for (String spanInfo : spanInfos) {
					String[] spanArr = spanInfo.split(" ");
					/**contains the "entity type, mention type, and entity subtype".
					 * Note ACE2004 PER entity does not have the subtype information.
					 * ***/
					String[] types = spanArr[1].split(",");
					String[] indices = spanArr[0].split(",");
					String entSubType = types.length == 2 ? types[0] : types[2];
					/**** Note: the end of span is the end point of the head end. ***/
					int end = ACE2004Config.HEAD_END_AS_MENT_END ? 
							Integer.valueOf(indices[3]) - 1 : Integer.valueOf(indices[1]) - 1;
					Span span = new Span(Integer.valueOf(indices[0]), end, 
							Entity.get(types[0]), MentionType.get(types[1]), entSubType);
					setHeadIdx(sent, span);
					if (!spans.contains(span)) {
						spans.add(span);
					}
				}
				Collections.sort(spans);
			}
			preSentId++;
			String allRelations = br.readLine();
			SentSpan input = new SentSpan(sent, spans);
			RelationType[][] output = new RelationType[spans.size()][spans.size()];
			maxNumEntInSent = Math.max(maxNumEntInSent, spans.size());
			if (allRelations.equals("")) {
				
			} else {
				String[] vals = allRelations.split("\\|");
				for (String oneRelation : vals) {
					numRel++;
					String[] indices = oneRelation.split(" ");
					String relType = indices[0];
					String[] arg1MentionTypes = indices[2].split(",");
					String[] arg2MentionTypes = indices[4].split(",");
					String[] firstIndices = indices[1].split(",");
					String[] secondIndices = indices[3].split(",");
					int end1 = ACE2004Config.HEAD_END_AS_MENT_END ?
							Integer.valueOf(firstIndices[3]) - 1 : Integer.valueOf(firstIndices[1]) - 1;
					int end2 = ACE2004Config.HEAD_END_AS_MENT_END ?
							Integer.valueOf(secondIndices[3]) - 1 : Integer.valueOf(secondIndices[1]) - 1;
					Span span1 = new Span(Integer.valueOf(firstIndices[0]), end1, 
							Entity.get(arg1MentionTypes[0]), MentionType.get(arg1MentionTypes[1]));
					Span span2 = new Span(Integer.valueOf(secondIndices[0]), end2, 
							Entity.get(arg2MentionTypes[0]), MentionType.get(arg2MentionTypes[1]));
					int span1Idx = spans.indexOf(span1);
					int span2Idx = spans.indexOf(span2);
					int leftIdx = span1Idx < span2Idx ? span1Idx : span2Idx;
					int rightIdx = span1Idx < span2Idx ? span2Idx : span1Idx;
					String coarsedType = null;
					if (ACE2004Config.COARSED) {
						coarsedType = relType.split("::")[0];
						relType = coarsedType;
						if (relType.equals(ACE2004Config.DISC) && !ACE2004Config.useDISC) {
							relType = ACE2004Config.NR;
						}
					} else {
						if (relType.equals(ACE2004Config.FINED_DISC) && !ACE2004Config.useDISC) {
							relType = ACE2004Config.NR;
						}
					}
					//define the direction.
					/**means the reversed direction for the relation**/
					String direction = span1Idx < span2Idx ? "" : ACE2004Config.REV_SUFF;
					if (ACE2004Config.PERSOC_SYM && relType.startsWith(ACE2004Config.PER_SOC)) {
						direction = "";
					}
					relType = relType.equals(ACE2004Config.NR) ? relType : relType + direction;
					RelationType relationType = RelationType.get(relType);
					output[leftIdx][rightIdx] = relationType;
					if (rules != null  && ACE2004Config.READ_RULE_FROM_DATA) {
						rules.add(relationType.form+","+spans.get(leftIdx).entity.form+","+spans.get(rightIdx).entity.form);
					}
					if (span1Idx < 0 || span2Idx < 0)
						throw new RuntimeException("smaller than 0?");
				}
			}
			for (int i = 0; i < spans.size(); i++) {
				for (int j = i + 1; j < spans.size(); j++) {
					numEntityPair++;
					if (output[i][j] == null) {
						output[i][j] = RelationType.get(ACE2004Config.NR);
						if (rules != null) {
							rules.add(ACE2004Config.NR + "," + spans.get(i).entity.form+ "," + spans.get(j).entity.form);
						}
					}
					if (!output[i][j].equals(RelationType.get(ACE2004Config.NR))) {
						maxRelationDist = Math.max(maxRelationDist, j - i);
					}
				}
			}
			TreeInstance inst = new TreeInstance(instId, 1.0, input, output);
			if (isLabeled)
				inst.setLabeled();
			else inst.setUnlabeled();
			//read the rule only from labeled data
			if (isLabeled && rules != null  && ACE2004Config.READ_RULE_FROM_DATA) {
				//add the relation production rules.
				for (int i = 0; i < spans.size(); i++) {
					for (int j = i + 2; j < spans.size(); j++) {
						String parent = output[i][j].form;
						for (int m = i + 1; m < j; m++) {
							String leftC = output[i][m].form;
							String rightC = output[m][j].form;
							rules.add(parent + "," + leftC + "," + rightC);
						}
					}
				}
			}
			if (input.spans.size() > 1) {
				notConnectedNum += TreeUtils.checkConnected(inst);
				numMentions += inst.getInput().spans.size();
				insts.add(inst);
				instId++;
			}
			if (number != -1 && insts.size() == number) {
				break;
			}
			spans = new ArrayList<>();
			line = br.readLine(); //empty line
		}
		br.close();
		RelationType.get(ACE2004Config.NR);
		System.out.println("number of relations:" + numRel);
		System.out.println("number of mention pairs:" + numEntityPair);
		System.out.println("The max relation distance:" + maxRelationDist);
		System.out.println("The max number of mentions in a sent:" + maxNumEntInSent);
		System.out.println("Total number of mentions:" + numMentions);
		System.out.println("Total Number of not connected mentions: " + notConnectedNum);
		System.out.printf("Total percentage of valid mentions under dep: %.2f%%\n", (numMentions - notConnectedNum) * 1.0 /
				numMentions * 100);
		return insts;
	}
	
	/**
	 * Read the fold file in a folder, the folder should only contains file
	 * This method read all folds and make the testFold^{th} as the test Fold. 
	 * without the fold sentences chunk
	 * @param folder
	 * @param testFold
	 * @return
	 * @throws IOException
	 */
	public static TreeInstance[][] readFolds(String folder, int testFold) throws IOException {
		return readFolds(folder, testFold, null);
	}
	
	public static TreeInstance[][] readFolds(String folder, int testFold, RESentence[][] foldSents) throws IOException {
		return readFolds(folder, testFold, foldSents, null, -1);
	}
	
	public static TreeInstance[][] readFolds(String folder, int testFold, RESentence[][] foldSents, Set<String> rules) throws IOException {
		return readFolds(folder, testFold, foldSents, rules, -1);
	}
	
	/**
	 * Read the fold file in a folder, the folder should only contains file
	 * This method read all folds and make the testFold^{th} as the test Fold.
	 * @param folder
	 * @param testFold: indicate which fold should be the testFold, starting from 1.
	 * @param foldSents: the preread sentence with chunk information
	 * @param numInstInFold: number of instances to read in each fold;
	 * @return
	 */
	public static TreeInstance[][] readFolds(String folder, int testFold, RESentence[][] foldSents, Set<String> rules, int numInstInFold) throws IOException {
		File aceDir = new File(folder);
		List<TreeInstance> trainInsts = new ArrayList<>();
		List<TreeInstance> testInsts = new ArrayList<>();
		TreeInstance[][] data = new TreeInstance[2][];
		for(File subdir: aceDir.listFiles()){
			if(subdir.isDirectory()){
				continue;
			}
			boolean labeled = true;
			String[] names = subdir.getName().split("\\.");
			int idx = Integer.valueOf(names[1]);
			if (idx == testFold)
				labeled = false;
			RESentence[] sents = foldSents!=null? foldSents[idx - 1] : null;
			System.out.println("Reading file "+subdir.getAbsolutePath());
			List<TreeInstance> insts = readList(subdir.getAbsolutePath(), sents, labeled, numInstInFold, rules);
			if (labeled) {
				trainInsts.addAll(insts);
			} else {
				testInsts = insts;
				data[1] = testInsts.toArray(new TreeInstance[testInsts.size()]);
			}
		}
		data[0] =  trainInsts.toArray(new TreeInstance[trainInsts.size()]);
		return data;
	}
	
	/**
	 * Set the head Index according to the paper Zhou et al., 2005. 
	 * @param sent
	 * @param span
	 * @return
	 */
	private static void setHeadIdx (Sentence sent, Span span) {
		//a mention is from the start to the head end according to Zhou 2005
		for (int i = span.start; i <= span.end; i++) {
			if (i > span.start && sent.get(i).getTag().equals("IN")) {
				span.headIdx = i -1;
				return;
			}
		}
		span.headIdx = span.end;
	}
	
	
}
