package org.statnlp.example.nerelation.tree;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.example.nerelation.ACE2004Config;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.MentionType;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkModel;
import org.statnlp.hypergraph.NetworkConfig.ModelType;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class TreeMain {

	public static double l2val = 0.01;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static int totalNumInst = 2;
	public static String ace2004 = "data/ACE2004Folds"; //including newswire and bnews data
	public static String ace2004Chunk = "data/ACE2004FoldsChunk"; //including newswire and bnews data
	public static int foldNum = 5; //total fold number, fixed to 5 for now
	public static int numInstsInFold = -1;
	public static boolean useDISC = true;
	public static Set<String> rules;
	public static boolean useHeadEnd = true;
	public static int topK = 1;
	public static boolean useRelRules = false;
	public static boolean maxMarginal = false;
	public static int maxRelDist = 11;
	//by default we are using the CRF model type
	public static ModelType modelType = ModelType.CRF;
	
	public static void main(String[] args) throws IOException, InterruptedException {
		setArgs(args);
		
		/***
		 * Parameter settings and model configuration
		 */
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.PARALLEL_FEATURE_EXTRACTION = true;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		NetworkConfig.MODEL_TYPE = modelType;
		NetworkConfig.MAX_MARGINAL_DECODING = topK > 1? false: maxMarginal? true : false;
		ACE2004Config.useDISC = useDISC;
		ACE2004Config.PERSOC_SYM = false;
		ACE2004Config.HEAD_END_AS_MENT_END = useHeadEnd;
		ACE2004Config.USE_RULE_CONSTRAINT = true;
		
		System.out.println("[Info] Model Type: " + modelType);
		/**
		 * 5-fold validation.
		 */
		//TODO: temporarily forget about the chunk first.
		//TreeInstance[][] lgsents = TreeReader.readFoldsChunk(ace2004Chunk, foldNum);
		double[] allMetrics = new double[3];
		for (int f = 1; f <= foldNum; f++) {
			rules = new HashSet<>();
			TreeInstance[][] data = TreeReader.readFolds(ace2004, f, null, rules, numInstsInFold);
			System.out.println("#Relations: " + RelationType.RELS.size());
			System.out.println("Relations: " + RelationType.RELS.toString());
			System.out.println("Entity: " + Entity.ENTITIES.toString());
			System.out.println("Rules: " + rules.toString());
			Entity.lock();
			RelationType.lock();
			MentionType.lock();
			GlobalNetworkParam gnp = new GlobalNetworkParam(OptimizerFactory.getLBFGSFactory());
			TreeFeatureManager tfm = new TreeFeatureManager(gnp);
			TreeNetworkCompiler tnc = new TreeNetworkCompiler(rules, useRelRules, maxRelDist);
			NetworkModel model = DiscriminativeNetworkModel.create(tfm, tnc);
			TreeInstance[] trainInsts  = data[0];
			/***debug info****/
//			trainInsts = new TreeInstance[1];
//			trainInsts[0] = data[0][1];
//			System.out.println(trainInsts[0].getInput().sent.toString());
			//trainInsts[1] = data[0][1];
			/*****/
			model.train(trainInsts, numIteration);
			/**
			 * Testing Phase
			 */
			TreeInstance[] testInsts = data[1];
			/**debug information**/
//			testInsts = trainInsts;
//			int instId = 1;
//			for(TreeInstance inst : testInsts) {
//				inst.setInstanceId(instId);
//				inst.setUnlabeled();
//				instId++;
//			}
			/****/
			Instance[] results = model.decode(testInsts, topK);
			double[] metrics = TreeEval.evaluate(results);
			for (int i = 0; i< allMetrics.length; i++) {
				allMetrics[i] += metrics[i];
			}
		}
		for (int i = 0; i < allMetrics.length; i++) {
			allMetrics[i] /= foldNum;
		}
		System.out.printf("[Result] Final: \t\tPrec.:%.2f%%\tRec.:%.2f%%\tF1.:%.2f%%\n", 
				allMetrics[0], allMetrics[1], allMetrics[2]);
	}
	
	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Tree-CRF Card-Pyramid Model for Relation Extraction");
		parser.addArgument("-t", "--thread").setDefault(8).help("number of threads");
		parser.addArgument("--numInstsInFold").setDefault(numInstsInFold).help("number of instances in each fold");
		parser.addArgument("--l2").setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--disc").action(Arguments.storeTrue()).setDefault(false).help("include disc or not");
		parser.addArgument("--headEnd").action(Arguments.storeTrue()).setDefault(false).help("use head as mention end.");
		parser.addArgument("--iter").setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("-k", "--topk").setDefault(topK).help("The topk prediction.");
		parser.addArgument("--relRules").action(Arguments.storeTrue()).setDefault(false).help("use the relation rules or not");
		parser.addArgument("--maxMarginal").action(Arguments.storeTrue()).setDefault(true).help("using max marginal or not.");
		parser.addArgument("--modelType").setDefault(modelType).type(ModelType.class).help("the model type");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = Integer.valueOf(ns.getString("thread"));
        numInstsInFold = Integer.valueOf(ns.getString("numInstsInFold"));
        l2val = Double.valueOf(ns.getString("l2"));
        useDISC = ns.getBoolean("disc");
        useHeadEnd = ns.getBoolean("headEnd");
        numIteration = Integer.valueOf(ns.getString("iter"));
        topK = Integer.valueOf(ns.getString("topk"));
        maxMarginal = ns.getBoolean("maxMarginal");
        modelType = (ModelType) ns.get("modelType");
        System.err.println(ns.getAttrs().toString());
	}

}
