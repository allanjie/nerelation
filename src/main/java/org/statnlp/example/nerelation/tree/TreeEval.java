package org.statnlp.example.nerelation.tree;

import java.util.List;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.nerelation.ACE2004Config;
import org.statnlp.example.nerelation.RelationType;

public class TreeEval {

	
	/**
	 * Evaluating the relation, precision
	 * for r1: precision: predict correctly
	 *      divided by the number of r1 predicted.
	 *     recall, divided by the number of r1 in the data
	 */
	public static double[] evaluate(Instance[] results) {
		//calculating the precision fist.
		double[] metrics = new double[3];
		int[] p = new int[RelationType.RELS.size()];
		int[] totalPredict = new int[RelationType.RELS.size()];
		int[] totalInData = new int[RelationType.RELS.size()];
		int unpred = 0;
		for (Instance inst : results) {
			TreeInstance res = (TreeInstance)inst;
			int spanLen = res.getInput().spans.size();
			RelationType[][] gold = res.getOutput();
			if (res.getTopKPredictions() != null) {
				//set back the prediction using topk
				List<RelationType[][]> predictions = res.getTopKPredictions();
				RelationType[][] resPred = new RelationType[spanLen][spanLen];
				int[][][] votes = new int[spanLen][spanLen][RelationType.RELS.size()];
				for (int i = 0; i < spanLen; i++) {
					for (int j = i + 1; j < spanLen; j++) {
						int maxVote = 0;
						int maxRelId = -1;
						//take the first one if no larger than.
						for(RelationType[][] pred: predictions) {
							if (pred[i][j] != null) {
								votes[i][j][pred[i][j].id]++;
								if (votes[i][j][pred[i][j].id] > maxVote) {
									maxVote = votes[i][j][pred[i][j].id];
									maxRelId = pred[i][j].id;
								}
							}
						}
//						if (maxRelId == -1) {
//							System.out.println(inst.getInput().toString());
//							System.out.println(i + "," + j);
//						}
						if (maxRelId == -1){
							unpred++;
							resPred[i][j] = RelationType.get(ACE2004Config.NR);
						}
						else resPred[i][j] = RelationType.get(maxRelId);
					}
				}
				res.setPrediction(resPred);
			}
			RelationType[][] prediction = res.getPrediction();
			for (int i = 0; i < spanLen; i++) {
				for (int j = i + 1; j < spanLen; j++) {
					RelationType rel = prediction[i][j];
					String predForm = rel.form;
					String goldForm = gold[i][j].form;
					int corrPredId = rel.id;
					int corrGoldId = gold[i][j].id;
					if (predForm.endsWith(ACE2004Config.REV_SUFF)) {
						corrPredId = RelationType.get(predForm.replace(ACE2004Config.REV_SUFF, "")).id;
					}
					if (goldForm.endsWith(ACE2004Config.REV_SUFF)) {
						corrGoldId = RelationType.get(goldForm.replace(ACE2004Config.REV_SUFF, "")).id;
					}
					totalPredict[corrPredId]++;
					if (rel.equals(gold[i][j])) {
						p[corrPredId]++;
					}
					totalInData[corrGoldId]++;
				}
			}
		}
		
		int allP = 0;
		int allPredict = 0;
		int allInData = 0;
		for (int r = 0; r < RelationType.RELS.size(); r++) {
			if (RelationType.get(r).form.equals(ACE2004Config.NR)|| RelationType.get(r).form.endsWith(ACE2004Config.REV_SUFF)) continue;
			double precision = p[r]*1.0/totalPredict[r]*100;
			double recall = p[r] * 1.0 / totalInData[r]*100;
			double fscore = 2.0 * p[r] / (totalPredict[r] + totalInData[r]) * 100;
			System.out.printf("[Result] %s: %sPrec.:%.2f%%\tRec.:%.2f%%\tF1.:%.2f%%\n", 
					RelationType.get(r).form, " ", precision, recall, fscore);
			allP += p[r];
			allPredict += totalPredict[r];
			allInData += totalInData[r];
		}
		double precision = allP * 1.0/ allPredict * 100;
		double recall = allP * 1.0 / allInData * 100;
		double fscore = 2.0 * allP / (allPredict + allInData) * 100;
		System.out.printf("[Result] number of unpredictable: %d, out of %d, (%.2f%%)\n", unpred, allInData, unpred*1.0/allInData*100);
		System.out.printf("[Result] All: \t\tPrec.:%.2f%%\tRec.:%.2f%%\tF1.:%.2f%%\n", 
				precision, recall, fscore);
		metrics[0] = precision; 
		metrics[1] = recall;
		metrics[2] = fscore;
		return metrics;
	}
	
}
