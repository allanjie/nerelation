package org.statnlp.example.nerelation.tree;

import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.Span;

public class TreeUtils {

	public static int checkConnected(TreeInstance inst){
		int number = 0;
		List<Span> output = inst.getInput().spans;
		Sentence sent = inst.getInput().sent;
		int[][] leftNodes = sent2LeftDepRel(sent);
		for(Span span: output){
			int start = span.start;
			int end = span.end;
			Entity label = span.entity;
			if(label.equals(Entity.get("O")) || start==end) continue;
			boolean connected = traverseLeft(start, end, leftNodes);
			if(!connected) number++;
		}
//		if(number>0)
//			System.out.println(sent.toString());
		return number;
	}
	
	private static boolean traverseLeft(int start, int end, int[][] leftNodes){
		for(int l=0; l<leftNodes[end].length; l++){
			if(leftNodes[end][l]<start) continue;
			if(leftNodes[end][l]==start)
				return true;
			else if(traverseLeft(start, leftNodes[end][l], leftNodes))
				return true;
			else continue;
		}
		return false;
	}
	
	/**
	 * The head Index is 0-indexed.
	 * @param sent
	 * @return
	 */
	private static int[][] sent2LeftDepRel(Sentence sent){
		int[][] leftDepRel = new int[sent.length()][];
		ArrayList<ArrayList<Integer>> leftDepList = new ArrayList<ArrayList<Integer>>();
		for(int i=0;i<leftDepRel.length;i++) leftDepList.add(new ArrayList<Integer>());
		for(int pos = 0; pos<sent.length(); pos++){
			int headIdx = sent.get(pos).getHeadIndex();
			if(headIdx<0) continue;
			int smallOne = Math.min(pos, headIdx);
			int largeOne = Math.max(pos, headIdx);
			ArrayList<Integer> curr = leftDepList.get(largeOne);
			curr.add(smallOne);
		}
		for(int pos=0; pos<sent.length(); pos++){
			ArrayList<Integer> curr = leftDepList.get(pos);
			leftDepRel[pos] = new int[curr.size()];
			for(int j=0; j<curr.size();j++)
				leftDepRel[pos][j] = curr.get(j);
		}
		return leftDepRel;
	}
	
}
