package org.statnlp.example.nerelation.tree;

import java.util.ArrayList;
import java.util.List;

import org.statnlp.example.nerelation.ACE2004Config;
import org.statnlp.example.nerelation.RESentence;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;
import org.statnlp.example.nerelation.tree.TreeNetworkCompiler.NodeType;
import org.statnlp.hypergraph.FeatureArray;
import org.statnlp.hypergraph.FeatureManager;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.Network;

import gnu.trove.stack.TIntStack;
import gnu.trove.stack.array.TIntArrayStack;

public class TreeFeatureManager extends FeatureManager {

	private static final long serialVersionUID = -2151329171672892994L;

	private enum FeaType {wm1, hm1, wm2, hm2, hm12,
		wbnull, wbfl, wbf, wbl, wbo, lexbigram, bm1f, bm1l, am2f, am2l,
		et12, etsub12, ml12, ml12et12, ml12etsub12, nummb,numwb, m1cm2, m2cm1, et12m1cm2, et12m2cm1, etsub12m1cm2, etsub12m2cm1, hm12m1cm2, hm12m2cm1,
		cm1m1, cp1p1, cm2m1, cm1p1, cp1p2,
		cphbnull, cphbfl, cphbf, cphbl, cphbo, cphbm1f, cphbm1l,cpham2f, cpham2l, cpp, cpph,
		h1head, h2head, depPath, depLabel, transition, mid_transition, mid_entity, mid_sub, mid_ml12, mid_ml12et12, mid_ml12etsub12}
	
	public TreeFeatureManager(GlobalNetworkParam param_g) {
		super(param_g);
	}

	@Override
	protected FeatureArray extract_helper(Network network, int parent_k, int[] children_k, int children_k_index) {

		int[] parentArr = network.getNodeArray(parent_k);
		if (parentArr[3] == NodeType.root.ordinal())
			return FeatureArray.EMPTY;
		int rightIdx = parentArr[0];
		int leftIdx = parentArr[0] - parentArr[1];
		int relId = parentArr[2];
		String rel = relId + "";
		String relForm = RelationType.get(relId).form;
		TreeInstance inst = (TreeInstance)network.getInstance();
		
		int arg1Idx = leftIdx;
		int arg2Idx = rightIdx;
		if (relForm.endsWith(ACE2004Config.REV_SUFF)) {
			arg1Idx = rightIdx;
			arg2Idx = leftIdx;
		}
		
		SentSpan input = inst.getInput();
		List<Span> spans = input.spans;
		RESentence sent = input.sent;
		Span arg1Span = spans.get(arg1Idx);
		Span arg2Span = spans.get(arg2Idx);
		int e1Id = arg1Span.entity.id;
		int e2Id = arg2Span.entity.id;
		int type1Id = arg1Span.type.id; 
		int type2Id = arg2Span.type.id;
		
		//simple features as bag-of-words.
		List<Integer> fs = new ArrayList<>();
		
		int hm1Idx = arg1Span.headIdx;
		int hm2Idx = arg2Span.headIdx;
		String hm1 = sent.get(hm1Idx).getForm();
		String hm2 = sent.get(hm2Idx).getForm();
		
		
		if (children_k.length > 0) {
			int child_1 = children_k[0];
			int[] c1arr = network.getNodeArray(child_1);
			int c1right = c1arr[0];
			int c1relId = c1arr[2];
			int child_2 = children_k[1];
			int[] c2arr = network.getNodeArray(child_2);
			int c2relId = c2arr[2];
			Span midSpan = spans.get(c1right);
			//fs.add(this._param_g.toFeature(network, FeaType.transition.name(), rel, c1relId + " " + c2relId));
			fs.add(this._param_g.toFeature(network, FeaType.mid_transition.name(), rel, c1relId + " " + c2relId + " " + midSpan.entity.id));
//			int midEntId = midSpan.entity.id;
//			fs.add(this._param_g.toFeature(network, FeaType.mid_entity.name(), rel, e1Id + " " + midEntId + " " + e2Id));
//			fs.add(this._param_g.toFeature(network, FeaType.mid_sub.name(), rel, arg1Span.subEnt + " " + midSpan.subEnt + " " + arg2Span.subEnt));
//			fs.add(this._param_g.toFeature(network, FeaType.mid_ml12.name(), rel, type1Id + " " + midSpan.type.id + " " + type2Id));
//			fs.add(this._param_g.toFeature(network, FeaType.mid_ml12et12.name(), rel, type1Id + " " + midSpan.type.id + " " + type2Id + " " + e1Id + " " + midEntId + " " + e2Id));
//			fs.add(this._param_g.toFeature(network, FeaType.mid_ml12etsub12.name(), rel, type1Id + " " + midSpan.type.id + " " + type2Id + " " + 
//					arg1Span.subEnt + " " + midSpan.subEnt + " " + arg2Span.subEnt));
		}
		
		fs.add(this._param_g.toFeature(network, FeaType.hm1.name(), rel, hm1));
		fs.add(this._param_g.toFeature(network, FeaType.hm2.name(), rel, hm2));
		fs.add(this._param_g.toFeature(network, FeaType.hm12.name(), rel, hm1 + " " + hm2));
		for (int i = arg1Span.start; i <= arg1Span.end; i++) {
			fs.add(this._param_g.toFeature(network, FeaType.wm1.name(), rel, sent.get(i).getForm()));
		}
		for (int i = arg2Span.start; i <= arg2Span.end; i++) {
			fs.add(this._param_g.toFeature(network, FeaType.wm2.name(), rel, sent.get(i).getForm()));
		}
		int comp = arg1Span.comparePosition(arg2Span);
		Span leftSpan = comp < 0 ? arg1Span : arg2Span;
		Span rightSpan = comp < 0 ? arg2Span : arg1Span;
		if (rightSpan.start - leftSpan.end <= 1) {
			//means no word in-between
			fs.add(this._param_g.toFeature(network, FeaType.wbnull.name(), rel, ""));
		} else if (rightSpan.start - leftSpan.end == 2) {
			//one word in-between
			String wib = sent.get(leftSpan.end + 1).getForm();
			fs.add(this._param_g.toFeature(network, FeaType.wbfl.name(), rel, wib));
		} else if (rightSpan.start - leftSpan.end >= 3) {
			//two words in-between
			String first = sent.get(leftSpan.end + 1).getForm();
			String last = sent.get(rightSpan.start - 1).getForm();
			fs.add(this._param_g.toFeature(network, FeaType.wbf.name(), rel, first));
			fs.add(this._param_g.toFeature(network, FeaType.wbl.name(), rel, last));
			//at least three words in between
			for (int i = leftSpan.end + 2; i < rightSpan.start - 1; i++) {
				String mid = sent.get(i).getForm();
				fs.add(this._param_g.toFeature(network, FeaType.wbo.name(), rel, mid));
			}
			//bigram
			for (int i = leftSpan.end + 1; i < rightSpan.start - 1; i++) {
				String l = sent.get(i).getForm();
				String r = sent.get(i+1).getForm();
				fs.add(this._param_g.toFeature(network, FeaType.lexbigram.name(), rel, l + " " + r));
			}
		} 
		
		//more words feature
		int bm1fIdx = arg1Idx - 1;
		int bm1lIdx = arg1Idx - 2;
		int am2fIdx = arg2Idx + 1;
		int am2lIdx = arg2Idx + 2;
		String bm1f = bm1fIdx >= 0 ? sent.get(bm1fIdx).getForm() : "STR1";
		String bm1l = bm1lIdx >= 0 ? sent.get(bm1lIdx).getForm() : bm1fIdx >= 0 ?  "STR1": "STR2";
		String am2f = am2fIdx < sent.length() ? sent.get(am2fIdx).getForm() : "END1";
		String am2l = am2lIdx < sent.length() ? sent.get(am2lIdx).getForm() : am2fIdx < sent.length() ? "END1" : "END2" ;
		
		fs.add(this._param_g.toFeature(network, FeaType.bm1f.name(), rel, bm1f));
		fs.add(this._param_g.toFeature(network, FeaType.bm1l.name(), rel, bm1l));
		fs.add(this._param_g.toFeature(network, FeaType.am2f.name(), rel, am2f));
		fs.add(this._param_g.toFeature(network, FeaType.am2l.name(), rel, am2l));
		
		//Entity Type and Mention Level features
		fs.add(this._param_g.toFeature(network, FeaType.et12.name(), rel, e1Id + " " + e2Id));
		fs.add(this._param_g.toFeature(network, FeaType.etsub12.name(), rel, arg1Span.subEnt + " " + arg2Span.subEnt));
		fs.add(this._param_g.toFeature(network, FeaType.ml12.name(), rel, type1Id + " " + type2Id));
		fs.add(this._param_g.toFeature(network, FeaType.ml12et12.name(), rel, type1Id + " " + type2Id + " " + e1Id + " " + e2Id));
		fs.add(this._param_g.toFeature(network, FeaType.ml12etsub12.name(), rel, type1Id + " " + type2Id + " " + arg1Span.subEnt + " " + arg2Span.subEnt));
		
		
		//number of mention in between
		int nummb = 0;
		for (int i = leftIdx + 1; i < rightIdx; i++) {
			Span span = spans.get(i);
			if (span.start > leftSpan.end && span.end < rightSpan.start) {
				nummb++;
			}
		}
		fs.add(this._param_g.toFeature(network, FeaType.nummb.name(), rel, nummb + ""));
		
		//number of words in between
		int numwb = rightSpan.start - leftSpan.end - 1;
		numwb = numwb > 0 ? numwb : 0;
		fs.add(this._param_g.toFeature(network, FeaType.numwb.name(), rel, numwb + ""));
		
		//whether m1 includes m2
		boolean m1cm2 = arg1Span.start <= arg2Span.start && arg1Span.end >= arg2Span.end ? true : false;
		//whether m2 includes m1
		boolean m2cm1 = arg2Span.start <= arg1Span.start && arg2Span.end >= arg1Span.end ? true : false;	
		fs.add(this._param_g.toFeature(network, FeaType.m1cm2.name(), rel, m1cm2 + ""));
		fs.add(this._param_g.toFeature(network, FeaType.m2cm1.name(), rel, m2cm1 + ""));
		fs.add(this._param_g.toFeature(network, FeaType.et12m1cm2.name(), rel, e1Id + " " + e2Id + " " + m1cm2));
		fs.add(this._param_g.toFeature(network, FeaType.et12m2cm1.name(), rel, e1Id + " " + e2Id + " " + m2cm1));
		fs.add(this._param_g.toFeature(network, FeaType.etsub12m1cm2.name(), rel, arg1Span.subEnt + " " + arg2Span.subEnt + " " + m1cm2));
		fs.add(this._param_g.toFeature(network, FeaType.etsub12m2cm1.name(), rel, arg1Span.subEnt + " " + arg2Span.subEnt + " " + m2cm1));
		fs.add(this._param_g.toFeature(network, FeaType.hm12m1cm2.name(), rel, hm1 + " " + hm2 + " " + m1cm2 ));
		fs.add(this._param_g.toFeature(network, FeaType.hm12m2cm1.name(), rel, hm1 + " " + hm2 + " " + m2cm1));
		
		
		//collocations features from Chan & Roth, 2010
		String hm1m1 = hm1Idx - 1 >=0 ?  sent.get(hm1Idx - 1).getForm() : "STR1";
		String hm1m2 = hm1Idx - 2 >=0 ?  sent.get(hm1Idx - 2).getForm() : hm1Idx - 1 >= 0 ? "STR1" :"STR2";
		String hm1p1 = hm1Idx + 1 < sent.length() ? sent.get(hm1Idx + 1).getForm() : "END1";
		String hm1p2 = hm1Idx + 2 < sent.length() ? sent.get(hm1Idx + 2).getForm() : hm1Idx + 1 < sent.length() ? "END1" : "END2";
		
		String hm2m1 = hm2Idx - 1 >=0 ?  sent.get(hm2Idx - 1).getForm() : "STR1";
		String hm2m2 = hm2Idx - 2 >=0 ?  sent.get(hm2Idx - 2).getForm() : hm2Idx - 1 >= 0 ? "STR1" :"STR2";
		String hm2p1 = hm2Idx + 1 < sent.length() ? sent.get(hm2Idx + 1).getForm() : "END1";
		String hm2p2 = hm2Idx + 2 < sent.length() ? sent.get(hm2Idx + 2).getForm() : hm2Idx + 1 < sent.length() ? "END1" : "END2";
		
		fs.add(this._param_g.toFeature(network, FeaType.cm1m1.name(), rel, hm1m1 + " " + hm1));
		fs.add(this._param_g.toFeature(network, FeaType.cp1p1.name(), rel, hm1 + " " + hm1p1));
		fs.add(this._param_g.toFeature(network, FeaType.cm2m1.name(), rel, hm1m2 + " " + hm1m1 + " " + hm1));
		fs.add(this._param_g.toFeature(network, FeaType.cm1p1.name(), rel, hm1m1 + " " + hm1 + " " + hm1p1));
		fs.add(this._param_g.toFeature(network, FeaType.cp1p2.name(), rel, hm1 + " " + hm1p1 + " " + hm1p2));
		
		fs.add(this._param_g.toFeature(network, FeaType.cm1m1.name(), rel, hm2m1+ " " + hm2));
		fs.add(this._param_g.toFeature(network, FeaType.cp1p1.name(), rel, hm2 + " " + hm2p1));
		fs.add(this._param_g.toFeature(network, FeaType.cm2m1.name(), rel, hm2m2 + " " + hm2m1 + " " + hm2));
		fs.add(this._param_g.toFeature(network, FeaType.cm1p1.name(), rel, hm2m1 + " " + hm2 + " " + hm2p1));
		fs.add(this._param_g.toFeature(network, FeaType.cp1p2.name(), rel, hm2 + " " + hm2p1 + " " + hm2p2));
		
		//dependency features
		int hm1HeadIdx = sent.get(hm1Idx).getHeadIndex();
		String hm1Head = hm1HeadIdx == -1 ? "<ROOT>" : sent.get(hm1HeadIdx).getForm();
		fs.add(this._param_g.toFeature(network, FeaType.h1head.name(), rel, hm1 + " " + hm1Head));
		int hm2HeadIdx = sent.get(hm2Idx).getHeadIndex();
		String hm2Head = hm2HeadIdx == -1 ? "<ROOT>" : sent.get(hm2HeadIdx).getForm();
		fs.add(this._param_g.toFeature(network, FeaType.h2head.name(), rel, hm2 + " " + hm2Head));
		//this will add the path features.
		this.findPath(sent, hm1Idx, hm2Idx, network, rel, fs);
		
		FeatureArray fa = this.createFeatureArray(network, fs);
		return fa;
	}

	/**
	 * Check if hm1Idx and hm2Idx are equal to each other.
	 * @param sent
	 * @param hm1Idx
	 * @param hm2Idx
	 */
	private void findPath(RESentence sent, int hm1Idx, int hm2Idx, Network network, String rel, List<Integer> fs) {
		StringBuilder path = new StringBuilder("path:");
		TIntStack stack1 = new TIntArrayStack();
		TIntStack stack2 = new TIntArrayStack();
		boolean found1 =  sent.depTree.dfsFind(hm1Idx, stack1);
		boolean found2 = sent.depTree.dfsFind(hm2Idx, stack2);
		if (!(found1 && found2)) throw new RuntimeException("Not found in the dependency tree?");
		int[] trace1 = stack1.toArray();
		int[] trace2 = stack2.toArray();
		int contIdx1 = containsElement(trace1, hm2Idx);
		int contIdx2 = containsElement(trace2, hm1Idx);
		if (contIdx1 >= 0 || contIdx2 >= 0) {
			if (contIdx1 >= 0 && contIdx2 >= 0 && hm1Idx != hm2Idx) throw new RuntimeException("impossible");
			if (contIdx1 >= 0) {
				for (int k = 0; k < contIdx1 ; k++) { // because the trace is backtracked.
					String depLabel = sent.get(trace1[k]).getDepLabel();
					fs.add(this._param_g.toFeature(network, FeaType.depLabel.name(), rel, depLabel));
					if (k==0) path.append(depLabel);
					else path.append(" " + depLabel);
				}
			} else {
				for (int k = contIdx2 - 1; k >=0 ; k--) { // because the trace is backtracked.
					String depLabel = sent.get(trace2[k]).getDepLabel();
					fs.add(this._param_g.toFeature(network, FeaType.depLabel.name(), rel, depLabel));
					if (k==contIdx2 - 1) path.append(depLabel);
					else path.append(" " + depLabel);
				}
			}
		} else {
			int stamp1 = trace1.length - 1;
			int stamp2 = trace2.length - 1;
			while (stamp1 >= 0 && stamp2 >= 0) {
				if (trace1[stamp1] != trace2[stamp2]) {
					stamp1++;
					stamp2++;
					break;
				} else {
					stamp1--;
					stamp2--;
				}
			}
			//trace1[stamp1] should be equal to trace2[stamp2]
			for (int k = 0; k < stamp1; k++) {
				String depLabel = sent.get(trace1[k]).getDepLabel();
				fs.add(this._param_g.toFeature(network, FeaType.depLabel.name(), rel, depLabel));
				if (k==0) path.append(depLabel);
				else path.append(" " + depLabel);
			}
			for (int k = stamp2 - 1; k >=0; k--) {
				String depLabel = sent.get(trace2[k]).getDepLabel();
				fs.add(this._param_g.toFeature(network, FeaType.depLabel.name(), rel, depLabel));
				path.append(" " + depLabel);
			}
		}
		//Note: the path is label path
		fs.add(this._param_g.toFeature(network, FeaType.depPath.name(), rel, path.toString()));
	}
	
	private int containsElement(int[] arr, int val) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == val)
				return i;
		}
		return -1;
	}
}
