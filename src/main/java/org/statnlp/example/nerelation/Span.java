package org.statnlp.example.nerelation;


public class Span implements Comparable<Span>{

	public Entity entity;
	public MentionType type;
	public String subEnt;
	public int start = -1;
	public int end = -1;
	/**
	 * The head span annotated in the ACE dataset.
	 */
	public int headStart = -1;
	public int headEnd = - 1;
	
	/**
	 * The head index of the mention, only one word.
	 */
	public int headIdx = -1;
	
	/**
	 * Span constructor
	 * @param start: inclusive
	 * @param end: inclusive
	 * @param headStart: needed for ACE dataset
	 * @param headEnd: needed for ACE dataset
	 * @param entity
	 */
	public Span(int start, int end, int headStart, int headEnd, Entity entity) {
		this.start = start;
		this.end = end;
		this.headStart = headStart;
		this.headEnd = headEnd;
		this.entity = entity;
	}
	
	public Span(int start, int end, int headStart, int headEnd, Entity entity, MentionType type, String subEnt) {
		this(start, end, headStart, headEnd, entity);
		this.type = type;
		this.subEnt = subEnt;
	}
	
	
	public Span(int start, int end, Entity entity) {
		this(start, end, -1, -1, entity, null, null);
	}
	
	public Span(int start, int end, Entity entity, MentionType type) {
		this(start, end, -1, -1, entity, type, null);
	}
	
	public Span(int start, int end, Entity entity, MentionType type, String subEnt) {
		this(start, end, -1, -1, entity, type, subEnt);
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + end;
		result = prime * result + ((entity == null) ? 0 : entity.form.hashCode());
		result = prime * result + start;
		return result;
	}
	
	public boolean equals(Object o){
		if(o instanceof Span){
			Span s = (Span)o;
			if(start != s.start) return false;
			if(end != s.end) return false;
//			if(headStart != s.headStart) return false;
//			if(headEnd != s.headEnd) return false;
			return entity.equals(s.entity);
		}
		return false;
	}

	@Override
	public int compareTo(Span o) {
		if(start < o.start) return -1;
		if(start > o.start) return 1;
		if(end < o.end) return -1;
		if(end > o.end) return 1;
		return entity.compareTo(o.entity);
	}
	
	public int comparePosition(Span o) {
		if(start < o.start) return -1;
		if(start > o.start) return 1;
		if(end < o.end) return -1;
		if(end > o.end) return 1;
		return 0;
	}
	
	public boolean overlap(Span other) {
		if (other.start > this.end) return false;
		if (other.end < this.start) return false;
		return true;
	}
	
	public boolean type2Overlap(Span other) {
		if (!this.entity.equals(other.entity)) return false;
		//it's like always type 1, which is nested.
		if (this.start<= other.end && this.start>other.start && this.end > other.end) return true;
		if (other.start<= this.end && other.start>this.start  && this.end < other.end) return true;
		//the following also return true for nested entities.
//		if (this.start < other.start && this.end > other.end) return true;
//		if (other.start < this.start && other.end > this.end) return true;
//		if (this.start<= other.end && this.start>other.start && this.end != other.end) return true;
//		if (other.start<= this.end && other.start>this.start  && this.end != other.end) return true;
		return false;
	}
	
	public String toString(){
		return String.format("%d,%d,%d,%d %s", start, end, headStart, headEnd, entity);
//		return entity.form;
	}
}
