package org.statnlp.example.nerelation.baseline;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;

import data.preprocessing.ace.ACECrossRelStat;

public class BaselineReader {

	private boolean useHeadStart;
	private boolean useHeadEnd;
	
	private boolean ignoreCrossOverlappingRelation;
	private int overlappingRelationNum;
	
	private boolean ignoreAdjacentSameTypeSecondArg;
	private int ignoreAdjacentSameTypeSecondArgNum;
	
	private boolean ignoreMultipleArg1SameTypeSameRelation;
	private int ignoreMultipleArg1SameTypeSameRelationNum;
	
	private boolean ignoreArg1Arg2OverlapRelation = true;
	private int arg1Arg2OverlapNum;
	
	private boolean ignoreOverlapEntities = true;
	private int overlapEntitiesNum;
	
	private int numRelationsMissingEntities;
	
	private int maxEntSize;
	private int maxNumEntityAtSamePos;
	public static final String REV_SUFF = "-rev";
	private static final String DISC = "Discourse";
	
	private boolean useRules;
	private RelRule rules;
	
	public BaselineReader (boolean useHeadStart, boolean useHeadEnd, boolean ignoreAdjacentSameTypeSecondArg, boolean ignoreOverlappingRelation,
			boolean ignoreMultipleArg1SameTypeSameRelation, boolean useRules) {
		this.useHeadStart = useHeadStart;
		this.useHeadEnd = useHeadEnd;
		this.ignoreAdjacentSameTypeSecondArg = ignoreAdjacentSameTypeSecondArg;
		this.ignoreAdjacentSameTypeSecondArgNum = 0;
		this.ignoreCrossOverlappingRelation = ignoreOverlappingRelation;
		this.overlappingRelationNum = 0;
		this.ignoreMultipleArg1SameTypeSameRelation = ignoreMultipleArg1SameTypeSameRelation;
		this.ignoreMultipleArg1SameTypeSameRelationNum = 0;
		this.arg1Arg2OverlapNum = 0;
		this.overlapEntitiesNum = 0;
		this.numRelationsMissingEntities = 0;
		this.useRules = useRules;
	}
	
	public BaselineInstance[] read(String file, boolean isLabeled, int number) {
		List<BaselineInstance> data = new ArrayList<>();
		maxEntSize = -1;
		maxNumEntityAtSamePos = 0;
		if (useRules && isLabeled) {
			rules = new RelRule();
		}
		try {
			BufferedReader br = RAWF.reader(file);
			String line = null;
			int instId = 1;
			while ((line = br.readLine())!= null ) {
				String[] words = line.split(" ");
				line = br.readLine(); // read the pos tag.
				String[] tags = line.split(" ");
				WordToken[] wts = new WordToken[words.length];
				for (int t = 0; t < wts.length; t++) {
					wts[t] = new WordToken(words[t], tags[t]);
				}
				line = br.readLine();
				String[] headIdxs = line.split(" ");
				line = br.readLine();
				String[] depLabels = line.split(" ");
				for (int t = 0; t < wts.length; t++) {
					wts[t].setHead(Integer.valueOf(headIdxs[t])); //might be -1, means root
					wts[t].setDepLabel(depLabels[t]);
				}
				String ners = br.readLine();
				Sentence sent = new Sentence(wts);
				List<Span> spans = new ArrayList<>();
				Set<Span> useless = new HashSet<Span>();
				if (!ners.equals("")) {
					String[] spanInfos = ners.split("\\|");
					for (String spanInfo : spanInfos) {
						String[] spanArr = spanInfo.split(" ");
						String entityType = spanArr[1];
						String[] indices = spanArr[0].split(",");
						int headStart = Integer.valueOf(indices[2]);
						int headEnd = Integer.valueOf(indices[3]) - 1;
						int start = useHeadStart ? headStart : Integer.valueOf(indices[0]);
						int end = useHeadEnd ? headEnd : Integer.valueOf(indices[1]) - 1;
						Span span = new Span(start, end, -1, -1, Entity.get(entityType));
						boolean overlap = this.checkOverlapWithPrevious(spans, span);
						if (overlap) {
							useless.add(span);
						}
						if (isLabeled && ignoreOverlapEntities && overlap) {
							continue;
						} else {
							if (!spans.contains(span)) {
								maxEntSize = Math.max(maxEntSize, span.end - span.start + 1);
								spans.add(span);
							}
						}
					}
					Collections.sort(spans);
				}
				this.overlapEntitiesNum += useless.size();
				int[] usedSpanIdx = new int[spans.size()];
				String allRelations = br.readLine();
				List<List<Relation>> relations = new ArrayList<>(sent.length());
				List<Relation> relList = new ArrayList<>();
				for(int p = 0; p < sent.length(); p++) relations.add(new ArrayList<>());
				if (!allRelations.equals("")) {
					String[] vals = allRelations.split("\\|");
					for (String oneRelation : vals) {
						String[] indices = oneRelation.split(" ");
						String relType = indices[0].split("::")[0];
						if (relType.equals(DISC)) continue;
						String arg1EntType = indices[2];
						String arg2EntType = indices[4];
						String[] firstIndices = indices[1].split(",");
						String[] secondIndices = indices[3].split(",");
						int headStart1 = Integer.valueOf(firstIndices[2]);
						int headEnd1 = Integer.valueOf(firstIndices[3]) - 1;
						int start1 = useHeadStart ? headStart1 : Integer.valueOf(firstIndices[0]);
						int end1 = useHeadEnd ? headEnd1 : Integer.valueOf(firstIndices[1]) - 1;
						Span span1 = new Span(start1, end1, -1, -1,Entity.get(arg1EntType));
						int headStart2 = Integer.valueOf(secondIndices[2]);
						int headEnd2 = Integer.valueOf(secondIndices[3]) - 1;
						int start2 = useHeadStart ? headStart2 : Integer.valueOf(secondIndices[0]);
						int end2 = useHeadEnd ? headEnd2 : Integer.valueOf(secondIndices[1]) - 1;
						Span span2 = new Span(start2, end2, -1, -1, Entity.get(arg2EntType));
						int span1Idx = spans.indexOf(span1);
						int span2Idx = spans.indexOf(span2);
						if (span1Idx < 0 || span2Idx < 0) {
							this.numRelationsMissingEntities++;
							continue;
						}
						int leftIdx = span1Idx < span2Idx ? span1Idx : span2Idx;
						int rightIdx = span1Idx < span2Idx ? span2Idx : span1Idx;
						//define the direction.
						/**means the reversed direction for the relation**/
						String direction = span1Idx < span2Idx ? "" : REV_SUFF;
						
						relType = relType + direction;
						RelationType relationType = RelationType.get(relType);
						Relation relation = new Relation(leftIdx, rightIdx, relationType);
						if (!relList.contains(relation)) {
							if (span1Idx < 0 || span2Idx < 0)
								throw new RuntimeException("smaller than 0?");
							boolean overlap = span1.overlap(span2);
							if (overlap) {
								arg1Arg2OverlapNum++;
							}
							if (isLabeled && ignoreArg1Arg2OverlapRelation && overlap) {
								System.out.println(sent.toString());
								System.out.println(spans.get(leftIdx) + "," + spans.get(rightIdx) + "," + relation.toString()); 
								continue;
							} else {
								relList.add(relation);
								usedSpanIdx[span1Idx] = 1;
								usedSpanIdx[span2Idx] = 1;
								if (isLabeled && rules != null) {
									rules.addLHS(relationType.form+","+spans.get(leftIdx).entity.form);
									rules.addTriple(relationType.form+","+spans.get(leftIdx).entity.form+","+spans.get(rightIdx).entity.form);
								}
							}
						}
					}
					for (Relation relation : relList) {
						relations.get(spans.get(relation.getArg1Idx()).start).add(relation);
					}
				}
				//postprocessing: return an array that relArr[position][relation type]
				Relation[][] relArr = new Relation[sent.length()][];
				for (int i = 0; i < sent.length(); i++) {
					relArr[i] = relations.get(i).toArray(new Relation[relations.get(i).size()]);
				}
				List<Integer> noRelIdxs = new ArrayList<>();
				for (int idx = 0 ; idx < usedSpanIdx.length; idx++) {
					if (usedSpanIdx[idx] == 0) noRelIdxs.add(idx);
				}
				int[] noRelSpanIdxs = new int[noRelIdxs.size()];
				for(int i = 0; i < noRelSpanIdxs.length; i++)
					noRelSpanIdxs[i] = noRelIdxs.get(i);
				RelPosition output = new RelPosition(spans.toArray(new Span[spans.size()]), relArr, noRelSpanIdxs);
				BaselineInstance inst = new BaselineInstance(instId, 1.0, sent, output);
				if (isLabeled) {
					inst.setLabeled();
				} else {
					inst.setUnlabeled();
				}
				maxNumEntityAtSamePos = Math.max(maxNumEntityAtSamePos, this.checkMaxNumberEntityUnderSameStarting(noRelSpanIdxs, spans, sent.length()));
				br.readLine();// empty line;
				if (isLabeled && ACECrossRelStat.checkOverlappingRelation(inst)) {
					overlappingRelationNum++;
					//System.out.println(inst.getInput().toString());
					if (ignoreCrossOverlappingRelation)
						continue;
				}
				if (isLabeled && checkOneArg1SameTypeSameRelation(relArr, output.getSpans())) {
					ignoreMultipleArg1SameTypeSameRelationNum++;
					if (ignoreMultipleArg1SameTypeSameRelation)
						continue;
				}
				if (isLabeled && checkAdjacentSecondArg(relArr, output.getSpans())) {
					//System.out.println(inst.getInput().toString());
					ignoreAdjacentSameTypeSecondArgNum++;
					if (ignoreAdjacentSameTypeSecondArg)
						continue;
				} 
				data.add(inst);
				if (data.size() == number) {
					break;
				}
				instId ++;
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("[Reader] number of instances: " + data.size());
		System.out.println("[Reader] Max entity size: " + maxEntSize);
		System.out.println("[Reader] number entities overlap: " + overlapEntitiesNum + " [ignore entities: " + ignoreOverlapEntities + "]");
		//actually no need to ignore this.
		System.out.println("[Reader] number of insts have overlapping cross relations: " + overlappingRelationNum + " [ignore inst: " + ignoreCrossOverlappingRelation + "]");
		System.out.println("[Reader] number of insts have multiple arg1 under same type same relations: " + ignoreMultipleArg1SameTypeSameRelationNum+ " [ignore inst: " + ignoreMultipleArg1SameTypeSameRelation + "]");
		System.out.println("[Reader] number of adjacent second arg2 insts ignore: " + ignoreAdjacentSameTypeSecondArgNum + " [ignore inst: " + ignoreAdjacentSameTypeSecondArg + "]");
		System.out.println("[Reader] number of arg1 arg2 overlap relations: " + arg1Arg2OverlapNum + "[ignore relations:" + ignoreArg1Arg2OverlapRelation + "]");
		System.out.println("[Reader] number relations missing entities: " + numRelationsMissingEntities  +" [ignore relation:true]" );
		System.out.println("[Reader] max number same entity without relations starting at same position: " + maxNumEntityAtSamePos);
		BaselineInstance[] dataArr = data.toArray(new BaselineInstance[data.size()]);
		//post processing.
		return dataArr;
	}
	
	/**
	 * Check the maximum number of same entities starting at the same position
	 * @param noRelSpanIdxs
	 * @param spans
	 * @param size
	 * @return
	 */
	private int checkMaxNumberEntityUnderSameStarting(int[] noRelSpanIdxs, List<Span> spans, int size) {
		Map<Integer, Map<Entity, Set<Span>>> map = new HashMap<>();
		int maxNum = 0;
		for (int i = 0; i < noRelSpanIdxs.length; i++) {
			Span span = spans.get(noRelSpanIdxs[i]);
			if (map.containsKey(span.start)) {
				Map<Entity, Set<Span>> subMap = map.get(span.start);
				if (subMap.containsKey(span.entity)) {
					subMap.get(span.entity).add(span);
					maxNum = Math.max(maxNum, subMap.get(span.entity).size());
				} else {
					Set<Span> set = new HashSet<>();
					set.add(span);
					subMap.put(span.entity, set);
					maxNum = Math.max(maxNum, set.size());
				}
			} else {
				Map<Entity, Set<Span>> subMap = new HashMap<>();
				Set<Span> set = new HashSet<>();
				set.add(span);
				subMap.put(span.entity, set);
				map.put(span.start, subMap);
				maxNum = Math.max(maxNum, set.size());
			}
		}
		return maxNum;
	}
	
	private boolean checkAdjacentSecondArg(Relation[][] relations, Span[] spans) {
		for (int p = 0; p < relations.length; p++) {
			for (int r = 0; r < relations[p].length; r++) {
				Span arg2one = spans[relations[p][r].getArg2Idx()];
				for (int another = r+1; another < relations[p].length; another++) {
					if (relations[p][r].getRel().equals(relations[p][another].getRel())) {
						Span arg2two = spans[relations[p][another].getArg2Idx()];
						//simply ignore all ajacent second arguments first.
						if(relations[p][r].getArg1Idx() == relations[p][another].getArg1Idx()) {
							if (arg2one.end == arg2two.start - 1 || arg2one.start - 1 == arg2two.end) {
								//System.out.println(arg2one);
								//System.out.println(arg2two);
								//found mostly, PER/PER, ORG/ORG, GPE/GPE, GPE/ORG,
								return true;
							}
						}
//						if(relations[p][r].getArg1Idx() == relations[p][another].getArg1Idx() && arg2one.entity.equals(arg2two.entity)) {
//							if (arg2one.end == arg2two.start - 1 || arg2one.start - 1 == arg2two.end)
//								return true;
//						}
					}
				}
			}
		}
		return false;
	}
	
	private boolean checkOneArg1SameTypeSameRelation(Relation[][] output, Span[] spans) {
		Span[][][] allRels = new Span[output.length][RelationType.RELS.size()][Entity.ENTITIES.size()];
		for (int pos = 0; pos < output.length; pos ++) {
			for (Relation relation : output[pos]) {
				Span arg1Span = spans[relation.getArg1Idx()];
				if (allRels[pos][relation.getRel().id][arg1Span.entity.id] == null) {
					allRels[pos][relation.getRel().id][arg1Span.entity.id] = arg1Span;
				} else {
					if (!arg1Span.equals(allRels[pos][relation.getRel().id][arg1Span.entity.id])) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Check the number of overlapping entities with previous
	 * @param spans
	 * @param span
	 * @return
	 */
	private boolean checkOverlapWithPrevious(List<Span> spans, Span span) {
		for(Span candidate: spans) {
			if (candidate.overlap(span)) 
				return true;
		}
		return false;
	}
	
	public RelRule getRules() {
		return this.rules;
	}
	
}
