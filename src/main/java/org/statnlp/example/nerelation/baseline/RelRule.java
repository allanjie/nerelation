package org.statnlp.example.nerelation.baseline;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class RelRule implements Serializable{

	private static final long serialVersionUID = 8775089331623755889L;
	
	protected Set<String> triples;
	protected Set<String> lhs;
	
	public RelRule() {
		this.triples = new HashSet<>();
		this.lhs = new HashSet<>();
	}
	
	public void addLHS(String lhs) {
		this.lhs.add(lhs);
	}
	
	public void addTriple(String triple) {
		this.triples.add(triple);
	}
	
	public boolean containsLHS(String lhs) {
		return this.lhs.contains(lhs);
	}
	
	public boolean containsTriple(String triple) {
		return this.triples.contains(triple);
	}
	
	public String toString(){
		return triples.toString() + "\n" + lhs.toString();
	}
}
