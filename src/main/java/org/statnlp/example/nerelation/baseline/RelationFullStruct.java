package org.statnlp.example.nerelation.baseline;

import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;

public class RelationFullStruct {

	Span arg1Span;
	Span arg2Span;
	RelationType rel;
	
	public RelationFullStruct(Span arg1Span, Span arg2Span, RelationType rel) {
		this.arg1Span = arg1Span;
		this.arg2Span = arg2Span;
		this.rel = rel;
	}

	@Override
	public String toString() {
		return "RelationFullStruct [arg1Span=" + arg1Span + ", arg2Span=" + arg2Span + ", rel=" + rel + "]";
	}
	
	
}
