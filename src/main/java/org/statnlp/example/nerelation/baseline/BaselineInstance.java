package org.statnlp.example.nerelation.baseline;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseInstance;

public class BaselineInstance extends BaseInstance<BaselineInstance, Sentence, RelPosition> {

	private static final long serialVersionUID = -24065040488574872L;
	
	private int[] depHeads;
	private String[] depLabels;
	
	public BaselineInstance(int instanceId, double weight) {
		super(instanceId, weight);
	}

	public BaselineInstance(int instanceId, double weight, Sentence input, RelPosition output) {
		super(instanceId, weight);
		this.input = input;
		this.output = output;
	}
	
	
	public int[] getDepHeads() {
		return depHeads;
	}

	public void setDepHeads(int[] depHeads) {
		this.depHeads = depHeads;
	}

	public String[] getDepLabels() {
		return depLabels;
	}

	public void setDepLabels(String[] depLabels) {
		this.depLabels = depLabels;
	}

	@Override
	public int size() {
		return this.input.length();
	}
	
	public Sentence duplicateInput(){
		return this.input;
	}
	
	public RelPosition duplicateOutput(){
		return this.output;
	}
	
	public RelPosition duplicatePrediction(){
		return this.prediction;
	}

}
