package org.statnlp.example.nerelation.baseline;

import static org.statnlp.commons.Utils.print;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

/**
 * We assume arg1 always on the left already. 
 * Please ensure this in data preprocessing.
 * @author allanjie
 *
 */
public class BaselineNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -8427069718884521214L;
	public static final boolean DEBUG = false;
	
	public int maxSize = 1;
	public BaseNetwork unlabeledNetwork;
	protected Set<String> rules;
	
	public enum NodeType{
		X_NODE,
		ARG2_NODE,
		O_NODE,
		ARG1_ARG2_NODE,
		ARG1_NODE,
		ARG1_C_NODE,
		I_NODE,
		ARG1_T_NODE,
		R_NODE,
		E_NODE,
		A_NODE,
	}
	
	public BaselineNetworkCompiler(int maxSize) {
		this(maxSize, null);
	}
	
	public BaselineNetworkCompiler(int maxSize, Set<String> rules) {
		this.maxSize = maxSize;
		this.rules = rules;
		this.buildUnlabeled();
	}

	@Override
	public BaseNetwork compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder();
		BaselineInstance bInst = (BaselineInstance)inst;
		//print(bInst.getInput().toString());
		int size = bInst.size();
		long xNode = toNode_X();
		builder.addNode(xNode);
		RelPosition output = bInst.getOutput();
		Span[] spans = output.getSpans();
		int[] noRelIdxs = output.getNoRelSpanIdxs();
		Relation[][][][] relPos = this.convertToModelOutput(output.getRelations(), spans);
		Span[][][] spanPos = this.convertToModelOutputNoRelSpan(size, spans, noRelIdxs);
		long prevANode = -1;
		long prevENode = -1;
		for (int pos = 0; pos < size; pos++) {
			Relation[][][] rels = relPos[pos];
			long aNode = this.toNode_A(pos, size);
			long eNode = this.toNode_E(pos, size);
			builder.addNode(aNode);
			builder.addNode(eNode);
			if (prevANode != -1) {
				builder.addEdge(prevANode, new long[]{prevENode, aNode});
			}
			if (pos == size - 1) {
				builder.addEdge(aNode, new long[]{eNode});
			}
			prevANode = aNode;
			prevENode = eNode;
			long[] allRNodes = new long[RelationType.RELS.size() + 1]; // the last one represents no relation
			long[][] allTNodes = new long[RelationType.RELS.size() + 1][Entity.ENTITIES.size()];
			for (int relId = 0; relId < allRNodes.length; relId++) {
				allRNodes[relId] = this.toNode_R(pos, size, relId);
				builder.addNode(allRNodes[relId]);
				for (int arg1LabelId = 0; arg1LabelId < Entity.ENTITIES.size(); arg1LabelId++) {
					allTNodes[relId][arg1LabelId] = this.toNode_ARG1_T(pos, size, relId, arg1LabelId);
					builder.addNode(allTNodes[relId][arg1LabelId]);
				}
				if (relId != RelationType.RELS.size()) {
					for (int arg1LabelId = 0; arg1LabelId < Entity.ENTITIES.size(); arg1LabelId++) {
						long prevNode = allTNodes[relId][arg1LabelId];
						if (rels[relId][arg1LabelId].length > 0) {
							Span arg1Span = spans[rels[relId][arg1LabelId][0].getArg1Idx()];
							int leftMostIdx = arg1Span.start;
							int rightMostIdx = getRightMost(rels[relId][arg1LabelId], spans);
							long[] chains = new long[rightMostIdx - leftMostIdx + 1];
							Arrays.fill(chains, -1L);
//							print("arg1span: "+ arg1Span.toString());
							int arg2LeftMost = this.getArg2SpanLeftMost(rels[relId][arg1LabelId], spans);
							for(int idx = arg1Span.start; idx <= arg1Span.end; idx++) {
								long currNode = this.toNode_ARG1(idx, size, relId, arg1LabelId);
								if (idx > arg2LeftMost) currNode = this.toNode_ARG1_C(idx, size, relId, arg1LabelId);
								chains[idx - leftMostIdx] = currNode;
							}
							for (Relation relation : rels[relId][arg1LabelId]) {
//								System.out.println(relation.toString());
								Span arg2Span = spans[relation.getArg2Idx()];
								int arg2LabelId = arg2Span.entity.id;
								for (int idx = arg1Span.end + 1; idx <= arg2Span.start - 1; idx++) {
									if (chains[idx - leftMostIdx] == -1) {
										//the O node is also respect to the relId and arg1LabelId.
										long currNode = this.toNode_O(idx, size, relId, arg1LabelId);
										chains[idx - leftMostIdx] = currNode;
									}
								}
//								print("arg2Span: "+ arg2Span.toString());
								for (int idx = arg2Span.start; idx <= arg2Span.end; idx++) {
									long currNode = -1;
									if (idx >= arg1Span.start && idx <= arg1Span.end) {
										currNode = this.toNode_ARG1_ARG2(idx, size, relId, arg1LabelId, arg2LabelId);
									} else {
										currNode = this.toNode_ARG2(idx, size, relId, arg2LabelId);
									}
									chains[idx - leftMostIdx] = currNode;
								}
							}
//							for (int x = 0; x < chains.length; x++) {
//								int[] arr = NetworkIDMapper.toHybridNodeArray(chains[x]);
//								System.out.print(NodeType.values()[arr[1]] + " ");
//							}
//							System.out.println("");
							for (int i = 0; i < chains.length; i++) {
								if (chains[i] == -1) {
									print(inst.getInput().toString());
									print(RelationType.get(arg1LabelId).form);
									print(arg1Span.toString());
									print(leftMostIdx + "," + rightMostIdx);
									throw new RuntimeException("not completed?");
								}
								builder.addNode(chains[i]);
								builder.addEdge(prevNode, new long[]{chains[i]});
								prevNode = chains[i];
							}
						} 
						builder.addEdge(prevNode, new long[]{xNode});
					}
				}
				builder.addEdge(allRNodes[relId], allTNodes[relId]);
			}
			builder.addEdge(eNode, allRNodes);
		}
		
		for (int pos = 0; pos < size; pos++) {
//			print("curr position: " + pos, System.err);
			for (int arg1LabelId = 0; arg1LabelId < Entity.ENTITIES.size(); arg1LabelId++) {
//				print("curr label: " + arg1LabelId, System.err);
				long tNode = this.toNode_ARG1_T(pos, size, RelationType.RELS.size(), arg1LabelId);
				if (spanPos[pos][arg1LabelId].length == 0) {
					builder.addEdge(tNode, new long[]{xNode});
					continue;
				}
				int rightMostIdx = this.getSpanRightMostEnd(spanPos[pos][arg1LabelId]);
				long[] chain = new long[rightMostIdx - pos + 1];
				boolean[] isEnd = new boolean[rightMostIdx - pos + 1];
				for (Span span : spanPos[pos][arg1LabelId]) {
					isEnd[span.end - pos] = true;
				}
//				print("span length: " + spanPos[pos][arg1LabelId].length, System.err);
				if (!isEnd[rightMostIdx - pos]) throw new RuntimeException("last one is not end?");
				long prevNode = tNode;
				for (int i = 0; i < chain.length; i++) {
					//print("curr i: " + i, System.err);
					//print("pos: " + (pos + i));
					chain[i] = this.toNode_I(pos + i, size, arg1LabelId);
					builder.addNode(chain[i]);
					List<long[]> prevNodeChildren = builder.getChildren_tmp(prevNode);
					if (prevNodeChildren != null) {
						if (prevNodeChildren.size() != 1) throw new RuntimeException("children size is not 1?");
						if (prevNodeChildren.get(0).length > 1) {
							//do nothing
						} else {
							if (prevNodeChildren.get(0).length == 0) throw new RuntimeException("children 0");
							//should be 1. 
							if (prevNodeChildren.get(0)[0] == xNode) {
								prevNodeChildren.clear();
								builder.addEdge(prevNode, new long[]{xNode, chain[i]});
							}//else do nothing.
						}
					} else {
						builder.addEdge(prevNode, new long[]{chain[i]});
					}
					if (isEnd[i]) {
//						print("adding end at pos: " + (pos + i));
						List<long[]> currNodeChildren = builder.getChildren_tmp(chain[i]);
						if (currNodeChildren != null && currNodeChildren.size() > 0) {
							if (currNodeChildren.size() != 1) throw new RuntimeException("children list size not 1 ?");
							if (currNodeChildren.get(0)[0] != xNode) {
								long node = currNodeChildren.get(0)[0];
								currNodeChildren.clear();
								builder.addEdge(chain[i], new long[]{xNode, node});
							}
						} else {
							builder.addEdge(chain[i], new long[]{xNode});
						}
					}
					prevNode = chain[i];
				}
			}
		}
		BaseNetwork network =  builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = compileUnlabeled(networkId, inst, param);
			boolean contained = unlabeled.contains(network);
			if (!contained) {
				print("Contained: "+contained);
				throw new RuntimeException("Labeled network is not contained.");
			}
		}
		return network;
	}
	
	/**
	 * Debugging code to see whether the network ends with X
	 * @param network
	 * @param node_k
	 */
	@SuppressWarnings("unused")
	private void findXNode(BaseNetwork network, int node_k) {
		if (network.getChildren(node_k).length == 0 && 
				NodeType.values()[network.getNodeArray(node_k)[1]] != NodeType.X_NODE) {
			throw new RuntimeException("can't find X node");
		}
		if (network.getChildren(node_k) == null && 
				NodeType.values()[network.getNodeArray(node_k)[1]] != NodeType.X_NODE) {
			throw new RuntimeException("can't find X node");
		}
		if (network.getChildren(node_k).length != 1) 
			throw new RuntimeException("ambiguous?");
		for (int[] children : network.getChildren(node_k)) {
			for (int child : children) {
				this.findXNode(network, child);
			}
		}
		
	}
	
	private int getSpanRightMostEnd(Span[] spans) {
		int rightMostIdx = -1;
		for(Span span : spans) {
			rightMostIdx = Math.max(rightMostIdx, span.end);
			rightMostIdx = Math.max(rightMostIdx, span.end);
		}
		return rightMostIdx;
	}
	
	private int getRightMost(Relation[] relations, Span[] spans) {
		int rightMostIdx = -1;
		for(Relation relation : relations) {
			rightMostIdx = Math.max(rightMostIdx, spans[relation.getArg1Idx()].end);
			rightMostIdx = Math.max(rightMostIdx, spans[relation.getArg2Idx()].end);
		}
		return rightMostIdx;
	}

	private int getArg2SpanLeftMost(Relation[] relations, Span[] spans) {
		int leftMostIdx = 100000;
		for(Relation relation : relations) {
			leftMostIdx = Math.min(leftMostIdx, spans[relation.getArg2Idx()].start);
			leftMostIdx = Math.min(leftMostIdx, spans[relation.getArg2Idx()].start);
		}
		return leftMostIdx;
	}
	
	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		//return this.compileLabeled(networkId, inst, param);
		
		int size = inst.size();
		long root = toNode_A(0, size);
		long[] allNodes = unlabeledNetwork.getAllNodes();
		int[][][] allChildren = unlabeledNetwork.getAllChildren();
		int root_k = unlabeledNetwork.getNodeIndex(root);
		int numNodes = root_k + 1;
		BaseNetwork network = NetworkBuilder.quickBuild(networkId, inst, allNodes, allChildren, numNodes, param, this);
		return network;
		
	}
	
	public void buildUnlabeled() {
		print("Building generic unlabeled tree up to size "+maxSize+"...");
		long startTime = System.currentTimeMillis();
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder();
		int size = maxSize;

		long xNode = this.toNode_X();
		builder.addNode(xNode);
		for (int pos = size - 1; pos >= 0; pos--) {
			long[] rNodes = new long[RelationType.RELS.size() + 1];
			long[][] tNodes = new long[rNodes.length][Entity.ENTITIES.size()];
			for (int rId = 0; rId < rNodes.length; rId++) {
				rNodes[rId] = this.toNode_R(pos, size, rId);
				builder.addNode(rNodes[rId]);
				if (rId != RelationType.RELS.size()) {
					for(int arg1LabelId = 0; arg1LabelId < tNodes[rId].length; arg1LabelId++) {
						tNodes[rId][arg1LabelId] = this.toNode_ARG1_T(pos, size, rId, arg1LabelId);
						builder.addNode(tNodes[rId][arg1LabelId]);
						long arg1Node = this.toNode_ARG1(pos, size, rId, arg1LabelId);
						long arg1CNode = this.toNode_ARG1_C(pos, size, rId, arg1LabelId);
						builder.addNode(arg1Node);
						builder.addNode(arg1CNode);
						builder.addEdge(tNodes[rId][arg1LabelId], new long[]{arg1Node});
						builder.addEdge(tNodes[rId][arg1LabelId], new long[]{xNode});
						if (pos > 0) {
							builder.addEdge(arg1CNode, new long[]{xNode}); //actually only if previous already have arg2.
						}
						//build arg1Node.
						if (pos < size - 1) {
							if (pos + 1 < size - 1) {
								long nextArg1Node = this.toNode_ARG1(pos + 1, size, rId, arg1LabelId);
								builder.addEdge(arg1Node, new long[]{nextArg1Node});
								long nextArg1CNode = this.toNode_ARG1_C(pos + 1, size, rId, arg1LabelId);
								builder.addEdge(arg1CNode, new long[]{nextArg1CNode});
								long oNode = this.toNode_O(pos + 1, size, rId, arg1LabelId);
								builder.addEdge(arg1Node, new long[]{oNode});
								builder.addEdge(arg1CNode, new long[]{oNode});
							}
							for (int arg2LabelId = 0; arg2LabelId < Entity.ENTITIES.size(); arg2LabelId++) {
								if (rules != null) {
									if (!rules.contains(RelationType.get(rId).form + " " + Entity.get(arg1LabelId).form + " " + Entity.get(arg2LabelId).form))
										continue;
								}
								long nextArg2Node = this.toNode_ARG2(pos + 1, size, rId, arg2LabelId);
								builder.addEdge(arg1Node, new long[]{nextArg2Node});
								builder.addEdge(arg1CNode, new long[]{nextArg2Node});
								long nextArg1Arg2Node = this.toNode_ARG1_ARG2(pos + 1, size, rId, arg1LabelId, arg2LabelId);
								builder.addEdge(arg1Node, new long[]{nextArg1Arg2Node});
								builder.addEdge(arg1CNode, new long[]{nextArg1Arg2Node});
							}
						}
						long oNode = this.toNode_O(pos, size, rId, arg1LabelId);
						if (pos > 0) {
							if (pos < size - 1) {
								
								if (pos + 1 < size - 1) {
									long nextONode = this.toNode_O(pos + 1, size, rId, arg1LabelId);
									builder.addNode(oNode);
									builder.addEdge(oNode, new long[]{nextONode});
								}
								for (int arg2LabelId = 0; arg2LabelId < Entity.ENTITIES.size(); arg2LabelId++) {
									if (rules != null) {
										if (!rules.contains(RelationType.get(rId).form + " " + Entity.get(arg1LabelId).form + " " + Entity.get(arg2LabelId).form))
											continue;
									}
									long nextArg2Node = this.toNode_ARG2(pos + 1, size, rId, arg2LabelId);
									builder.addNode(oNode);
									builder.addEdge(oNode, new long[]{nextArg2Node});
								}
							}
						}
						for (int arg2LabelId = 0; arg2LabelId < Entity.ENTITIES.size(); arg2LabelId++) {
							if (rules != null) {
								if (!rules.contains(RelationType.get(rId).form + " " + Entity.get(arg1LabelId).form + " " + Entity.get(arg2LabelId).form))
									continue;
							}
							if (pos > 0) {
								long arg2Node = this.toNode_ARG2(pos, size, rId, arg2LabelId);
								builder.addNode(arg2Node);
								builder.addEdge(arg2Node, new long[]{xNode});
								if (pos < size - 1) {
									//TODO: we don't consider the ajacent second arguments to be adjacents.
									long nextArg2Node = this.toNode_ARG2(pos + 1, size, rId, arg2LabelId);
									builder.addEdge(arg2Node, new long[]{nextArg2Node});
									if (pos < size - 2) {
										long nextONode = this.toNode_O(pos + 1, size, rId, arg1LabelId);
										builder.addEdge(arg2Node, new long[]{nextONode});
									}
								}
							}
							//the reverse suff only within one direction.
							long arg1Arg2Node = this.toNode_ARG1_ARG2(pos, size, rId, arg1LabelId, arg2LabelId);
							builder.addNode(arg1Arg2Node);
							builder.addEdge(arg1Arg2Node, new long[]{xNode});
							builder.addEdge(tNodes[rId][arg1LabelId], new long[]{arg1Arg2Node});
							if (pos < size - 1) {
								//not allowed: since we always make  sure arg1 is shorter one.
								if (pos + 1 < size - 1){
									long nextArg1CNode = this.toNode_ARG1_C(pos + 1, size, rId, arg1LabelId);
									builder.addEdge(arg1Arg2Node, new long[]{nextArg1CNode});
									long nextONode = this.toNode_O(pos + 1, size, rId, arg1LabelId);
									builder.addEdge(arg1Arg2Node, new long[]{nextONode});
								}
								long nextArg2Node = this.toNode_ARG2(pos + 1, size, rId, arg2LabelId);
								builder.addEdge(arg1Arg2Node, new long[]{nextArg2Node});
								long nextArg1Arg2Node = this.toNode_ARG1_ARG2(pos + 1, size, rId, arg1LabelId, arg2LabelId);
								builder.addEdge(arg1Arg2Node, new long[]{nextArg1Arg2Node});
							}
						}
					}
				} else {
					for(int tId = 0; tId < tNodes[rId].length; tId++) {
						Entity entLabel = Entity.get(tId);
						long iNode = toNode_I(pos, size, entLabel.id);
						builder.addNode(iNode);
						builder.addEdge(iNode, new long[]{xNode});
						if (pos < size - 1) {
							long nextINode = toNode_I(pos + 1, size, entLabel.id);
							builder.addEdge(iNode, new long[]{nextINode});
							//TODO: since we found max number of same entity starting from same position is 1.
							//builder.addEdge(iNode, new long[]{xNode, nextINode});
						}
						tNodes[rId][tId] = this.toNode_ARG1_T(pos, size, rId, tId);
						builder.addNode(tNodes[rId][tId]);
						builder.addEdge(tNodes[rId][tId], new long[]{xNode});
						builder.addEdge(tNodes[rId][tId], new long[]{iNode});
					}
				}
				builder.addEdge(rNodes[rId], tNodes[rId]);
			}
			
			long eNode = this.toNode_E(pos, size);
			builder.addNode(eNode);
			builder.addEdge(eNode, rNodes);
			long aNode = this.toNode_A(pos, size);
			builder.addNode(aNode);
			if (pos < size - 1) {
				long nextANode = this.toNode_A(pos + 1, size);
				builder.addEdge(aNode, new long[]{eNode, nextANode});
			} else {
				builder.addEdge(aNode, new long[]{eNode});
			}
		}
		
		BaseNetwork network = builder.buildRudimentaryNetwork();
		this.unlabeledNetwork = network;
		print("#nodes in unlabeled: " + this.unlabeledNetwork.getAllNodes().length);
		long endTime = System.currentTimeMillis();
		System.err.println(String.format("Done in %.3fs", (endTime-startTime)/1000.0));
	}

	private long toNode_X() {
		return this.toNode(0, 0, NodeType.X_NODE, 0, 0, 0);
	}
	
	private long toNode_ARG2(int pos, int size, int relId, int arg2LabelId) {
		return this.toNode(pos, size, NodeType.ARG2_NODE, relId, Entity.ENTITIES.size(), arg2LabelId);
	}
	
	private long toNode_O(int pos, int size, int relId, int arg1LabelId) {
		return this.toNode(pos, size, NodeType.O_NODE, relId, arg1LabelId, 0);
	}
	
	private long toNode_ARG1_ARG2(int pos, int size, int relId, int arg1LabelId, int arg2LabelId) {
		return this.toNode(pos, size, NodeType.ARG1_ARG2_NODE, relId, arg1LabelId, arg2LabelId);
	}
	
	private long toNode_ARG1(int pos, int size, int relId, int arg1LabelId) {
		return this.toNode(pos, size, NodeType.ARG1_NODE, relId, arg1LabelId, Entity.ENTITIES.size());
	}
	
	private long toNode_ARG1_C(int pos, int size, int relId, int arg1LabelId) {
		return this.toNode(pos, size, NodeType.ARG1_C_NODE, relId, arg1LabelId, Entity.ENTITIES.size());
	}
	
	private long toNode_I(int pos, int size, int entId) {
		return this.toNode(pos, size, NodeType.I_NODE, RelationType.RELS.size(), entId, Entity.ENTITIES.size());
	}

	private long toNode_ARG1_T(int pos, int size, int relId, int arg1LabelId) {
		return this.toNode(pos, size, NodeType.ARG1_T_NODE, relId, arg1LabelId, Entity.ENTITIES.size());
	}
	
	private long toNode_R(int pos, int size, int relId) {
		return this.toNode(pos, size, NodeType.R_NODE, relId, 0, 0);
	}
	
	private long toNode_E(int pos, int size) {
		return this.toNode(pos, size, NodeType.E_NODE, 0, 0, 0);
	}
	
	private long toNode_A(int pos, int size) {
		return this.toNode(pos, size, NodeType.A_NODE, 0, 0, 0);
	}

	private long toNode(int pos, int size, NodeType nodeType, int relationLabelId, int arg1LabelId, int arg2LabelId){
		int[] arr = new int[]{size - pos - 1, nodeType.ordinal(), relationLabelId, arg1LabelId, arg2LabelId};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	/**
	 * Convert the output read from data to a model output
	 * @param output: output[position][number of relations at this position], each dimension have a number of relations at current 
	 * @param spans
	 * @return model output[position][relationType][arg1entityType][number of relations with the above 3 conditions]
	 */
	private Relation[][][][] convertToModelOutput(Relation[][] output, Span[] spans) {
		Relation[][][][] allRels = new Relation[output.length][RelationType.RELS.size()][Entity.ENTITIES.size()][0];
		for (int pos = 0; pos < output.length; pos ++) {
			for (Relation relation : output[pos]) {
				Span arg1Span = spans[relation.getArg1Idx()];
				int relId = relation.getRel().id;
				int arg1LabelId = arg1Span.entity.id;
				Relation[] curr = new Relation[allRels[pos][relId][arg1LabelId].length + 1];
				for (int i = 0; i < curr.length - 1; i++)
					curr[i] = allRels[pos][relId][arg1LabelId][i];
				curr[curr.length - 1] = relation;
				allRels[pos][relId][arg1Span.entity.id] = curr;
			}
		}
		return allRels;
	}
	
	private Span[][][] convertToModelOutputNoRelSpan(int size, Span[] spans, int[] noRelIdxs) {
		Span[][][] allSpans = new Span[size][Entity.ENTITIES.size()][0];
		for (int idx : noRelIdxs) {
			Span span = spans[idx];
			Span[] newOne = new Span[allSpans[span.start][span.entity.id].length + 1];
			for (int i = 0; i < newOne.length - 1; i++)
				newOne[i] = allSpans[span.start][span.entity.id][i];
			newOne[newOne.length - 1] = span;
			allSpans[span.start][span.entity.id] = newOne;
		}
		return allSpans;
	}
 	
	@Override
	public Instance decompile(Network net) {
		BaseNetwork network = (BaseNetwork)net;
		BaselineInstance result = (BaselineInstance)network.getInstance();
		//System.err.println(result.getInput().toString());
		int size = result.size();
		long[] nodes = network.getAllNodes();
		long aNode = network.getRoot();
		int aNode_k = Arrays.binarySearch(nodes, aNode);
		Set<Span> spans = new HashSet<>();
		List<RelationFullStruct> allrelations = new ArrayList<>();
		for (int pos = 0; pos < size; pos++) {
			int[] curChildren = network.getMaxPath(aNode_k);
			int eNode_k = curChildren[0];
			int[] curRNodes = network.getMaxPath(eNode_k);
			for (int rIdx = 0; rIdx < curRNodes.length; rIdx++) {
				int rNode_k = curRNodes[rIdx];
				int[] curArg1TNodes = network.getMaxPath(rNode_k);
				for (int arg1TIdx = 0; arg1TIdx < curArg1TNodes.length; arg1TIdx++) {
					int arg1TNode_k = curArg1TNodes[arg1TIdx];
					int[] tNodeArr = network.getNodeArray(arg1TNode_k);
					int arg1LabelId = tNodeArr[3];
					Entity arg1Ent = Entity.get(arg1LabelId);
					int[] child = network.getMaxPath(arg1TNode_k);
					int node_k = child[0];
					int[] nodeArr = network.getNodeArray(node_k);
					NodeType nodeType = NodeType.values()[nodeArr[1]];
					if (nodeType == NodeType.ARG1_NODE || nodeType == NodeType.ARG1_ARG2_NODE || nodeType == NodeType.O_NODE ||
							nodeType == NodeType.ARG2_NODE) {
						RelationType relLabel = RelationType.get(rIdx);
						//print("relation label: " + relLabel.form + ", " + nodeType.toString());
						List<Integer> candidates = new ArrayList<>();
						while (nodeType != NodeType.X_NODE) {
							candidates.add(node_k);
							if (child.length != 1) throw new RuntimeException("children length is not 1?");
							child = network.getMaxPath(node_k);
							node_k = child[0];
							nodeArr = network.getNodeArray (node_k);
							nodeType = NodeType.values()[nodeArr[1]];
							//print(nodeType.toString());
						}
						candidates.add(node_k);
						//post-process.
						int arg1Start = pos;
						int arg1End = pos;
						int prevArg2LabelId = -1;
						boolean startOfArg2 = false;
						boolean foundArg1 = false;
						int arg2Start = -1;
						int arg2End = -1;
						Entity arg2Ent = null;
						NodeType prevNodeType = null;
						Span arg1Span = null;
						List<Span> listOfArg2 = new ArrayList<>();
						for (int i = 0; i < candidates.size(); i++) {
							nodeArr = network.getNodeArray(candidates.get(i));
							nodeType = NodeType.values()[nodeArr[1]];
//							print("nodeType: " + nodeType.toString()+" curr idx: " + (size - nodeArr[0] - 1)+ "arg1 ent: "+Entity.get(nodeArr[3])+ "  arg2 ent: "+Entity.get(nodeArr[4]));
							if (!foundArg1 && (nodeType != NodeType.ARG1_NODE && nodeType != NodeType.ARG1_ARG2_NODE && nodeType != NodeType.ARG1_C_NODE)) {
								if (nodeType == NodeType.X_NODE) {
									arg1End = size -  network.getNodeArray(candidates.get(i-1))[0] - 1;
								}else {
									arg1End = size - nodeArr[0] - 1 - 1;
								}
//								print("arg 1 end: " + arg1End);
								arg1Span = new Span(arg1Start, arg1End, arg1Ent);
//								print("arg1Span: " + arg1Span);
								spans.add(arg1Span);
								foundArg1 = true;
							}
							if (!startOfArg2 && (nodeType == NodeType.ARG2_NODE || nodeType == NodeType.ARG1_ARG2_NODE)) {
								startOfArg2 = true;
								arg2Start = size - nodeArr[0] - 1;
								arg2Ent = Entity.get(nodeArr[4]);
							}
							if (startOfArg2) {
								if (prevNodeType == NodeType.ARG1_ARG2_NODE && (nodeType == NodeType.ARG1_C_NODE || nodeType == NodeType.O_NODE || nodeType == NodeType.X_NODE)) {
									startOfArg2 = false;
									if (nodeType == NodeType.X_NODE) {
										arg2End = size -  network.getNodeArray(candidates.get(i-1))[0] - 1;
									} else {
										arg2End =size - nodeArr[0] - 1 - 1;
									}
//									print("arg 2 end: " + arg2End);
									Span arg2Span = new Span(arg2Start, arg2End, arg2Ent);
//									print("arg 2 span: " + arg2Span);
									listOfArg2.add(arg2Span);
									spans.add(arg2Span);
								}
								//for adjacent arg2 with different type.
								if ((prevNodeType == NodeType.ARG1_ARG2_NODE || prevNodeType == NodeType.ARG2_NODE) && (nodeType == NodeType.ARG2_NODE || nodeType == NodeType.ARG1_ARG2_NODE)) {
									if (nodeArr[4] != prevArg2LabelId) {
										startOfArg2 = false;
										arg2End = size - nodeArr[0] - 1 - 1;
//										print("prev: "+ Entity.get(prevArg2LabelId) + ", now:"+Entity.get(nodeArr[4]));
//										print("arg 2 end: " + arg2End);
										Span arg2Span = new Span(arg2Start, arg2End, arg2Ent);
//										print("arg 2 span: " + arg2Span);
										listOfArg2.add(arg2Span);
										spans.add(arg2Span);
										startOfArg2 = true;
										arg2Start = size - nodeArr[0] - 1;
										arg2Ent = Entity.get(nodeArr[4]);
									}
								}
								if (prevNodeType == NodeType.ARG2_NODE && (nodeType == NodeType.O_NODE || nodeType == NodeType.X_NODE)) {
									startOfArg2 = false;
									if (nodeType == NodeType.X_NODE) {
										arg2End = size -  network.getNodeArray(candidates.get(i-1))[0] - 1;
									} else {
										arg2End =size - nodeArr[0] - 1 - 1;
									}
//									print("arg 2 end: " + arg2End);
									Span arg2Span = new Span(arg2Start, arg2End, arg2Ent);
//									print("arg 2 span: " + arg2Span);
									listOfArg2.add(arg2Span);
									spans.add(arg2Span);
								}
							}
							prevNodeType = nodeType;
							prevArg2LabelId = nodeArr[4];
						}
						
						for (Span arg2 : listOfArg2) {
							allrelations.add(new RelationFullStruct(arg1Span, arg2, relLabel));
						}
//						print("");
					} else if (nodeType == NodeType.I_NODE) {
						int start = pos;
						int end = start;
						List<Integer> endIdxs = new ArrayList<>();
//						print("start: "+start);
						while(nodeType == NodeType.I_NODE) {
							child = network.getMaxPath(node_k);
							if(child.length == 1){
								node_k = child[0];
							} else { //overlapping mentions with same entity type.
								endIdxs.add(end);
								node_k = child[1];
							}
							nodeArr = network.getNodeArray(node_k);
							nodeType = NodeType.values()[nodeArr[1]];
							end++;
						}
						endIdxs.add(end - 1);
//						print("all end indexes: " + endIdxs.toString());
						for (int endIdx : endIdxs) {
							Span span = new Span(start, endIdx, arg1Ent);
							spans.add(span);
						}
//						if (endIdxs.size() == 1) {
//							Span span = new Span(start, endIdxs.get(endIdxs.size() - 1), arg1Ent);
//							spans.add(span);
//						} else if (endIdxs.size() == 2) {
//							if (usedNoRelEndIdx[arg1Ent.id][endIdxs.get(1)] == 1) {
//								Span span = new Span(start, endIdxs.get(0), arg1Ent);
//								spans.add(span);
//								usedNoRelEndIdx[arg1Ent.id][endIdxs.get(0)] = 1;
//							} else {
//								Span span = new Span(start, endIdxs.get(1), arg1Ent);
//								spans.add(span);
//								usedNoRelEndIdx[arg1Ent.id][endIdxs.get(1)] = 1;
//							}
//						} else {
//							System.out.println(result.getInput().toString());
//							throw new RuntimeException("end idx size: " + endIdxs.size());
//						}
					}
				}
			}
			if(curChildren.length == 2){
				aNode_k = curChildren[1];
			}
		}
		List<Span> spanList = new ArrayList<>(spans);
		Collections.sort(spanList);
//		System.err.println(spanList.toString());
		Relation[][] relations = new Relation[size][];
		List<List<Relation>> relationList = new ArrayList<>();
		for (int i = 0; i< size; i++) relationList.add(new ArrayList<>());
		for (RelationFullStruct singleRel : allrelations) {
			//System.err.println(singleRel.toString());
			int arg1Idx = spanList.indexOf(singleRel.arg1Span);
			int arg2Idx = spanList.indexOf(singleRel.arg2Span);
			Relation rel = new Relation(arg1Idx, arg2Idx, singleRel.rel);
			//System.err.println(singleRel.arg1Span.start);
			relationList.get(singleRel.arg1Span.start).add(rel);
		}
		for (int i = 0; i < size; i++) {
			relations[i] = relationList.get(i).toArray(new Relation[relationList.get(i).size()]);
		}
		result.setPrediction(new RelPosition(spanList.toArray(new Span[spanList.size()]), relations));
		return result;
	}

}
