package org.statnlp.example.nerelation.baseline;

import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.Span;

public class RelPosition {

	Span[] spans;
	Relation[][] relations; //list of relations at each position
	int[] noRelSpanIdxs;
	
	public RelPosition(Span[] spans, Relation[][] relations, int[] noRelSpanIdxs) {
		this.spans = spans;
		this.relations = relations;
		this.noRelSpanIdxs = noRelSpanIdxs;
	}
	
	public RelPosition(Span[] spans, Relation[][] relations) {
		this(spans, relations, null);
	}

	public Span[] getSpans() {
		return spans;
	}

	public void setSpans(Span[] spans) {
		this.spans = spans;
	}

	public Relation[][] getRelations() {
		return relations;
	}

	public void setRelations(Relation[][] relations) {
		this.relations = relations;
	}

	public int[] getNoRelSpanIdxs() {
		return noRelSpanIdxs;
	}

	public void setNoRelSpanIdxs(int[] noRelSpanIdxs) {
		this.noRelSpanIdxs = noRelSpanIdxs;
	}
	
}
