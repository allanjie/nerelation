package org.statnlp.example.nerelation.baseline;

import static org.statnlp.commons.Utils.print;

import java.io.IOException;
import java.util.Set;

import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkModel;
import org.statnlp.hypergraph.NetworkConfig.ModelType;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class BaselineACE2005Main {

	public static double l2val = 0.01;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static int trainNum = 1;
	public static int testNum = -1;
	public static String trainFile = "data/ACE2004/train.data"; //including newswire and bnews data
	public static String testFile = "data/ACE2004/test.data"; //including newswire and bnews data
	public static Set<String> rules;
	public static boolean useRules = false;
	public static boolean useHeadEnd = false;
	public static boolean useHeadStart = false;
	public static boolean useRelRules = false;
	public static boolean maxMarginal = false;
	/**
	 * ignore the type-2 overlapping mentions in the data.
	 */
	public static boolean ignoreType2 = false; 
	/**
	 * ignore the instances that same relation, same arg1 ent type, different starting, but overlap instances
	 */
	public static boolean ignoreOverlappingRelation = false;
	public static boolean ignoreAdjacentSameTypeSecondArg;
	public static boolean ignoreMultipleArg1SameTypeSameRelation = false;
	public static int maxRelDist = 11;
	//by default we are using the CRF model type
	public static ModelType modelType = ModelType.CRF;
	
	public static void main(String[] args) throws IOException, InterruptedException {
		setArgs(args);
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.PARALLEL_FEATURE_EXTRACTION = true;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		NetworkConfig.MODEL_TYPE = modelType;
		print("[Info] Model Type: " + modelType);
		
		/****Debug information****/
		trainFile = "data/ACE2004/train.data";
		int start = 0;
		trainNum = 200;
		testFile = trainFile;
		numIteration = 5;
		//testNum = trainNum;
		ignoreType2 = true;
		ignoreAdjacentSameTypeSecondArg = true;
		ignoreOverlappingRelation = true;
		ignoreMultipleArg1SameTypeSameRelation = true;
		useHeadStart = true;
		useHeadEnd = true;
		/*******/
		
		BaselineReader reader = new BaselineReader(useHeadStart, useHeadEnd, ignoreType2, ignoreAdjacentSameTypeSecondArg, ignoreOverlappingRelation, ignoreMultipleArg1SameTypeSameRelation);
		BaselineInstance[] trainInsts = reader.read(trainFile, true, trainNum);
		/***Debug**/
		
		BaselineInstance[] now = new BaselineInstance[trainInsts.length - start];
		for (int i = 0; i < now.length; i++) {
			now[i] = trainInsts[start + i];
		}
		trainInsts = now;
		/****/
		
		System.out.println(trainInsts[trainInsts.length - 1].getInput().toString());
		Entity.lock();
		RelationType.lock();
		print("[Info] #Entity: " + Entity.ENTITIES_INDEX.size());
		print("[Info] Entity: " + Entity.ENTITIES_INDEX.toString());
		print("[Info] #RelationType: " + RelationType.RELS.size());
		print("[Info] RelationType: " + RelationType.RELS_INDEX.toString());
		GlobalNetworkParam gnp = new GlobalNetworkParam(OptimizerFactory.getLBFGSFactory());
		BaselineFeatureManager fm = new BaselineFeatureManager(gnp);
		int maxSize = -1;
		for (BaselineInstance inst : trainInsts) maxSize = Math.max(maxSize, inst.size());
		BaselineNetworkCompiler compiler = new BaselineNetworkCompiler(maxSize, rules);
		NetworkModel model = DiscriminativeNetworkModel.create(fm, compiler);
		model.train(trainInsts, numIteration);
		
		
		/**
		 * Decoding Phase.
		 */
		BaselineInstance[] testInsts = reader.read(testFile, true, testNum);
		/**Debug**/
		BaselineInstance[] nowTest = new BaselineInstance[testInsts.length - start];
		for (int i = 0; i < now.length; i++) {
			nowTest[i] = testInsts[start + i];
		}
		testInsts = nowTest;
		/*****/
		Instance[] results = model.decode(testInsts);
		BaselineEvaluator evaluator = new BaselineEvaluator(false);
		evaluator.evaluate(results);
	}

	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Tree-CRF Card-Pyramid Model for Relation Extraction");
		parser.addArgument("-t", "--thread").setDefault(numThreads).help("number of threads");
		parser.addArgument("--l2").setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--disc").action(Arguments.storeTrue()).setDefault(false).help("include disc or not");
		parser.addArgument("--headEnd").action(Arguments.storeTrue()).setDefault(false).help("use head as mention end.");
		parser.addArgument("--iter").setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--relRules").action(Arguments.storeTrue()).setDefault(false).help("use the relation rules or not");
		parser.addArgument("--maxMarginal").action(Arguments.storeTrue()).setDefault(true).help("using max marginal or not.");
		parser.addArgument("--modelType").setDefault(modelType).type(ModelType.class).help("the model type");
		Namespace ns = null;
	    try {
	        ns = parser.parseArgs(args);
	    } catch (ArgumentParserException e) {
	        parser.handleError(e);
	        System.exit(1);
	    }
	    numThreads = Integer.valueOf(ns.getString("thread"));
	    l2val = Double.valueOf(ns.getString("l2"));
	    useHeadEnd = ns.getBoolean("headEnd");
	    numIteration = Integer.valueOf(ns.getString("iter"));
	    maxMarginal = ns.getBoolean("maxMarginal");
	    modelType = (ModelType) ns.get("modelType");
	    System.err.println(ns.getAttrs().toString());
	}
}
