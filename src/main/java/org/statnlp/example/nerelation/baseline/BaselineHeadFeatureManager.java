package org.statnlp.example.nerelation.baseline;

import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.baseline.BaselineHeadNetworkCompiler.NodeType;
import org.statnlp.hypergraph.FeatureArray;
import org.statnlp.hypergraph.FeatureManager;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.Network;

public class BaselineHeadFeatureManager extends FeatureManager {
	
	private static final long serialVersionUID = 8227800758748672748L;
	
	private int wordHalfWindowSize = 1;
	private int posHalfWindowSize = 1;
	
	private int wordNGramMinSize = 2;
	private int wordNGramMaxSize = 4;
	private int posNGramMinSize = 2;
	private int posNGramMaxSize = 4;
	
	private int bowHalfWindowSize = 2;
	
	private static final boolean useCheatFeature = false;
	
	public enum FeatureType {
		CHEAT,
		START_WORD,
		END_WORD,
		START_POS,
		END_POS,
		WORD,
		WORD_NGRAM,
		POS_TAG,
		POS_TAG_NGRAM,
		BOW,
		ALL_CAPS,
		ALL_DIGITS,
		ALL_ALPHANUMERIC,
		ALL_LOWERCASE,
		CONTAINS_DIGITS,
		CONTAINS_DOTS,
		CONTAINS_HYPHEN,
		INITIAL_CAPS,
		LONELY_INITIAL,
		PUNCTUATION_MARK,
		ROMAN_NUMBER,
		SINGLE_CHARACTER,
		URL,
		MENTION_PENALTY,
		
		REL_WORD,
		REL_POS,
		REL_ENT,
		REL_WORD_NGRAM,
		REL_POS_NGRAM,
		REL_ENT_BOTH,
		RELATION_PENALTY,
		TRANSITION
		
	}
	
	public BaselineHeadFeatureManager(GlobalNetworkParam param_g) {
		super(param_g);
	}
	
	@Override
	protected FeatureArray extract_helper(Network net, int parent_k, int[] children_k, int children_k_index) {
		BaseNetwork network = (BaseNetwork)net;
		BaselineInstance inst = (BaselineInstance)network.getInstance();
		Sentence sent = inst.getInput();
		
		int size = inst.size();
		
		int[] parent_arr = network.getNodeArray(parent_k);
		int pos = size - parent_arr[0]-1;
		NodeType nodeType = NodeType.values()[parent_arr[1]];
		
		if (nodeType == NodeType.A_NODE) return FeatureArray.EMPTY;
		if (nodeType == NodeType.E_NODE) return FeatureArray.EMPTY;
		if (nodeType == NodeType.R_NODE) return FeatureArray.EMPTY;
		if (nodeType == NodeType.X_NODE) return FeatureArray.EMPTY;
		
		NodeType[] nodeTypes = NodeType.values();
		
		int[][] childrenArrs = new int[children_k.length][];
		for (int c = 0; c < childrenArrs.length; c++) {
			childrenArrs[c] = net.getNodeArray(children_k[c]);
		}
		
		List<Integer> fs = new ArrayList<Integer>();
		GlobalNetworkParam g = this._param_g;
		
		
		
		String relLabel = parent_arr[2] == RelationType.RELS.size() ? "NR" : RelationType.get(parent_arr[2]).form;
		String entityLabel = parent_arr[3] == Entity.ENTITIES.size() ? "UNKNOWN" : Entity.get(parent_arr[3]).form;
		String combLabel = relLabel + "-" + entityLabel;
		String w = sent.get(pos).getForm();
		String t = sent.get(pos).getTag();
		
		List<Integer> cheats = new ArrayList<>();
		String childListStr = "";
		for(int[] childNodeArr: childrenArrs){
			if(childListStr.length() > 0){
				childListStr += "#";
			}
			childListStr += NodeType.values()[childNodeArr[1]].name();
		}
		if (useCheatFeature) {
			cheats.add(g.toFeature(net, FeatureType.CHEAT.name(), relLabel, Math.abs(inst.getInstanceId())+" "+pos+" "+nodeType+" "+childListStr));
		}
		
		if (nodeType == NodeType.ARG1_T_NODE) {
			int[] childArr = childrenArrs[0];
			NodeType childNodeType = nodeTypes[childArr[1]];
			if (childNodeType == NodeType.X_NODE) {
//				for (int idx = pos - wordHalfWindowSize; idx <= pos + wordHalfWindowSize; idx++) {
//					String word = "";
//					if (idx >= 0 && idx < size) {
//						word = sent.get(idx).getForm();
//					}
//					fs.add(g.toFeature(network, FeatureType.WORD.name() + "[" +(idx - pos) + "]", combLabel+ "-" + "outside", word));
//				}
			} else {
				//Arg1 node or I node: start of relation/noRelation, 
				fs.add(g.toFeature(network, FeatureType.START_WORD.name(), combLabel+ "-" + nodeType, w));
				fs.add(g.toFeature(network, FeatureType.START_POS.name(), combLabel + "-" + nodeType, t));
			}
		}
		
		if (nodeType == NodeType.ARG1_NODE) {
			for (int idx = pos - wordHalfWindowSize; idx <= pos + wordHalfWindowSize; idx++) {
				String word = "";
				if (idx >= 0 && idx < size) {
					word = sent.get(idx).getForm();
				}
				fs.add(g.toFeature(network, FeatureType.WORD.name() + "[" +(idx - pos) + "]", combLabel+ "-" + nodeType, word));
			}
			if (childrenArrs.length == 1) {
				//should be O or arg2 or arg1
				int[] childArr = childrenArrs[0];
				NodeType childNodeType = nodeTypes[childArr[1]];
				if (childNodeType == NodeType.ARG2_NODE) {
					int arg2Pos = size - childArr[0]-1;
					String arg2w = sent.get(arg2Pos).getForm();
					String arg2t = sent.get(arg2Pos).getTag();
					String arg2EntityLabel = Entity.get(childArr[3]).form;
					fs.add(g.toFeature(network, FeatureType.TRANSITION.name(), relLabel, entityLabel + " " + arg2EntityLabel));
					fs.add(g.toFeature(network, FeatureType.START_WORD.name(), relLabel+ "-" + arg2EntityLabel + "-" + childNodeType, arg2w));
					fs.add(g.toFeature(network, FeatureType.START_POS.name(), relLabel+ "-" + arg2EntityLabel + "-" + childNodeType, arg2t));
				}else {
					//TODO: O or arg1. have nothing
				}
			} else {
				//must be arg2 and O.
				int[] childArr = childrenArrs[0];
				NodeType childNodeType = nodeTypes[childArr[1]];
				int arg2Pos = size - childArr[0]-1;
				String arg2w = sent.get(arg2Pos).getForm();
				String arg2t = sent.get(arg2Pos).getTag();
				String arg2EntityLabel = Entity.get(childArr[3]).form;
				fs.add(g.toFeature(network, FeatureType.TRANSITION.name(), relLabel, entityLabel + " " + arg2EntityLabel));
				fs.add(g.toFeature(network, FeatureType.START_WORD.name(), relLabel+ "-" + arg2EntityLabel + "-" + childNodeType, arg2w));
				fs.add(g.toFeature(network, FeatureType.START_POS.name(), relLabel+ "-" + arg2EntityLabel + "-" + childNodeType, arg2t));
			}
		}
		
		if (nodeType == NodeType.I_NODE) {
			for (int idx = pos - wordHalfWindowSize; idx <= pos + wordHalfWindowSize; idx++) {
				String word = "";
				if (idx >= 0 && idx < size) {
					word = sent.get(idx).getForm();
				}
				fs.add(g.toFeature(network, FeatureType.WORD.name() + "[" +(idx - pos) + "]", combLabel+ "-" + nodeType, word));
			}
			if (childrenArrs.length > 0) {
				int[] childArr = childrenArrs[0];
				NodeType childNodeType = nodeTypes[childArr[1]];
				if (childNodeType == NodeType.X_NODE) {
					fs.add(g.toFeature(network, FeatureType.END_WORD.name(), combLabel + "-" + nodeType, w));
					fs.add(g.toFeature(network, FeatureType.END_POS.name(), combLabel + "-" + nodeType, t));
				}
			}
		}
		
		if (nodeType == NodeType.O_NODE) {
			if (childrenArrs.length == 1) {
				//can be o or arg2
				int[] childArr = childrenArrs[0];
				NodeType childNodeType = nodeTypes[childArr[1]];
				if (childNodeType == NodeType.ARG2_NODE) {
					int arg2Pos = size - childArr[0]-1;
					String arg2w = sent.get(arg2Pos).getForm();
					String arg2t = sent.get(arg2Pos).getTag();
					String arg2EntityLabel = Entity.get(childArr[3]).form;
					fs.add(g.toFeature(network, FeatureType.TRANSITION.name(), relLabel, entityLabel + " " + arg2EntityLabel));
					fs.add(g.toFeature(network, FeatureType.START_WORD.name(), relLabel+ "-" + arg2EntityLabel + "-" + childNodeType, arg2w));
					fs.add(g.toFeature(network, FeatureType.START_POS.name(), relLabel+ "-" + arg2EntityLabel + "-" + childNodeType, arg2t));
				}else {
					//TODO: O or have nothing
				}
			} else {
				//must be arg2 and O
				int[] childArr = childrenArrs[0];
				NodeType childNodeType = nodeTypes[childArr[1]];
				int arg2Pos = size - childArr[0]-1;
				String arg2w = sent.get(arg2Pos).getForm();
				String arg2t = sent.get(arg2Pos).getTag();
				String arg2EntityLabel = Entity.get(childArr[3]).form;
				fs.add(g.toFeature(network, FeatureType.TRANSITION.name(), relLabel, entityLabel + " " + arg2EntityLabel));
				fs.add(g.toFeature(network, FeatureType.START_WORD.name(), relLabel+ "-" + arg2EntityLabel + "-" + childNodeType, arg2w));
				fs.add(g.toFeature(network, FeatureType.START_POS.name(), relLabel+ "-" + arg2EntityLabel + "-" + childNodeType, arg2t));
			}
		}
		
		if (nodeType == NodeType.ARG2_NODE) {
			for (int idx = pos - wordHalfWindowSize; idx <= pos + wordHalfWindowSize; idx++) {
				String word = "";
				if (idx >= 0 && idx < size) {
					word = sent.get(idx).getForm();
				}
				fs.add(g.toFeature(network, FeatureType.WORD.name() + "[" +(idx - pos) + "]", combLabel+ "-" + nodeType, word));
			}
			if (childrenArrs.length > 0) {
				int[] childArr = childrenArrs[0];
				NodeType childNodeType = nodeTypes[childArr[1]];
				if (childNodeType == NodeType.X_NODE) {
					fs.add(g.toFeature(network, FeatureType.END_WORD.name(), combLabel + "-" + nodeType, w));
					fs.add(g.toFeature(network, FeatureType.END_POS.name(), combLabel + "-" + nodeType, t));
				}
			}
		}
		
		
		
		return this.createFeatureArray(network, fs);
	}

}
