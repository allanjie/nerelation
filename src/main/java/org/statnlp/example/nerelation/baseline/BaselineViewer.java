package org.statnlp.example.nerelation.baseline;

import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Map;

import org.statnlp.commons.types.Label;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.nerelation.baseline.BaselineHeadNetworkCompiler.NodeType;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.ui.visualize.type.VNode;
import org.statnlp.ui.visualize.type.VisualizationViewerEngine;
import org.statnlp.ui.visualize.type.VisualizeGraph;

public class BaselineViewer extends VisualizationViewerEngine {

	public BaselineViewer(GlobalNetworkParam param) {
		super(param);
	}
	
	static double span_width = 100;

	static double span_height = 100;
	
	static double offset_width = 100;
	
	static double offset_height = 100;
	
	/**
	 * The list of instances to be visualized
	 */
	protected BaselineInstance instance;
	
	protected Sentence inputs;
	
	protected RelPosition outputs;
	
	/**
	 * The list of labels to be used in the visualization.<br>
	 * This member is used to support visualization on those codes that do not utilize
	 * the generic pipeline. In the pipeline, use {@link #instanceParser} instead.
	 */
	protected Map<Integer, Label> labels;
	
	public BaselineViewer(Map<Integer, Label> labels){
		super(null);
		this.labels = labels;
	}
	
	
	protected void initData() {
		this.instance = (BaselineInstance)super.instance;
		this.inputs = (Sentence)super.inputs;
		this.outputs = (RelPosition)super.outputs;
		//WIDTH = instance.Length * span_width;
	}
	
	@Override
	protected String label_mapping(VNode node) {
		int[] ids = node.ids;
		int size = instance.size();
		int pos = size - ids[0] - 1;
		int nodeType = ids[1];
		int relId = ids[2];
		int argLabelId = ids[3];
		String label = null;
		if (nodeType == NodeType.A_NODE.ordinal()) {
			label = "A,"+pos;
		}else if (nodeType == NodeType.E_NODE.ordinal()) {
			label = "E,"+pos;
		} else if (nodeType == NodeType.R_NODE.ordinal()) {
			label = "R,"+pos+","+relId;
		} else if (nodeType == NodeType.ARG1_T_NODE.ordinal()) {
			label = "T,"+pos+","+relId+","+argLabelId;
		} else if (nodeType == NodeType.ARG1_NODE.ordinal()) {
			label = "ARG1,"+pos+","+relId+","+argLabelId;
		}  else {
			label =  Arrays.toString(ids);
		}
		
		return  label;
	}
	
	protected void initNodeColor(VisualizeGraph vg)
	{
		if (colorMap != null){
			for(VNode node : vg.getNodes())
			{
				int[] ids = node.ids;
				int nodeType = ids[1];
				if (nodeType == NodeType.A_NODE.ordinal()) {
					node.color = colorMap[0];
				}else if (nodeType == NodeType.E_NODE.ordinal()){
					node.color = colorMap[1];
				}else if (nodeType == NodeType.R_NODE.ordinal()){
					node.color = colorMap[2];
				}else if (nodeType == NodeType.ARG1_T_NODE.ordinal()){
					node.color = colorMap[3];
				}else if (nodeType == NodeType.ARG1_NODE.ordinal()) {
					node.color = colorMap[4];
				}else if (nodeType == NodeType.I_NODE.ordinal()) {
					node.color = colorMap[8];
				}else if (nodeType == NodeType.O_NODE.ordinal()){
					node.color = colorMap[5];
				}else if (nodeType == NodeType.ARG2_NODE.ordinal()){
					node.color = colorMap[6];
				}else if(nodeType == NodeType.X_NODE.ordinal()){
					node.color = colorMap[7];
				} else
				{ //root
					throw new RuntimeException("nothing");
				}
			}
		}
		
	}
	
	protected void initNodeCoordinate(VisualizeGraph vg)
	{
		for(VNode node : vg.getNodes())
		{
			int[] ids = node.ids;
			int size = this.inputs.length();
			int pos = size - ids[0] - 1;
			int relLabelId = ids[2];
			int argLabelId = ids[3];
			//int argLabelId = ids[3];
			int nodeType = ids[1];
			double x = pos * span_width;
			int mappedId = relLabelId;
			double y = mappedId * span_height + offset_height;
			if (nodeType == NodeType.X_NODE.ordinal()){
				x = span_width;
				y = 3 * span_height + offset_height;
			} else if (nodeType == NodeType.A_NODE.ordinal()){
				y-=300;
			} else if (nodeType == NodeType.E_NODE.ordinal()){
				y-=200;
			} else if (nodeType == NodeType.R_NODE.ordinal()){
				y-=100;
				x += relLabelId * 13;
			} else if (nodeType == NodeType.ARG1_NODE.ordinal()){
				y+= 100+ argLabelId*20;
				x += relLabelId * 13;
			} else if (nodeType == NodeType.ARG1_T_NODE.ordinal()){
				y+=100 + argLabelId*15;
				x += relLabelId * 13;
			} else if (nodeType == NodeType.I_NODE.ordinal()){
				y+= 100 + argLabelId*15;
				x += relLabelId * 13 + (argLabelId+1)*15;
			}
			node.point = new Point2D.Double(x,y);
			layout.setLocation(node, node.point);
			layout.lock(node, true);
		}
	}
	


}
