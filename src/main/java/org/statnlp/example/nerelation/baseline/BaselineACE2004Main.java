package org.statnlp.example.nerelation.baseline;

import static org.statnlp.commons.Utils.print;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkConfig.ModelType;
import org.statnlp.hypergraph.NetworkModel;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class BaselineACE2004Main {

	public static double l2val = 0.01;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static int trainNum = 1;
	public static int testNum = -1;
	public static String trainFile = "data/ace2004/train1.data"; //including newswire and bnews data
	public static String testFile = "data/ace2004/test1.data"; //including newswire and bnews data
	public static RelRule rules;
	public static final boolean useHeadEnd = true;
	public static final boolean useHeadStart = true;
	public static boolean useRelRules = false;
	
	public static boolean ignoreCrossOverlappingRelation = true;
	public static boolean ignoreAdjacentSameTypeSecondArg = false;
	public static boolean ignoreMultipleArg1SameTypeSameRelation = true;
	public static String modelPath = "models/ace.m";
	public static boolean readModel = false;
	public static boolean saveModel = true;
	
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		setArgs(args);
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.PARALLEL_FEATURE_EXTRACTION = true;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		NetworkConfig.MODEL_TYPE = ModelType.CRF;
		print("[Info] Model Type: " + NetworkConfig.MODEL_TYPE);
		
		/****Debug information****/
		trainFile = "data/ace2004/debug.txt";
//		int start = 0;
		trainNum = 70;
		testFile = trainFile;
//		numIteration = 1000;
		testNum = 40;
		saveModel = true;
		readModel = false;
//		useRelRules = true;
		/*******/
		
		BaselineReader reader = new BaselineReader(useHeadStart, useHeadEnd, ignoreAdjacentSameTypeSecondArg, 
				ignoreCrossOverlappingRelation, ignoreMultipleArg1SameTypeSameRelation, useRelRules);
		BaselineInstance[] trainInsts = reader.read(trainFile, true, trainNum);
		if (useRelRules) {
			rules = reader.getRules();
			System.out.println(rules);
		}
		/***Debug**/
		System.out.println(trainInsts[trainInsts.length -1].getInput().toString());
//		BaselineInstance[] now = new BaselineInstance[trainInsts.length - start];
//		for (int i = 0; i < now.length; i++) {
//			now[i] = trainInsts[start + i];
//		}
//		trainInsts = now;
		/****/
		
//		Entity.lock();
//		RelationType.lock();
		print("[Info] #Entity: " + Entity.ENTITIES_INDEX.size());
		print("[Info] Entity: " + Entity.ENTITIES_INDEX.toString());
		print("[Info] #RelationType: " + RelationType.RELS.size());
		print("[Info] RelationType: " + RelationType.RELS_INDEX.toString());
		int maxSize = 150;
		for (BaselineInstance inst : trainInsts) maxSize = Math.max(maxSize, inst.size());
		maxSize  = 150;
		NetworkModel model = null;
		GlobalNetworkParam gnp = null;
		if (readModel) {
			System.out.println("[Info] Reading Model from: " + modelPath);
			ObjectInputStream in = RAWF.objectReader(modelPath);
			model = (NetworkModel)in.readObject();
		} else {
			gnp = new GlobalNetworkParam(OptimizerFactory.getLBFGSFactory());
			BaselineHeadFeatureManager fm = new BaselineHeadFeatureManager(gnp);
			BaselineHeadNetworkCompiler compiler = new BaselineHeadNetworkCompiler(maxSize, rules);
			model = DiscriminativeNetworkModel.create(fm, compiler);
			model.train(trainInsts, numIteration);
		}
		if (saveModel) {
			System.out.println("[Info] Saving Model at: " + modelPath);
			ObjectOutputStream out = RAWF.objectWriter(modelPath);
			out.writeObject(model);
			out.close();
		}
		BaselineViewer viewer = new BaselineViewer(gnp);
		
		/**
		 * Decoding Phase.
		 */
		BaselineInstance[] testInsts = reader.read(testFile, false, testNum);
		/**Debug**/
//		BaselineInstance[] nowTest = new BaselineInstance[testInsts.length - start];
//		for (int i = 0; i < now.length; i++) {
//			nowTest[i] = testInsts[start + i];
//		}
//		testInsts = nowTest;
		/*****/
		Instance[] results = model.decode(testInsts);
		
		model.visualize(viewer, results);
		BaselineEvaluator evaluator = new BaselineEvaluator(true);
		evaluator.evaluate(results);
	}

	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Tree-CRF Card-Pyramid Model for Relation Extraction");
		parser.addArgument("-t", "--thread").setDefault(numThreads).help("number of threads");
		parser.addArgument("--l2").setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--disc").action(Arguments.storeTrue()).setDefault(false).help("include disc or not");
		parser.addArgument("--iter").setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--relRules").action(Arguments.storeTrue()).setDefault(false).help("use the relation rules or not");
		parser.addArgument("--maxMarginal").action(Arguments.storeTrue()).setDefault(true).help("using max marginal or not.");
		parser.addArgument("--trainNum").setDefault(trainNum).help("The number of training instances.");
		parser.addArgument("--testNum").setDefault(testNum).help("The number of testing instances.");
		parser.addArgument("--trainFile").setDefault(trainFile).help("The file for training");
		parser.addArgument("--testFile").setDefault(testFile).help("The file for testing");
		parser.addArgument("--saveModel").setDefault(saveModel).help("saveModel or not");
		parser.addArgument("--readModel").setDefault(readModel).help("read the model or not");
		Namespace ns = null;
	    try {
	        ns = parser.parseArgs(args);
	    } catch (ArgumentParserException e) {
	        parser.handleError(e);
	        System.exit(1);
	    }
	    numThreads = Integer.valueOf(ns.getString("thread"));
	    l2val = Double.valueOf(ns.getString("l2"));
	    numIteration = Integer.valueOf(ns.getString("iter"));
	    trainNum = Integer.valueOf(ns.getString("trainNum"));
	    testNum = Integer.valueOf(ns.getString("testNum"));
	    trainFile = ns.getString("trainFile");
	    testFile = ns.getString("testFile");
	    saveModel = Boolean.valueOf(ns.getString("saveModel"));
	    readModel = Boolean.valueOf(ns.getString("readModel"));
	    useRelRules = ns.getBoolean("relRules");
	    System.err.println(ns.getAttrs().toString());
	}
}
