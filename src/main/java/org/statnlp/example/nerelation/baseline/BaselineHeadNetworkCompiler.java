package org.statnlp.example.nerelation.baseline;

import static org.statnlp.commons.Utils.print;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

/**
 * We assume arg1 always on the left already. 
 * Please ensure this in data preprocessing.
 * @author allanjie
 *
 */
public class BaselineHeadNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -8427069718884521214L;
	public static final boolean DEBUG = false;
	
	public int maxSize = 1;
	public BaseNetwork unlabeledNetwork;
	protected RelRule rules;
	
	public enum NodeType{
		X_NODE,
		ARG2_NODE,
		O_NODE,
		ARG1_NODE,
		I_NODE,
		ARG1_T_NODE,
		R_NODE,
		E_NODE,
		A_NODE,
	}
	
	static {
		NetworkIDMapper.setCapacity(new int[]{150, 10, 15, 10});
	}
	
	public BaselineHeadNetworkCompiler(int maxSize) {
		this(maxSize, null);
	}
	
	public BaselineHeadNetworkCompiler(int maxSize, RelRule rules) {
		this.maxSize = maxSize;
		this.rules = rules;
		this.buildUnlabeled();
	}

	@Override
	public BaseNetwork compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder();
		BaselineInstance bInst = (BaselineInstance)inst;
		//print(bInst.getInput().toString());
		int size = bInst.size();
		long xNode = toNode_X(size);
		builder.addNode(xNode);
		RelPosition output = bInst.getOutput();
		Span[] spans = output.getSpans();
		int[] noRelIdxs = output.getNoRelSpanIdxs();
		Relation[][][][] relPos = this.convertToModelOutput(output.getRelations(), spans);
		Span[][][] spanPos = this.convertToModelOutputNoRelSpan(size, spans, noRelIdxs);
		long prevANode = -1;
		long prevENode = -1;
		Set<Span> arg2SpanAdded = new HashSet<>();
		for (int pos = 0; pos < size; pos++) {
			Relation[][][] rels = relPos[pos];
			long aNode = this.toNode_A(pos, size);
			long eNode = this.toNode_E(pos, size);
			builder.addNode(aNode);
			builder.addNode(eNode);
			if (prevANode != -1) {
				builder.addEdge(prevANode, new long[]{prevENode, aNode});
			}
			if (pos == size - 1) {
				builder.addEdge(aNode, new long[]{eNode});
			}
			prevANode = aNode;
			prevENode = eNode;
			long[] allRNodes = new long[RelationType.RELS.size() + 1]; // the last one represents no relation
			long[][] allTNodes = new long[RelationType.RELS.size() + 1][Entity.ENTITIES.size()];
			for (int relId = 0; relId < allRNodes.length; relId++) {
				allRNodes[relId] = this.toNode_R(pos, size, relId);
				builder.addNode(allRNodes[relId]);
				for (int arg1LabelId = 0; arg1LabelId < Entity.ENTITIES.size(); arg1LabelId++) {
					allTNodes[relId][arg1LabelId] = this.toNode_ARG1_T(pos, size, relId, arg1LabelId);
					builder.addNode(allTNodes[relId][arg1LabelId]);
				}
				if (relId != RelationType.RELS.size()) {
					for (int arg1LabelId = 0; arg1LabelId < Entity.ENTITIES.size(); arg1LabelId++) {
						long prevNode = allTNodes[relId][arg1LabelId];
						if (rels[relId][arg1LabelId].length > 0) {
							Span arg1Span = spans[rels[relId][arg1LabelId][0].getArg1Idx()];
//							print("arg1span: "+ arg1Span.toString());
							for(int idx = arg1Span.start; idx <= arg1Span.end; idx++) {
								long currNode = this.toNode_ARG1(idx, size, relId, arg1LabelId);
								builder.addNode(currNode);
								builder.addEdge(prevNode, new long[]{currNode});
								prevNode = currNode;
							}
							long lastArg1Node = prevNode;
							for (Relation relation : rels[relId][arg1LabelId]) {
								prevNode = lastArg1Node;
								Span arg2Span = spans[relation.getArg2Idx()];
								int arg2LabelId = arg2Span.entity.id;
								for (int idx = arg1Span.end + 1; idx <= arg2Span.start - 1; idx++) {
									long currNode = this.toNode_O(idx, size, relId, arg1LabelId);
									builder.addNode(currNode);
									List<long[]> childrens = builder.getChildren_tmp(prevNode);
									if (childrens!= null && childrens.size() > 0) {
										if (childrens.size() != 1) throw new RuntimeException("hyperedge num not 1?");
										if (childrens.get(0).length == 2) {
											prevNode = currNode;
										} else {
											//children length is 1;
											long tmpNode = childrens.get(0)[0];
											int[] tmpArr = NetworkIDMapper.toHybridNodeArray(tmpNode);
											NodeType tmpType = NodeType.values()[tmpArr[1]];
											if (tmpType == NodeType.O_NODE) {
												//do nothing.
												prevNode = currNode;
											} else if (tmpType == NodeType.ARG2_NODE) {
												childrens.clear();
												builder.addEdge(prevNode, new long[]{tmpNode, currNode});
												prevNode = currNode;
											} else {
												throw new RuntimeException("unknown node type: " + tmpType);
											}
										}
									} else {
										builder.addNode(currNode);
										builder.addEdge(prevNode, new long[]{currNode});
										prevNode = currNode;
									}
								}
//								print("arg2Span: "+ arg2Span.toString());
								for (int idx = arg2Span.start; idx <= arg2Span.end; idx++) {
									long currNode = this.toNode_ARG2(idx, size, relId, arg2LabelId);
									List<long[]> childrens = builder.getChildren_tmp(prevNode);
									if (childrens!= null && childrens.size() > 0) {
										if (childrens.size() != 1) throw new RuntimeException("hyperedge num not 1?");
										if (childrens.get(0).length == 2) {
											prevNode = currNode;
										} else {
											//children length is 1;
											long tmpNode = childrens.get(0)[0];
											int[] tmpArr = NetworkIDMapper.toHybridNodeArray(tmpNode);
											NodeType tmpType = NodeType.values()[tmpArr[1]];
											if (tmpType == NodeType.O_NODE) {
												childrens.clear();
												builder.addNode(currNode);
												builder.addEdge(prevNode, new long[]{currNode, tmpNode});
												prevNode = currNode;
											} else if (tmpType == NodeType.ARG2_NODE) {
												//do nothing.
												prevNode = currNode;
												if (!arg2SpanAdded.contains(arg2Span)) {
													print(inst.getInput().toString());
													throw new RuntimeException("overlapping arg2?");	
												}
											} else {
												throw new RuntimeException("unknown node type: " + tmpType);
											}
										}
									} else {
										builder.addNode(currNode);
										builder.addEdge(prevNode, new long[]{currNode});
										prevNode = currNode;
									}
								}
								arg2SpanAdded.add(arg2Span);
								builder.addEdge(prevNode, new long[]{xNode});
							}
						} else {
							builder.addEdge(prevNode, new long[]{xNode});
						}
					}
				}
				builder.addEdge(allRNodes[relId], allTNodes[relId]);
			}
			builder.addEdge(eNode, allRNodes);
		}
		
		for (int pos = 0; pos < size; pos++) {
//			print("curr position: " + pos, System.err);
			for (int arg1LabelId = 0; arg1LabelId < Entity.ENTITIES.size(); arg1LabelId++) {
//				print("curr label: " + arg1LabelId, System.err);
				long tNode = this.toNode_ARG1_T(pos, size, RelationType.RELS.size(), arg1LabelId);
				if (spanPos[pos][arg1LabelId].length == 0) {
					builder.addEdge(tNode, new long[]{xNode});
					continue;
				}
				int rightMostIdx = this.getSpanRightMostEnd(spanPos[pos][arg1LabelId]);
				long[] chain = new long[rightMostIdx - pos + 1];
				boolean[] isEnd = new boolean[rightMostIdx - pos + 1];
				for (Span span : spanPos[pos][arg1LabelId]) {
					isEnd[span.end - pos] = true;
				}
//				print("span length: " + spanPos[pos][arg1LabelId].length, System.err);
				if (!isEnd[rightMostIdx - pos]) throw new RuntimeException("last one is not end?");
				long prevNode = tNode;
				for (int i = 0; i < chain.length; i++) {
					//print("curr i: " + i, System.err);
					//print("pos: " + (pos + i));
					chain[i] = this.toNode_I(pos + i, size, arg1LabelId);
					builder.addNode(chain[i]);
					List<long[]> prevNodeChildren = builder.getChildren_tmp(prevNode);
					if (prevNodeChildren != null) {
						throw new RuntimeException("overlap with entities?");
					} else {
						builder.addEdge(prevNode, new long[]{chain[i]});
					}
					if (isEnd[i]) {
//						print("adding end at pos: " + (pos + i));
						List<long[]> currNodeChildren = builder.getChildren_tmp(chain[i]);
						if (currNodeChildren != null && currNodeChildren.size() > 0) {
							throw new RuntimeException("overlap with entities?");
						} else {
							builder.addEdge(chain[i], new long[]{xNode});
						}
					}
					prevNode = chain[i];
				}
			}
		}
		BaseNetwork network =  builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = compileUnlabeled(networkId, inst, param);
			boolean contained = unlabeled.contains(network);
			if (!contained) {
				print("Contained: "+contained);
				throw new RuntimeException("Labeled network is not contained.");
			}
		}
		return network;
	}
	
	/**
	 * Debugging code to see whether the network ends with X
	 * @param network
	 * @param node_k
	 */
	@SuppressWarnings("unused")
	private void findXNode(BaseNetwork network, int node_k) {
		if (network.getChildren(node_k).length == 0 && 
				NodeType.values()[network.getNodeArray(node_k)[1]] != NodeType.X_NODE) {
			throw new RuntimeException("can't find X node");
		}
		if (network.getChildren(node_k) == null && 
				NodeType.values()[network.getNodeArray(node_k)[1]] != NodeType.X_NODE) {
			throw new RuntimeException("can't find X node");
		}
		if (network.getChildren(node_k).length != 1) 
			throw new RuntimeException("ambiguous?");
		for (int[] children : network.getChildren(node_k)) {
			for (int child : children) {
				this.findXNode(network, child);
			}
		}
		
	}
	
	private int getSpanRightMostEnd(Span[] spans) {
		int rightMostIdx = -1;
		for(Span span : spans) {
			rightMostIdx = Math.max(rightMostIdx, span.end);
			rightMostIdx = Math.max(rightMostIdx, span.end);
		}
		return rightMostIdx;
	}
	
	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
//		return this.compileLabeled(networkId, inst, param);
		int size = inst.size();
		long root = toNode_A(0, size);
		long[] allNodes = unlabeledNetwork.getAllNodes();
		int[][][] allChildren = unlabeledNetwork.getAllChildren();
		int root_k = unlabeledNetwork.getNodeIndex(root);
		int numNodes = root_k + 1;
		BaseNetwork network = NetworkBuilder.quickBuild(networkId, inst, allNodes, allChildren, numNodes, param, this);
		return network;
	}
	
	public void buildUnlabeled() {
		print("Building generic unlabeled tree up to size "+maxSize+"...");
		long startTime = System.currentTimeMillis();
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder();
		int size = maxSize;
		long xNode = this.toNode_X(size);
		builder.addNode(xNode);
		for (int pos = size - 1; pos >= 0; pos--) {
			long[] rNodes = new long[RelationType.RELS.size() + 1];
			long[][] tNodes = new long[rNodes.length][Entity.ENTITIES.size()];
			for (int rId = 0; rId < rNodes.length; rId++) {
				rNodes[rId] = this.toNode_R(pos, size, rId);
				builder.addNode(rNodes[rId]);
				if (rId != RelationType.RELS.size()) {
					for(int arg1LabelId = 0; arg1LabelId < tNodes[rId].length; arg1LabelId++) {
						tNodes[rId][arg1LabelId] = this.toNode_ARG1_T(pos, size, rId, arg1LabelId);
						builder.addNode(tNodes[rId][arg1LabelId]);
						builder.addEdge(tNodes[rId][arg1LabelId], new long[]{xNode});
						if (rules != null) {
							if (!rules.containsLHS(RelationType.get(rId).form + "," + Entity.get(arg1LabelId).form))
								continue;
						}
						//build arg1Node.
						long arg1Node = this.toNode_ARG1(pos, size, rId, arg1LabelId);
						if (pos < size - 1) {
							builder.addNode(arg1Node);
							builder.addEdge(tNodes[rId][arg1LabelId], new long[]{arg1Node});
							if (pos + 1 < size - 1) {
								long nextArg1Node = this.toNode_ARG1(pos + 1, size, rId, arg1LabelId);
								builder.addEdge(arg1Node, new long[]{nextArg1Node});
								long oNode = this.toNode_O(pos + 1, size, rId, arg1LabelId);
								builder.addEdge(arg1Node, new long[]{oNode});
							}
							long nextONode = this.toNode_O(pos + 1, size, rId, arg1LabelId);
							for (int arg2LabelId = 0; arg2LabelId < Entity.ENTITIES.size(); arg2LabelId++) {
								if (rules != null) {
									if (!rules.containsTriple(RelationType.get(rId).form + " " + Entity.get(arg1LabelId).form + " " + Entity.get(arg2LabelId).form))
										continue;
								}
								long nextArg2Node = this.toNode_ARG2(pos + 1, size, rId, arg2LabelId);
								builder.addEdge(arg1Node, new long[]{nextArg2Node});
								if (pos + 1 < size - 1) {
									builder.addEdge(arg1Node, new long[]{nextArg2Node, nextONode});
								}
							}
						}
						long oNode = this.toNode_O(pos, size, rId, arg1LabelId);
						if (pos > 0) {
							if (pos < size - 1) {
								
								if (pos + 1 < size - 1) {
									long nextONode = this.toNode_O(pos + 1, size, rId, arg1LabelId);
									builder.addNode(oNode);
									builder.addEdge(oNode, new long[]{nextONode});
								}
								for (int arg2LabelId = 0; arg2LabelId < Entity.ENTITIES.size(); arg2LabelId++) {
									if (rules != null) {
										if (!rules.containsTriple(RelationType.get(rId).form + "," + Entity.get(arg1LabelId).form + "," + Entity.get(arg2LabelId).form))
											continue;
									}
									long nextArg2Node = this.toNode_ARG2(pos + 1, size, rId, arg2LabelId);
									builder.addNode(oNode);
									builder.addEdge(oNode, new long[]{nextArg2Node});
									if (pos + 1 < size - 1) {
										long nextONode = this.toNode_O(pos + 1, size, rId, arg1LabelId);
										builder.addEdge(oNode, new long[]{nextArg2Node, nextONode});
									}
								}
							}
						}
						for (int arg2LabelId = 0; arg2LabelId < Entity.ENTITIES.size(); arg2LabelId++) {
							if (rules != null) {
								if (!rules.containsTriple(RelationType.get(rId).form + "," + Entity.get(arg1LabelId).form + "," + Entity.get(arg2LabelId).form))
									continue;
							}
							if (pos > 0) {
								long arg2Node = this.toNode_ARG2(pos, size, rId, arg2LabelId);
								builder.addNode(arg2Node);
								builder.addEdge(arg2Node, new long[]{xNode});
								if (pos < size - 1) {
									//TODO: we don't consider the adjacent second arguments to be adjacent.
									long nextArg2Node = this.toNode_ARG2(pos + 1, size, rId, arg2LabelId);
									builder.addEdge(arg2Node, new long[]{nextArg2Node});
								}
							}
						}
					}
				} else {
					for(int tId = 0; tId < tNodes[rId].length; tId++) {
						Entity entLabel = Entity.get(tId);
						long iNode = toNode_I(pos, size, entLabel.id);
						builder.addNode(iNode);
						builder.addEdge(iNode, new long[]{xNode});
						if (pos < size - 1) {
							long nextINode = toNode_I(pos + 1, size, entLabel.id);
							builder.addEdge(iNode, new long[]{nextINode});
							//since we found max number of same entity starting from same position is 1.
						}
						tNodes[rId][tId] = this.toNode_ARG1_T(pos, size, rId, tId);
						builder.addNode(tNodes[rId][tId]);
						builder.addEdge(tNodes[rId][tId], new long[]{xNode});
						builder.addEdge(tNodes[rId][tId], new long[]{iNode});
					}
				}
				builder.addEdge(rNodes[rId], tNodes[rId]);
			}
			
			long eNode = this.toNode_E(pos, size);
			builder.addNode(eNode);
			builder.addEdge(eNode, rNodes);
			long aNode = this.toNode_A(pos, size);
			builder.addNode(aNode);
			if (pos < size - 1) {
				long nextANode = this.toNode_A(pos + 1, size);
				builder.addEdge(aNode, new long[]{eNode, nextANode});
			} else {
				builder.addEdge(aNode, new long[]{eNode});
			}
		}
		
		BaseNetwork network = builder.buildRudimentaryNetwork();
		this.unlabeledNetwork = network;
		//this.findXNode(network, this.unlabeledNetwork.countNodes() - 1);
		print("#nodes in unlabeled: " + this.unlabeledNetwork.getAllNodes().length);
		long endTime = System.currentTimeMillis();
		System.err.println(String.format("Done in %.3fs", (endTime-startTime)/1000.0));
	}

	private long toNode_X(int size) {
		return this.toNode(size - 1, size, NodeType.X_NODE, 0, 0);
	}
	
	private long toNode_ARG2(int pos, int size, int relId, int arg2LabelId) {
		return this.toNode(pos, size, NodeType.ARG2_NODE, relId, arg2LabelId);
	}
	
	private long toNode_O(int pos, int size, int relId, int arg1LabelId) {
		return this.toNode(pos, size, NodeType.O_NODE, relId, arg1LabelId);
	}
	
	private long toNode_ARG1(int pos, int size, int relId, int arg1LabelId) {
		return this.toNode(pos, size, NodeType.ARG1_NODE, relId, arg1LabelId);
	}
	
	private long toNode_I(int pos, int size, int entId) {
		return this.toNode(pos, size, NodeType.I_NODE, RelationType.RELS.size(), entId);
	}

	private long toNode_ARG1_T(int pos, int size, int relId, int arg1LabelId) {
		return this.toNode(pos, size, NodeType.ARG1_T_NODE, relId, arg1LabelId);
	}
	
	private long toNode_R(int pos, int size, int relId) {
		return this.toNode(pos, size, NodeType.R_NODE, relId, 0);
	}
	
	private long toNode_E(int pos, int size) {
		return this.toNode(pos, size, NodeType.E_NODE, 0, 0);
	}
	
	private long toNode_A(int pos, int size) {
		return this.toNode(pos, size, NodeType.A_NODE, 0, 0);
	}

	private long toNode(int pos, int size, NodeType nodeType, int relationLabelId, int argLabelId){
		int[] arr = new int[]{size - pos - 1, nodeType.ordinal(), relationLabelId, argLabelId};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	/**
	 * Convert the output read from data to a model output
	 * @param output: output[position][number of relations at this position], each dimension have a number of relations at current 
	 * @param spans
	 * @return model output[position][relationType][arg1entityType][number of relations with the above 3 conditions]
	 */
	private Relation[][][][] convertToModelOutput(Relation[][] output, Span[] spans) {
		Relation[][][][] allRels = new Relation[output.length][RelationType.RELS.size()][Entity.ENTITIES.size()][0];
		for (int pos = 0; pos < output.length; pos ++) {
			for (Relation relation : output[pos]) {
				Span arg1Span = spans[relation.getArg1Idx()];
				int relId = relation.getRel().id;
				int arg1LabelId = arg1Span.entity.id;
				Relation[] curr = new Relation[allRels[pos][relId][arg1LabelId].length + 1];
				for (int i = 0; i < curr.length - 1; i++)
					curr[i] = allRels[pos][relId][arg1LabelId][i];
				curr[curr.length - 1] = relation;
				allRels[pos][relId][arg1Span.entity.id] = curr;
			}
		}
		return allRels;
	}
	
	private Span[][][] convertToModelOutputNoRelSpan(int size, Span[] spans, int[] noRelIdxs) {
		Span[][][] allSpans = new Span[size][Entity.ENTITIES.size()][0];
		for (int idx : noRelIdxs) {
			Span span = spans[idx];
			Span[] newOne = new Span[allSpans[span.start][span.entity.id].length + 1];
			for (int i = 0; i < newOne.length - 1; i++)
				newOne[i] = allSpans[span.start][span.entity.id][i];
			newOne[newOne.length - 1] = span;
			allSpans[span.start][span.entity.id] = newOne;
		}
		return allSpans;
	}
 	
	@Override
	public Instance decompile(Network net) {
		BaseNetwork network = (BaseNetwork)net;
		BaselineInstance result = (BaselineInstance)network.getInstance();
		//System.err.println(result.getInput().toString());
		int size = result.size();
		long[] nodes = network.getAllNodes();
		long aNode = network.getRoot();
//		print("max score:" + network.getMax());
		int aNode_k = Arrays.binarySearch(nodes, aNode);
		Set<Span> spans = new HashSet<>();
		List<RelationFullStruct> allRelations = new ArrayList<>();
		for (int pos = 0; pos < size; pos++) {
			int[] curChildren = network.getMaxPath(aNode_k);
			int eNode_k = curChildren[0];
			int[] curRNodes = network.getMaxPath(eNode_k);
			for (int rIdx = 0; rIdx < curRNodes.length; rIdx++) {
				int rNode_k = curRNodes[rIdx];
				int[] curArg1TNodes = network.getMaxPath(rNode_k);
				for (int arg1TIdx = 0; arg1TIdx < curArg1TNodes.length; arg1TIdx++) {
					int arg1TNode_k = curArg1TNodes[arg1TIdx];
					int[] tNodeArr = network.getNodeArray(arg1TNode_k);
					int arg1LabelId = tNodeArr[3];
					Entity arg1Ent = Entity.get(arg1LabelId);
					int[] child = network.getMaxPath(arg1TNode_k);
					int node_k = child[0];
					int[] nodeArr = network.getNodeArray(node_k);
					NodeType nodeType = NodeType.values()[nodeArr[1]];
					if (nodeType == NodeType.ARG1_NODE) {
						this.findAllRelations(allRelations, network, size, node_k, spans);
					} else if (nodeType == NodeType.I_NODE) {
						int start = pos;
						int end = start;
						List<Integer> endIdxs = new ArrayList<>();
						while(nodeType == NodeType.I_NODE) {
							child = network.getMaxPath(node_k);
							node_k = child[0];
							nodeArr = network.getNodeArray(node_k);
							nodeType = NodeType.values()[nodeArr[1]];
							end++;
						}
						endIdxs.add(end - 1);
//						print("all end indexes: " + endIdxs.toString());
						for (int endIdx : endIdxs) {
							Span span = new Span(start, endIdx, arg1Ent);
							spans.add(span);
						}
					} else if (nodeType == NodeType.X_NODE) {
						//do nothing
					} else {
						throw new RuntimeException("decompile node type: " + nodeType.name() + "?");
					}
				}
			}
			if(curChildren.length == 2){
				aNode_k = curChildren[1];
			}
		}
		List<Span> spanList = new ArrayList<>(spans);
		Collections.sort(spanList);
//		System.err.println(spanList.toString());
		Relation[][] relations = new Relation[size][];
		List<List<Relation>> relationList = new ArrayList<>();
		for (int i = 0; i< size; i++) relationList.add(new ArrayList<>());
		for (RelationFullStruct singleRel : allRelations) {
			int arg1Idx = spanList.indexOf(singleRel.arg1Span);
			int arg2Idx = spanList.indexOf(singleRel.arg2Span);
			Relation rel = new Relation(arg1Idx, arg2Idx, singleRel.rel);
			//System.err.println(singleRel.arg1Span.start);
			relationList.get(singleRel.arg1Span.start).add(rel);
		}
		for (int i = 0; i < size; i++) {
			relations[i] = relationList.get(i).toArray(new Relation[relationList.get(i).size()]);
		}
		result.setPrediction(new RelPosition(spanList.toArray(new Span[spanList.size()]), relations));
		return result;
	}
	
	private void findAllRelations(List<RelationFullStruct> list, BaseNetwork network, int size, int node_k, Set<Span> spans) {
		int[] nodeArr = network.getNodeArray(node_k);
		NodeType nodeType = NodeType.values()[nodeArr[1]];
		int[] child  = null;
		int arg1Start = size - nodeArr[0] - 1;
		int arg1LabelId = nodeArr[3];
		int relationId = nodeArr[2];
		RelationType relLabel = RelationType.get(relationId);
		int prevNode_k = -1;
		int arg1End = arg1Start;
		while(nodeType == NodeType.ARG1_NODE) {
			child = network.getMaxPath(node_k);
			prevNode_k = node_k;
			node_k = child[0];
			nodeArr = network.getNodeArray (node_k);
			nodeType = NodeType.values()[nodeArr[1]];
			arg1End++;
		}
		arg1End = arg1End - 1;
		Span arg1Span = new Span(arg1Start, arg1End, Entity.get(arg1LabelId));
//		print("find arg1: " + arg1Span);
		spans.add(arg1Span);
		List<Span> arg2Spans = new ArrayList<>();
		this.findArg2Spans(arg2Spans, network, size, prevNode_k);
		for (Span arg2 : arg2Spans) {
			spans.add(arg2);
			RelationFullStruct struct = new RelationFullStruct(arg1Span, arg2, relLabel);
//			System.out.println(struct.toString());
			list.add(struct);
		}
	}
	
	private void findArg2Spans (List<Span> list, BaseNetwork network, int size, int node_k) {
		int[] child  = network.getMaxPath(node_k);
		if (child.length == 1) {
			int[] childArr = network.getNodeArray(child[0]);
			NodeType nodeType = NodeType.values()[childArr[1]];
			if (nodeType == NodeType.ARG2_NODE) {
				int arg2Start = size - childArr[0] - 1;
				int arg2LabelId = childArr[3];
				int arg2End = arg2Start;
				while (nodeType != NodeType.X_NODE) {
					child  = network.getMaxPath(node_k);
					childArr = network.getNodeArray(child[0]);
					nodeType = NodeType.values()[childArr[1]];
					node_k = child[0];
					arg2End++;
				}
				arg2End = arg2End - 2;
				Span arg2Span = new Span(arg2Start, arg2End, Entity.get(arg2LabelId));
//				print("find arg2: " + arg2Span);
				list.add(arg2Span);
			} else {
				//O node.
				this.findArg2Spans(list, network, size, child[0]);
			}
		} else if (child.length == 2) {
			int[] firstChildArr = network.getNodeArray(child[0]);
			NodeType firstNodeType = NodeType.values()[firstChildArr[1]];
			int arg2Start = size - firstChildArr[0] - 1;
			int arg2LabelId = firstChildArr[3];
			int arg2End = arg2Start;
			while (firstNodeType != NodeType.X_NODE) {
				int[] currChild  = network.getMaxPath(node_k);
				firstChildArr = network.getNodeArray(currChild[0]);
				firstNodeType = NodeType.values()[firstChildArr[1]];
				node_k = currChild[0];
				arg2End++;
			}
			arg2End = arg2End - 2;
			Span arg2Span = new Span(arg2Start, arg2End, Entity.get(arg2LabelId));
			list.add(arg2Span);
//			print("find arg2: " + arg2Span);
			this.findArg2Spans(list, network, size, child[1]);
		}
	}

}
