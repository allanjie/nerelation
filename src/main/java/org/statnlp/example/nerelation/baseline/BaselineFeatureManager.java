package org.statnlp.example.nerelation.baseline;

import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.baseline.BaselineNetworkCompiler.NodeType;
import org.statnlp.hypergraph.FeatureArray;
import org.statnlp.hypergraph.FeatureManager;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.Network;

public class BaselineFeatureManager extends FeatureManager {
	
	private static final long serialVersionUID = 8227800758748672748L;
	
	private int wordWindowSize = 3;
	private int wordHalfWindowSize = 0;
	private int postagWindowSize = 3;
	private int postagHalfWindowSize = 1;
	
	private int wordNgramMinSize = 2;
	private int wordNgramMaxSize = 4;
	private int postagNgramMinSize = 2;
	private int postagNgramMaxSize = 4;
	
	private int bowWindowSize = 5;
	private int bowHalfWindowSize = 2;
	
	private static final boolean useCheatFeature = false;
	
	public enum FeatureType {
		CHEAT,
		WORD,
		WORD_NGRAM,
		
		POS_TAG,
		POS_TAG_NGRAM,
		
		BOW,
		
		ALL_CAPS,
		ALL_DIGITS,
		ALL_ALPHANUMERIC,
		ALL_LOWERCASE,
		CONTAINS_DIGITS,
		CONTAINS_DOTS,
		CONTAINS_HYPHEN,
		INITIAL_CAPS,
		LONELY_INITIAL,
		PUNCTUATION_MARK,
		ROMAN_NUMBER,
		SINGLE_CHARACTER,
		URL,
		RELATION_PENALTY,
		MENTION_PENALTY,
		ENT_REL
		
	}
	
	public BaselineFeatureManager(GlobalNetworkParam param_g) {
		super(param_g);
	}
	
	@Override
	protected FeatureArray extract_helper(Network net, int parent_k, int[] children_k, int children_k_index) {
		BaseNetwork network = (BaseNetwork)net;
		BaselineInstance inst = (BaselineInstance)network.getInstance();
		Sentence sent = inst.getInput();
		
		int size = inst.size();
		
		int[] parent_arr = network.getNodeArray(parent_k);
		int pos = size - parent_arr[0]-1;
		String relLabelId = String.valueOf(parent_arr[2]);
		String arg1LabelId = String.valueOf(parent_arr[3]);
		String arg2LabelId = String.valueOf(parent_arr[4]);
		String labelId = relLabelId + " " + arg1LabelId  + " " + arg2LabelId;
//		String labelStr = RelationType.get(parent_arr[2]).form + " " + Entity.get(parent_arr[3]).form + " " + Entity.get(parent_arr[4]).form;
		NodeType nodeType = NodeType.values()[parent_arr[1]];
		
		if (nodeType == NodeType.X_NODE || nodeType == NodeType.A_NODE || nodeType == NodeType.E_NODE || nodeType == NodeType.R_NODE || nodeType == NodeType.O_NODE) {
			return FeatureArray.EMPTY;
		}
		
		
		int[][] childrenArrs = new int[children_k.length][];
		for (int c = 0; c < childrenArrs.length; c++) {
			childrenArrs[c] = net.getNodeArray(children_k[c]);
		}
		if (nodeType == NodeType.ARG1_T_NODE && childrenArrs[0][1] == NodeType.X_NODE.ordinal()) {
			return FeatureArray.EMPTY;
		}
		
		List<Integer> features = new ArrayList<>();
		GlobalNetworkParam param_g = this._param_g;
		
		//CHEAT feature
		List<Integer> cheats = new ArrayList<>();
		String childListStr = "";
		for(int[] childNodeArr: childrenArrs){
			if(childListStr.length() > 0){
				childListStr += "#";
			}
			childListStr += NodeType.values()[childNodeArr[1]].name();
		}
		String specificIndicator = nodeType + "-" + childListStr;
		
		if (useCheatFeature) {
			cheats.add(param_g.toFeature(net, FeatureType.CHEAT.name(), relLabelId, Math.abs(inst.getInstanceId())+" "+pos+" "+nodeType+" "+childListStr));
		}
		
		List<Integer> relWords = new ArrayList<>();
		List<Integer> entWords = new ArrayList<>();
		List<Integer> entRel = new ArrayList<>();
		
		for (int[] childArr : childrenArrs) {
			NodeType childNodeType = NodeType.values()[childArr[1]];
			if (nodeType == NodeType.ARG1_T_NODE && childNodeType == NodeType.ARG1_NODE) {
				features.add(param_g.toFeature(network, FeatureType.RELATION_PENALTY.name(), "RP", "RP"));
			}
			if (nodeType == NodeType.ARG1_T_NODE && childNodeType == NodeType.I_NODE) {
				//mention penalty
				features.add(param_g.toFeature(network, FeatureType.MENTION_PENALTY.name(), "MP", "MP"));
			}
			String indicator = nodeType + "-" + childNodeType;
			for (int idx = pos - wordHalfWindowSize; idx <= pos + wordHalfWindowSize; idx++) {
				String word = "";
				if (idx >= 0 && idx < size) {
					word = sent.get(idx).getForm();
				}
				relWords.add(param_g.toFeature(network, indicator + ":" + FeatureType.WORD.name() + "[" +(idx - pos) + "]", relLabelId, word));
				entWords.add(param_g.toFeature(network, indicator + ":" + FeatureType.WORD.name() + "[" +(idx - pos) + "]", arg1LabelId + arg2LabelId, word));
				
				if (net.getInstance().getInstanceId() > 0) {
					String relStr = parent_arr[2] == RelationType.RELS.size()  ?  "NR" : RelationType.get(parent_arr[2]).form;
					String ent1Str = parent_arr[3] == Entity.ENTITIES.size() ? "Unknown" : Entity.get(parent_arr[3]).form ;
					String ent2Str = parent_arr[4] == Entity.ENTITIES.size() ? "Unknown" : Entity.get(parent_arr[4]).form;
					System.out.println("rel Feature is: "+indicator + "-" +FeatureType.WORD.name() + "[" +(idx - pos) + "]"+","+ relStr + "," + ent1Str + "," + ent2Str + "," +  word);
					System.out.println("entity Feature is: "+indicator + "-" +FeatureType.WORD.name() + "[" +(idx - pos) + "]"+","+ ent1Str + "," + ent2Str + "," +  word);
				}
			}
			entRel.add(param_g.toFeature(network, FeatureType.ENT_REL.name(), relLabelId, arg1LabelId + "," + arg2LabelId));
			
//			for(int idx=pos-postagHalfWindowSize; idx<=pos+postagHalfWindowSize; idx++){
//				String postag = "";
//				if(idx >= 0 && idx < size){
//					postag = sent.get(idx).getTag();
//				}
//				features.add(param_g.toFeature(network, indicator+FeatureType.POS_TAG.name()+(idx-pos), labelId, postag));
//			}
//			
//			for(int ngramSize=wordNgramMinSize; ngramSize<=wordNgramMaxSize; ngramSize++){
//				for(int relPos=0; relPos<ngramSize; relPos++){
//					String ngram = "";
//					for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
//						if(ngram.length() > 0) ngram += " ";
//						if(idx >= 0 && idx < size){
//							ngram += sent.get(idx).getForm();
//						}
//					}
//					features.add(param_g.toFeature(network, indicator+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
//				}
//			}
		}
		
		return this.createFeatureArray(network, features);
	}

}
