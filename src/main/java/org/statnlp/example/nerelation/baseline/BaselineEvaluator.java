package org.statnlp.example.nerelation.baseline;

import static org.statnlp.commons.Utils.print;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;

public class BaselineEvaluator {
	
	private boolean includeRev;
	
	public BaselineEvaluator(boolean includeRev) {
		this.includeRev = includeRev;
	}
	
	public void evaluate(Instance[] insts) {
		double[][] metrics = this.getNEScore(insts);
		double[][] relMetrics = this.includeRev? this.getREScoreIncludeRev(insts) : this.getREScore(insts);
		//print
		int numEntities = Entity.ENTITIES.size();
		int numRelations = RelationType.RELS.size();
		print("[Evluation Info] Entity/Mention");
		for(int l = 0; l < numEntities; l++) {
			String entity = Entity.get(l).form;
			System.out.printf("%s:\tPrecision: %.2f%% Recall: %.2f%% Fscore: %.2f%%\n", entity, metrics[l][0] * 100, metrics[l][1] * 100, metrics[l][2] * 100);
		}
		System.out.printf("Comb:\tPrecision: %.2f%% Recall: %.2f%% Fscore: %.2f%%\n", metrics[numEntities][0] * 100, metrics[numEntities][1] * 100, metrics[numEntities][2] * 100);
		print("[Evaluation Info] Relation");
		for (int l = 0; l < numRelations; l++) {
			String relation = RelationType.get(l).form;
			if (relation.endsWith(BaselineReader.REV_SUFF) && !this.includeRev) continue;
			System.out.printf("%s:\tPrecision: %.2f%% Recall: %.2f%% Fscore: %.2f%%\n", relation.substring(0, 5), relMetrics[l][0] * 100, relMetrics[l][1] * 100, relMetrics[l][2] * 100);
		}
		System.out.printf("Comb:\tPrecision: %.2f%% Recall: %.2f%% Fscore: %.2f%%\n", relMetrics[numRelations][0] * 100, relMetrics[numRelations][1] * 100, relMetrics[numRelations][2] * 100);
	}
	
	/**
	 * Pass in the prediction, return the precision, recall and f-score
	 * @param predictions
	 * @return
	 */
	private double[][] getNEScore(Instance[] predictions) {
		double[][] all = new double[Entity.ENTITIES.size()+1][3];
		int[] corr = new int[Entity.ENTITIES.size()];
		int[] totalPred = new int[Entity.ENTITIES.size()];
		int[] totalExpected = new int[Entity.ENTITIES.size()];
		for (Instance inst : predictions) {
			BaselineInstance baseInst = (BaselineInstance)inst;
			RelPosition output = baseInst.getOutput();
			RelPosition prediction = baseInst.getPrediction();
			Span[] goldSpanArr = output.getSpans();
			List<Span> goldSpanList = new ArrayList<>(goldSpanArr.length);
			for (int i = 0; i < goldSpanArr.length; i++) goldSpanList.add(goldSpanArr[i]);
			//print(goldSpanList.toString());
			Set<Span> goldSpanSet = new HashSet<>(goldSpanList);
			Span[] predSpanArr = prediction.getSpans();
			
			List<Span> predSpanList = new ArrayList<>(predSpanArr.length);
			for (int i = 0; i < predSpanArr.length; i++) predSpanList.add(predSpanArr[i]);
			Collections.sort(predSpanList);
			//print(predSpanList.toString());
			
			for (int s = 0; s < predSpanArr.length; s++) {
				Span predSpan = predSpanArr[s];
				totalPred[predSpan.entity.id]++;
				if (goldSpanSet.contains(predSpan)) corr[predSpan.entity.id]++;
			}
			for (int s = 0; s < goldSpanArr.length; s++) totalExpected[goldSpanArr[s].entity.id]++;
		}
		double sumCorr = 0;
		double sumPred = 0;
		double sumExpected = 0;
		for (int l = 0; l < Entity.ENTITIES.size(); l++) {
			all[l][0] = corr[l] * 1.0 / totalPred[l];
			all[l][1] = corr[l] * 1.0 / totalExpected[l];
			all[l][2] = 2.0/(1.0/all[l][0] + 1.0/all[l][1]);
			sumCorr += corr[l];
			sumPred += totalPred[l];
			sumExpected += totalExpected[l];
		}
		all[Entity.ENTITIES.size()][0] = sumCorr/sumPred;
		all[Entity.ENTITIES.size()][1] = sumCorr/sumExpected;
		all[Entity.ENTITIES.size()][2] = 2 * sumCorr / (sumPred +sumExpected) ;
		return all;
	}

	private double[][] getREScore(Instance[] predictions) {
		int numRelations = RelationType.RELS.size();
		double[][] relMetrics = new double[numRelations + 1][3];
		int[] corr = new int[numRelations];
		int[] totalPred = new int[numRelations];
		int[] totalExpected = new int[numRelations];
		double sumCorr = 0;
		double sumPred = 0;
		double sumExpected = 0;
		for (Instance inst : predictions) {
			BaselineInstance baseInst = (BaselineInstance)inst;
			RelPosition output = baseInst.getOutput();
			RelPosition prediction = baseInst.getPrediction();
			List<EvalRelation> gold = this.convertStruct(output.getRelations(), output.getSpans());
			Set<EvalRelation> goldSet = new HashSet<>(gold);
			List<EvalRelation> pred = this.convertStruct(prediction.getRelations(), prediction.getSpans());
			for (EvalRelation predRelation : pred) {
				totalPred[predRelation.relType.id]++;
				sumPred++;
				if (goldSet.contains(predRelation)) {
					corr[predRelation.relType.id]++;
					sumCorr++;
				}
			}
			for (EvalRelation goldRelation : gold) {
				totalExpected[goldRelation.relType.id]++;
				sumExpected++;
			}
		}
		for (int l = 0; l < numRelations; l++) {
			if (RelationType.get(l).form.endsWith(BaselineReader.REV_SUFF)) continue;
			String relationForm = RelationType.get(l).form;
			String revForm = relationForm + BaselineReader.REV_SUFF;
			int revCorr = 0;
			int revTotalPred = 0;
			int revTotalExpected = 0;
			if (RelationType.RELS.containsKey(revForm)) {
				int revId = RelationType.get(revForm).id;
				revCorr = corr[revId];
				revTotalPred = totalPred[revId];
				revTotalExpected = totalExpected[revId];
			} 
			relMetrics[l][0] = (corr[l] + revCorr) * 1.0 / (totalPred[l] + revTotalPred);
			relMetrics[l][1] = (corr[l] + revCorr) * 1.0 / (totalExpected[l] + revTotalExpected);
			relMetrics[l][2] = 2.0 * (corr[l] + revCorr) / (totalPred[l] + revTotalPred + totalExpected[l] + revTotalExpected);
		}
		relMetrics[numRelations][0] = sumCorr / sumPred;
		relMetrics[numRelations][1] = sumCorr / sumExpected;
		relMetrics[numRelations][2] = (2 * sumCorr) / (sumPred + sumExpected);
		return relMetrics;
	}
	
	private double[][] getREScoreIncludeRev(Instance[] predictions) {
		int numRelations = RelationType.RELS.size();
		double[][] relMetrics = new double[numRelations + 1][3];
		int[] corr = new int[numRelations];
		int[] totalPred = new int[numRelations];
		int[] totalExpected = new int[numRelations];
		double sumCorr = 0;
		double sumPred = 0;
		double sumExpected = 0;
		for (Instance inst : predictions) {
			BaselineInstance baseInst = (BaselineInstance)inst;
			RelPosition output = baseInst.getOutput();
			RelPosition prediction = baseInst.getPrediction();
			List<EvalRelation> gold = this.convertStruct(output.getRelations(), output.getSpans());
			Set<EvalRelation> goldSet = new HashSet<>(gold);
			List<EvalRelation> pred = this.convertStruct(prediction.getRelations(), prediction.getSpans());
			for (EvalRelation predRelation : pred) {
				totalPred[predRelation.relType.id]++;
				sumPred++;
				if (goldSet.contains(predRelation)) {
					corr[predRelation.relType.id]++;
					sumCorr++;
				}
			}
			for (EvalRelation goldRelation : gold) {
				totalExpected[goldRelation.relType.id]++;
				sumExpected++;
			}
		}
		for (int l = 0; l < numRelations; l++) {
			relMetrics[l][0] = (corr[l]) * 1.0 / (totalPred[l]);
			relMetrics[l][1] = (corr[l]) * 1.0 / (totalExpected[l]);
			relMetrics[l][2] = 2.0 * (corr[l]) / (totalPred[l] + totalExpected[l]);
		}
		relMetrics[numRelations][0] = sumCorr / sumPred;
		relMetrics[numRelations][1] = sumCorr / sumExpected;
		relMetrics[numRelations][2] = (2 * sumCorr) / (sumPred + sumExpected);
		return relMetrics;
	}
	
	
	/**
	 * Convert the relation structure into list of relation.
	 * @param relStruct
	 * @return
	 */
	private List<EvalRelation> convertStruct(Relation[][] relStruct, Span[] spans) {
		List<EvalRelation> relations = new ArrayList<>();
		for (int p = 0; p < relStruct.length; p++) {
			for (int r = 0; r < relStruct[p].length; r++) {
				EvalRelation evalRelation = new EvalRelation(spans[relStruct[p][r].getArg1Idx()], 
						spans[relStruct[p][r].getArg2Idx()], relStruct[p][r].getRel());
				relations.add(evalRelation);
			}
		}
		return relations;
	}
	
	public static class EvalRelation {
		Span arg1Span;
		Span arg2Span;
		RelationType relType;
		
		public EvalRelation(Span arg1Span, Span arg2Span, RelationType relType) {
			this.arg1Span = arg1Span;
			this.arg2Span = arg2Span;
			this.relType = relType;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((arg1Span == null) ? 0 : arg1Span.hashCode());
			result = prime * result + ((arg2Span == null) ? 0 : arg2Span.hashCode());
			result = prime * result + ((relType == null) ? 0 : relType.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EvalRelation other = (EvalRelation) obj;
			if (arg1Span == null) {
				if (other.arg1Span != null)
					return false;
			} else if (!arg1Span.equals(other.arg1Span))
				return false;
			if (arg2Span == null) {
				if (other.arg2Span != null)
					return false;
			} else if (!arg2Span.equals(other.arg2Span))
				return false;
			if (relType == null) {
				if (other.relType != null)
					return false;
			} else if (!relType.equals(other.relType))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "EvalRelation [arg1Span=" + arg1Span + ", arg2Span=" + arg2Span + ", relType=" + relType + "]";
		}
		
		
		
	}
}
