package data.preprocessing.ace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.statnlp.commons.io.RAWF;



public class ACEProcess {

	
	/**
	 * Read the .data file and output the .penn file.
	 * They are then used by chunklink.pl.
	 * @param file
	 * @param output
	 * @throws IOException
	 */
	public static void data2Penn(String file, String output) throws IOException {
		BufferedReader br = RAWF.reader(file);
		PrintWriter pw = RAWF.writer(output);
		String line = null;
		while ((line = br.readLine()) != null) {
			//System.err.println(line);
			line = br.readLine();
			//System.err.println(line);
			line = br.readLine();
//			tree = ptr.readTree();
//			tree.setValue("");
			line = line.replaceFirst("ROOT", "");
			//line = line.replaceAll("\\(\\)", "\\( \\)");
			pw.println(line.toString());
			//System.err.println(line);
			line = br.readLine();
			//System.err.println(line);
			line = br.readLine();
			//System.err.println(line);
			line = br.readLine();
			//System.err.println(line);
			line = br.readLine();
			//System.err.println(line);
			br.readLine();
			//line = br.readLine();
			
			//
		}
		pw.close();
		br.close();
	}
	
	public static void main(String[] args) throws IOException {
		for (int i = 1; i <= 5; i++) {
			String file = "data/ACE2004Folds/fold."+i+".data";
			String output = "data/ACE2004FoldsPenn/fold."+i+".penn";
			data2Penn(file, output);
		}
		//fold 2, the (888) needs to be modified as 888.
	}
}
