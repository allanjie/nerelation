package data.preprocessing.ace;

import static org.statnlp.commons.Utils.print;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.statnlp.example.nerelation.Entity;
import org.statnlp.example.nerelation.Relation;
import org.statnlp.example.nerelation.RelationType;
import org.statnlp.example.nerelation.Span;
import org.statnlp.example.nerelation.baseline.BaselineInstance;

import cern.colt.Arrays;

public class ACECrossRelStat {

	public static boolean useHeadEnd = false;
	public static boolean useHeadStart = false;
	public static boolean maxMarginal = false;
	public static boolean ignoreType2 = false; 
	
	public static boolean checkCrossArg1Overlap (BaselineInstance inst) {
		List<Relation> relations = convertOutput2List(inst.getOutput().getRelations());
		Span[] spans = inst.getOutput().getSpans();
		Map<RelationType, List<Relation>> relMap = new HashMap<>();
		for (Relation relation : relations) {
			if (relMap.containsKey(relation.getRel())) {
				relMap.get(relation.getRel()).add(relation);
			} else {
				List<Relation> list = new ArrayList<>();
				list.add(relation);
				relMap.put(relation.getRel(), list);
			}
		}
		for (RelationType rel : relMap.keySet()) {
			List<Relation> list = relMap.get(rel);
			//arg1 entity, list of relation
			Map<Entity, List<Relation>> map = new HashMap<>();
			for (Relation relation : list) {
				Span arg1Span = spans[relation.getArg1Idx()];
				Entity arg1Ent = arg1Span.entity;
				if (map.containsKey(arg1Ent)) {
					map.get(arg1Ent).add(relation);
				} else {
					List<Relation> rels = new ArrayList<>();
					rels.add(relation);
					map.put(arg1Ent, rels);
				}
			}
			for (Entity entity : map.keySet()) {
				List<Relation> currList = map.get(entity);
				for (int i = 0; i < currList.size(); i++) {
					Relation one = currList.get(i);
					for (int j = i + 1; j < currList.size(); j++) {
						Relation two = currList.get(j);
						if (one.getArg1Idx() == two.getArg1Idx()) continue;
						if (spans[one.getArg1Idx()].overlap(spans[two.getArg1Idx()])) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public static boolean checkCrossArg2Overlap (BaselineInstance inst) {
		List<Relation> relations = convertOutput2List(inst.getOutput().getRelations());
		Span[] spans = inst.getOutput().getSpans();
		Map<RelationType, List<Relation>> relMap = new HashMap<>();
		for (Relation relation : relations) {
			if (relMap.containsKey(relation.getRel())) {
				relMap.get(relation.getRel()).add(relation);
			} else {
				List<Relation> list = new ArrayList<>();
				list.add(relation);
				relMap.put(relation.getRel(), list);
			}
		}
		for (RelationType rel : relMap.keySet()) {
			List<Relation> list = relMap.get(rel);
			//arg1 entity, list of relation
			Map<Entity, List<Relation>> map = new HashMap<>();
			for (Relation relation : list) {
				Span arg1Span = spans[relation.getArg1Idx()];
				Entity arg1Ent = arg1Span.entity;
				if (map.containsKey(arg1Ent)) {
					map.get(arg1Ent).add(relation);
				} else {
					List<Relation> rels = new ArrayList<>();
					rels.add(relation);
					map.put(arg1Ent, rels);
				}
			}
			for (Entity entity : map.keySet()) {
				List<Relation> currList = map.get(entity);
				for (int i = 0; i < currList.size(); i++) {
					Relation one = currList.get(i);
					for (int j = i + 1; j < currList.size(); j++) {
						Relation two = currList.get(j);
						if (one.getArg1Idx() == two.getArg1Idx()) continue;
						if (spans[one.getArg2Idx()].overlap(spans[two.getArg2Idx()]) && spans[one.getArg2Idx()].end != spans[two.getArg2Idx()].end) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	
	public static boolean checkOverlappingRelation (BaselineInstance inst) {
		List<Relation> relations = convertOutput2List(inst.getOutput().getRelations());
		Span[] spans = inst.getOutput().getSpans();
		for (int r1 = 0; r1 < relations.size(); r1++) {
			Relation rel1 = relations.get(r1);
			int r1LeftMost = Math.min(spans[rel1.getArg1Idx()].start, spans[rel1.getArg2Idx()].start);
			int r1RightMost = Math.max(spans[rel1.getArg1Idx()].end, spans[rel1.getArg2Idx()].end);
			for (int r2 = r1+1; r2 < relations.size(); r2++) {
				Relation rel2 = relations.get(r2);
				if (!rel1.getRel().equals(rel2.getRel())) continue;
				if (rel1.getArg1Idx() == rel2.getArg1Idx()) continue;
				int r2LeftMost = Math.min(spans[rel2.getArg1Idx()].start, spans[rel2.getArg2Idx()].start);
				int r2RightMost = Math.max(spans[rel2.getArg1Idx()].end, spans[rel2.getArg2Idx()].end);
				if (r1RightMost < r2LeftMost || r1LeftMost > r2RightMost) continue;
				int l = Math.min(r1LeftMost, r2LeftMost);
				int r = Math.max(r1RightMost, r2RightMost);
				String[] r1Str = new String[r - l + 1];
				String[] r2Str = new String[r - l + 1];
				for (int i = 0; i < r1Str.length; i++) {
					int realIdx = l + i;
					if (realIdx >= r1LeftMost && realIdx <= r1RightMost) {
						if (realIdx >= spans[rel1.getArg1Idx()].start && realIdx <= spans[rel1.getArg1Idx()].end
								&& realIdx >= spans[rel1.getArg2Idx()].start && realIdx <= spans[rel1.getArg2Idx()].end) {
							r1Str[i] = "1-2-"+spans[rel1.getArg1Idx()].entity.form + "-"+spans[rel1.getArg2Idx()].entity.form;
						} else if (realIdx >= spans[rel1.getArg1Idx()].start && realIdx <= spans[rel1.getArg1Idx()].end) {
							r1Str[i] = "1-"+spans[rel1.getArg1Idx()].entity.form;
						} else if (realIdx >= spans[rel1.getArg2Idx()].start && realIdx <= spans[rel1.getArg2Idx()].end) {
							r1Str[i] = "2-"+spans[rel1.getArg2Idx()].entity.form;
						} else if (realIdx > spans[rel1.getArg1Idx()].end && realIdx < spans[rel1.getArg2Idx()].start) {
							r1Str[i] = "O";
						} else {
							System.out.println(r1LeftMost+"," +r1RightMost);
							System.out.println(spans[rel1.getArg1Idx()].toString());
							System.out.println(spans[rel1.getArg2Idx()].toString());
							throw new RuntimeException("what is this idex:" + realIdx);
						}
					} else {
						r1Str[i] = "nothing";
					}
				}
				for (int i = 0; i < r2Str.length; i++) {
					int realIdx = l + i;
					if (realIdx >= r2LeftMost && realIdx <= r2RightMost) {
						if (realIdx >= spans[rel2.getArg1Idx()].start && realIdx <= spans[rel2.getArg1Idx()].end
								&& realIdx >= spans[rel2.getArg2Idx()].start && realIdx <= spans[rel2.getArg2Idx()].end) {
							r2Str[i] = "1-2-"+spans[rel2.getArg1Idx()].entity.form + "-"+spans[rel2.getArg2Idx()].entity.form;
						} else if (realIdx >= spans[rel2.getArg1Idx()].start && realIdx <= spans[rel2.getArg1Idx()].end) {
							r2Str[i] = "1-"+spans[rel2.getArg1Idx()].entity.form;
						} else if (realIdx >= spans[rel2.getArg2Idx()].start && realIdx <= spans[rel2.getArg2Idx()].end) {
							r2Str[i] = "2-"+spans[rel2.getArg2Idx()].entity.form;
						} else if (realIdx > spans[rel2.getArg1Idx()].end && realIdx < spans[rel2.getArg2Idx()].start) {
							r2Str[i] = "O";
						} else {
							throw new RuntimeException("what is this idex:" + realIdx);
						}
					} else {
						r2Str[i] = "nothing";
					}
				}
				if (r1RightMost == r2RightMost && rel1.getArg2Idx() == rel2.getArg2Idx()) {
					int diffIdx = r1Str.length - 1;
					for (int i = r1Str.length - 1; i >=0 ; i--) {
						if (!r1Str[i].equals(r2Str[i])) {
							diffIdx = i;
							break;
						}
					}
					for (int i = diffIdx - 1; i >= 0; i--) {
						if(r1Str[i].equals(r2Str[i]) && r1Str[i].equals("nothing")) continue;
						if (!r1Str[i].equals(r2Str[i])) continue;
						if (r1Str[i].equals(r2Str[i])) {
//							System.out.println(Arrays.toString(r1Str));
//							System.out.println(Arrays.toString(r2Str));
//							System.out.println();
							return true;
						}
					}
				} else if (r1LeftMost == r2LeftMost && spans[rel1.getArg1Idx()].entity.form.equals(spans[rel2.getArg1Idx()].entity.form)) {	
					int diffIdx = 0;
					for (int i = 0; i < r1Str.length; i++) {
						if (!r1Str[i].equals(r2Str[i])) {
							diffIdx = i;
							break;
						}
					}
					if (diffIdx != 0) {
						for (int i = diffIdx; i < r1Str.length; i++) {
							if (!r1Str[i].equals(r2Str[i])) continue;
							if(r1Str[i].equals(r2Str[i]) && r1Str[i].equals("nothing")) continue;
							if (r1Str[i].equals(r2Str[i])) {
								System.out.println(Arrays.toString(r1Str));
								System.out.println(Arrays.toString(r2Str));
								System.out.println();
								return true;
							}
						}
					}
				}else {
					for (int i = 0; i < r1Str.length; i++) {
						if (r1Str[i].equals(r2Str[i]) && r1Str[i].equals("nothing")) continue;
						if (!r1Str[i].equals(r2Str[i])) continue;
						if (r1Str[i].equals(r2Str[i])) {
//							System.out.println(Arrays.toString(r1Str));
//							System.out.println(Arrays.toString(r2Str));
//							System.out.println();
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public static int checkRelation (BaselineInstance[] insts) {
		int num = 0;
		int crossArg1OverlapNum = 0;
		int crossArg2OverlapNum = 0;
		int crossArg1Arg2OverlapNum = 0;
		for (BaselineInstance inst : insts) {
			List<Relation> relations = convertOutput2List(inst.getOutput().getRelations());
			Span[] spans = inst.getOutput().getSpans();
			Map<RelationType, List<Relation>> relMap = new HashMap<>();
			for (Relation relation : relations) {
				if (relMap.containsKey(relation.getRel())) {
					relMap.get(relation.getRel()).add(relation);
				} else {
					List<Relation> list = new ArrayList<>();
					list.add(relation);
					relMap.put(relation.getRel(), list);
				}
			}
			boolean found = false;
			boolean foundArg1CrossOverlap = false;
			boolean foundArg2CrossOverlap = false;
			boolean foundArg1Arg2CrossOverlap = false;
			for (RelationType rel : relMap.keySet()) {
				if (!found && relMap.get(rel).size() > 1) {
					num++;
					found = true;
				}
				
				List<Relation> list = relMap.get(rel);
				//arg1 entity, list of relation
				Map<Entity, List<Relation>> map = new HashMap<>();
				for (Relation relation : list) {
					Span arg1Span = spans[relation.getArg1Idx()];
					Entity arg1Ent = arg1Span.entity;
					if (map.containsKey(arg1Ent)) {
						map.get(arg1Ent).add(relation);
					} else {
						List<Relation> rels = new ArrayList<>();
						rels.add(relation);
						map.put(arg1Ent, rels);
					}
				}
				
				for (Entity entity : map.keySet()) {
					List<Relation> currList = map.get(entity);
					for (int i = 0; i < currList.size(); i++) {
						Relation one = currList.get(i);
						for (int j = i + 1; j < currList.size(); j++) {
							Relation two = currList.get(j);
							if (one.getArg1Idx() == two.getArg1Idx()) continue;
							if (spans[one.getArg1Idx()].overlap(spans[two.getArg1Idx()])) {
//								System.out.println(inst.getInput().toString());
//								System.out.println(one.toString());
//								System.out.println(two.toString());
//								System.out.println(spans[one.getArg1Idx()]);
//								System.out.println(spans[two.getArg1Idx()]);
//								System.out.println();
								foundArg1CrossOverlap = true;
								if (spans[one.getArg1Idx()].overlap(spans[two.getArg1Idx()]) && spans[one.getArg2Idx()].overlap(spans[two.getArg1Idx()])) {
									System.out.println(inst.getInput().toString());
									System.out.println(one.toString());
									System.out.println(two.toString());
									System.out.println(spans[one.getArg1Idx()]);
									System.out.println(spans[two.getArg1Idx()]);
									System.out.println();
									foundArg1Arg2CrossOverlap = true;
								}
							}
							
							if (spans[one.getArg2Idx()].overlap(spans[two.getArg2Idx()])) {
								foundArg2CrossOverlap = true;
							}
						}
					}
				}
			}
			if (foundArg1CrossOverlap) {
				crossArg1OverlapNum++;
			}
			if (foundArg2CrossOverlap) {
				crossArg2OverlapNum++;
			}
			if (foundArg1Arg2CrossOverlap) {
				crossArg1Arg2OverlapNum++;
			}
		}
		print("Cross arg1 overlap num: "+ crossArg1OverlapNum);
		print("Cross arg2 overlap num: "+ crossArg2OverlapNum);
		print("Cross arg1 and also arg2 overlap num: "+ crossArg1Arg2OverlapNum);
		return num;
	}
	
	private static List<Relation> convertOutput2List(Relation[][] output) {
		List<Relation> list = new ArrayList<>();
		for (int p = 0; p < output.length; p++) {
			for (int r = 0; r< output[p].length; r++) {
				if (!list.contains(output[p][r]))
					list.add(output[p][r]);
			}
		}
		return list;
	}
	
	
//	public static void main (String[] args) {
//		String file = "data/ACE2004/all.data";
//		
//		BaselineReader reader = new BaselineReader(useHeadStart, useHeadEnd, ignoreType2, false, false, false, false);
//		BaselineInstance[] allInsts = reader.read(file, true, -1);
//		print(" mulitple same relation type insts: " + checkRelation(allInsts));
//	}
}
