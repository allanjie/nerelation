#!/bin/bash


## read the number of documents in the ace 2004 dataset
## save the file list in a file by appending
ace2004_dir=/data/allan/data/ACE_Dataset/ACE2004/English

num=0
fileList="allFileList.txt"
touch $fileList
for chapter in $ace2004_dir/*
do
   if [[ -d $chapter  ]]; then
      echo "Entering $chapter"
      for article in $chapter/*
      do 
         if [[ -f $article ]]; then
            article_name=$(basename $article)
            #echo "Access File $article_name"
            suffix=".sgm"
    	    if [[ "$article_name" == *$suffix ]]; then
               #echo $(( num++ ))
               name=${article_name%$suffix}
               echo "$name" >> $fileList
               let "num=num+1"
            fi
         fi
      done
   fi
done
echo "Total number of documents in $ace2004_dir"
echo $num


