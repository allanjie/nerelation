#!/bin/bash

fold_num=5
#fold 2 needs special treatment as in replacing (888) with just 888
for i in $(seq 1 $fold_num);
do
  echo $i
   perl chunklink.pl -ns < ../data/ACE2004FoldsPenn/fold.$i.penn > ../data/ACE2004FoldsChunk/fold.$i.chunk
done

